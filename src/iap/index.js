import React from 'react';
import * as RNIap from 'react-native-iap';

import {saveData} from '../constants/GlobalFunction';
import {Variables} from '../constants/Variables';

export const IAPService = () => {
  let purchaseUpdateSubscription = null;
  let purchaseErrorSubscription = null;

  /**
   * @param productId string
   */
  const updateStoreOnPurchaseSuccess = async productId => {
    // please connect your logic to hide adds
    await saveData(Variables.showAd, JSON.stringify(false));
  };

  /**
   * subscribs to purchaseUpdateSubscription and purchaseErrorSubscription
   */
  const addProductPurchaseUpdateListener = async () => {
    if (Platform.OS === 'android') {
      try {
        let _ = await RNIap.flushFailedPurchasesCachedAsPendingAndroid();
      } catch (error) {
        // report error on pending purchase
      }
    }
    purchaseUpdateSubscription = RNIap.purchaseUpdatedListener(
      async purchase => {
        const receipt = purchase.transactionReceipt;
        if (receipt) {
          updateStoreOnPurchaseSuccess(purchase.productId);
          await RNIap.finishTransaction(purchase, true);
        }
      },
    );

    purchaseErrorSubscription = RNIap.purchaseErrorListener(error => {
      // listen for purchase error and act accordingly
    });
  };

  /**
   * Initializes in app purchase and subscribs Product Purchase Update Listener
   */
  const initializeInAppPurchase = async () => {
    let canMakePayment = await RNIap.initConnection();
    if (canMakePayment) {
      addProductPurchaseUpdateListener();
    }
  };

  React.useEffect(() => {
    initializeInAppPurchase();
    return () => {
      if (purchaseUpdateSubscription) {
        purchaseUpdateSubscription.remove();
      }
      if (purchaseErrorSubscription) {
        purchaseErrorSubscription.remove();
      }
    };
  }, []);

  return null;
};
