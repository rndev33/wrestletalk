import React, { useState, useEffect } from "react";
import { View, Image, TouchableOpacity, ScrollView, FlatList, Platform, useWindowDimensions } from 'react-native'
import CustomText from "../../components/CustomText";
import HeaderComponent from "../../components/HeaderComponent";
import { redBackIcon } from "../../constants/Images";
import CommonStyles from "../../style/CommonStyles";
import SpaceStyles from "../../style/SpaceStyles";
import TextStyles from "../../style/TextStyles";
import { DARK_BLACK, MAIN_BLACK, PRIMARY_COLOR, WHITE } from "../../constants/Colors";
import CustomButton from "../../components/CustomButton";
import { getAllCommentApiCall } from "../../redux/services/talkServices";
import YoutubePlayer from 'react-native-youtube-iframe';
import HTML from "react-native-render-html";
import { SafeAreaView } from "react-native";
import { getData } from "../../constants/GlobalFunction";
import { Variables } from "../../constants/Variables";
var moment = require('moment-timezone');
moment().tz("Europe/London").format();

function VideoDetailScreen(props) {
    const { navigation, route } = props
    const { videoDetail } = route?.params
    const isIos = Platform.OS === 'ios'
    const [token, setToken] = useState('')
    const {width}=useWindowDimensions()


    const [allComment, setAllComment] = useState('')

    useEffect(() => {
        getComment()
        getToken()
    }, [])

    const getToken = async () => {
        getData(Variables.accessToken).then((res) => {
            setToken(JSON.parse(res))
        }).catch(() => {
        })
    }

    const getComment = () => {
        getAllCommentApiCall(`?post=${videoDetail.id}`)
            .then((res) => {
                setAllComment(res.data)
            }).catch((err) => {

            })
    }

    const tagsStyles = {
        body: {
            fontFamily: isIos ? 'Montserrat-Medium' : 'GothamMedium',
            fontSize: 14,
            color: MAIN_BLACK,
            letterSpacing: 0.5,
            lineHeight: isIos ? 19 : 17,
            fontWeight :'900'
        },
        a: {
            color: '#EF0054',
        }
    };

    const commentStyle = {
        fontFamily: isIos ? 'Montserrat-Medium' : 'GothamMedium',
        fontSize: 12,
        color: MAIN_BLACK,
        letterSpacing: 0.5,
        lineHeight: isIos ? 17 : 15,
        fontWeight :'900'
    };
    const titleTagStyle = {
        fontFamily: isIos ? 'Montserrat-Black' : 'Gotham Black Regular',
        fontSize: 14,
        color: DARK_BLACK,
        letterSpacing: 0.5,
        lineHeight: isIos ? 19 : 17,
        fontWeight :'900'
    };

    const renderComment = ({ item, index }) => {
        let time = moment.tz(item.date, 'Europe/London')

        return (
            <>
                <View style={[SpaceStyles.alignSpaceBlock, SpaceStyles.top2]}>
                    <CustomText
                        text={item?.author_name}
                        style={[TextStyles.bold14LightBlack]}
                    />
                    <CustomText
                        text={moment(time).format('MMM DD, hh:mm a')}
                        style={[TextStyles.medium10FullGray]}
                    />
                </View>
                <HTML
                contentWidth={width}
                    source={{ html: item?.content?.rendered?.split("</p>")[0] }}
                    baseStyle={commentStyle}
                />
                {/* <View style={[SpaceStyles.alignSpaceBlock, SpaceStyles.top2]}>
                    <CustomText
                        text={'REPLY'}
                        style={[TextStyles.bold10Black]}
                    />
                    <View style={SpaceStyles.rowFlex}>
                        <Image source={likeIcon} />
                        <CustomText
                            text={'454'}
                            style={[TextStyles.bold12Black, SpaceStyles.left1]}
                        />
                        <Image source={disLikeIcon} style={SpaceStyles.left2} />
                        <CustomText
                            text={'454'}
                            style={[TextStyles.bold10GrayText, SpaceStyles.left1]}
                        />
                    </View>
                </View> */}

                {/* <View style={[SpaceStyles.rowFlex, SpaceStyles.top2]}>
                    <Image source={downCommentArrow} />
                    <View style={[SpaceStyles.alignSpaceBlock, SpaceStyles.top1, SpaceStyles.left2]}>
                        <CustomText
                            text={'Tim Burr'}
                            style={[TextStyles.bold14LightBlack]}
                        />
                    </View>
                </View> */}

                {/* <View style={SpaceStyles.left5}>

                    <CustomText
                        text={'His contract is said to be a bit of a surprise since so many WWE contracts have time added because of injuries or other reasons.'}
                        style={[TextStyles.medium12LightBlack, SpaceStyles.top1, { lineHeight: 18 }]}
                    />
                    <View style={[SpaceStyles.alignSpaceBlock, SpaceStyles.top2]}>
                        <CustomText
                            text={'REPLY'}
                            style={[TextStyles.bold10Black]}
                        />
                        <View style={SpaceStyles.rowFlex}>
                            <Image source={likeIcon} />
                            <CustomText
                                text={'454'}
                                style={[TextStyles.bold12Black, SpaceStyles.left1]}
                            />
                            <Image source={disLikeIcon} style={SpaceStyles.left2} />
                            <CustomText
                                text={'454'}
                                style={[TextStyles.bold10GrayText, SpaceStyles.left1]}
                            />
                        </View>
                    </View>
                </View> */}
            </>
        )
    }
    return (
        <View style={CommonStyles.mainContainer}>
            <SafeAreaView style={{ backgroundColor: PRIMARY_COLOR }} />
            <HeaderComponent />
            <ScrollView
                showsVerticalScrollIndicator={false}
                style={SpaceStyles.spaceHorizontal}
            >
                <View style={SpaceStyles.vertical1}>
                    <View
                        style={[SpaceStyles.spaceVertical, SpaceStyles.row]}
                    >
                        <TouchableOpacity
                            onPress={() => navigation.goBack()}
                        >
                            <Image resizeMode={'contain'} source={redBackIcon} />
                        </TouchableOpacity>
                        <View style={[SpaceStyles.left2, SpaceStyles.width80]}>
                            <HTML
                            contentWidth={width}
                                source={{ html: videoDetail?.title?.rendered?.toUpperCase() }}
                                baseStyle={titleTagStyle}
                            />
                        </View>
                    </View>
                    <YoutubePlayer
                        height={230}
                        play={false}
                        videoId={videoDetail?.ACF?.you_tube?.split('embed/')[1]?.split('?start')[0] || videoDetail?.ACF?.you_tube?.split('embed/')[1]?.split('?feature')[0]}
                    />

                    <HTML
                    contentWidth={width}
                        source={{ html: videoDetail?.content?.rendered }}
                        baseStyle={tagsStyles.body}
                        tagsStyles={tagsStyles}
                    />

                    {allComment.length > 0 &&
                        <CustomText
                            text={`COMMENTS ${allComment?.length}`}
                            style={[TextStyles.bold13MainBlack, SpaceStyles.top3]}
                        />
                    }

                    <FlatList
                        data={allComment}
                        showsVerticalScrollIndicator={false}
                        renderItem={renderComment}
                        keyboardShouldPersistTaps="handled"
                        keyExtractor={(_, index) => index.toString()}
                    />
                </View>
            </ScrollView>

            {token ?
                <CustomButton
                    containerStyle={[SpaceStyles.vertical2, { backgroundColor: WHITE, borderWidth: 2 }, SpaceStyles.spaceHorizontal]}
                    text={'POST A COMMENT'}
                    textStyle={TextStyles.bold12Black}
                    onPress={() => navigation.navigate('CommentScreen', {
                        detail: videoDetail
                    })}
                />
                :
                <CustomButton
                    containerStyle={[SpaceStyles.vertical2, SpaceStyles.spaceHorizontal]}
                    text={'SIGN IN TO COMMENT'}
                    onPress={() => navigation.navigate('LoginScreen')}
                />
            }

            <SafeAreaView />
        </View>
    )
}

export default VideoDetailScreen