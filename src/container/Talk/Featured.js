import React, {useState} from 'react';
import {
  Image,
  View,
  FlatList,
  RefreshControl,
  Platform,
  useWindowDimensions,
} from 'react-native';
import CustomText from '../../components/CustomText';
import CommonStyles from '../../style/CommonStyles';
import SpaceStyles from '../../style/SpaceStyles';
import TextStyles from '../../style/TextStyles';
import HTML from 'react-native-render-html';
import {BLACK, PRIMARY_COLOR, WHITE} from '../../constants/Colors';
import {shadow} from '../../constants/Images';
import NavigationService from '../../navigation/NavigationService';
import PostSkeleton from '../../components/Skeleton/PostSkeleton';
import {useSelector} from 'react-redux';
import FastImage from 'react-native-fast-image';
import {TouchableOpacity} from 'react-native-gesture-handler';
import BottomLoader from '../../components/BottomLoader';
import {MODERN} from '../../constants/GlobalFunction';
import {BannerAd, BannerAdSize, TestIds} from 'react-native-google-mobile-ads';

var moment = require('moment-timezone');
moment().tz('Europe/London').format();

function Featured({isRefresh, isScroll, page, flatListRefFeatured}) {
  const isIos = Platform.OS === 'ios';

  // const adUnitId = TestIds.BANNER;
  const adUnitId = isIos
    ? 'ca-app-pub-7858948897424048/2642863605'
    : 'ca-app-pub-7858948897424048/8272548515';
  const featuredData = useSelector(state => state.featured.featuredData);
  const featureLoading = useSelector(state => state.loading.featureLoading);
  const refreshLoading = useSelector(state => state.loading.refreshLoading);
  const featuredScrollLoading = useSelector(
    state => state.loading.featuredScrollLoading,
  );
  const featuredCountData = useSelector(
    state => state.featured.featuredCountData,
  );
  const themeData = useSelector(state => state.theme.themeData);
  const showAds = useSelector(state => state.ads.AdsData);
  const {width} = useWindowDimensions();

  const [adHeight, setAdHeight] = useState(false);

  const tagsStyle = {
    fontFamily: isIos ? 'Montserrat-Black' : 'Gotham Black Regular',
    fontSize: 13,
    color: WHITE,
    lineHeight: isIos ? 18 : 16,
    fontWeight: '900',
  };

  const newDesignStyle = {
    fontFamily: isIos ? 'Montserrat-Bold' : 'GothamBold',
    fontSize: 12,
    color: BLACK,
    lineHeight: isIos ? 18 : 16,
    fontWeight: '900',
  };

  const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
    const paddingToBottom = 20;
    return (
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom
    );
  };

  const handleScrollEnd = () => {
    if (!featuredScrollLoading && featuredCountData > page) {
      isScroll();
    }
  };

  const renderPagingLoader = () => {
    if (featuredScrollLoading) {
      return <BottomLoader />;
    } else {
      return null;
    }
  };

  const renderModernFeatured = ({item, index}) => {
    const originalMoment = moment(item.datetime, 'DD MMMM, YYYY HH:mm:ss');
    const targetDateString = originalMoment.format('YYYY-MM-DDTHH:mm:ss');
    let time = moment.tz(
      item.datetime.includes('T') ? item.datetime : targetDateString,
      'Europe/London',
    );
    return (
      <>
        <TouchableOpacity
        
          onPress={() =>{
            // console.log("=-=-=-=-=-++++++++>>>>>>>",item?.content)
            NavigationService.navigate('ArticleScreen', {
              detail: item,
              showADSscr :true
            })
          }}
          activeOpacity={0.6}
          key={index}
          style={SpaceStyles.spaceVertical}>
          <FastImage
            style={CommonStyles.videosBannerStyle}
            source={{
              uri: item?.image,
              priority: FastImage.priority.normal,
            }}
            resizeMode={FastImage.resizeMode.stretch}
          />
          <Image
            source={shadow}
            resizeMode={'stretch'}
            style={CommonStyles.positionDarkview}
          />
          <View style={CommonStyles.textPositionOnImage}>
            <View style={SpaceStyles.vertical1}>
              <HTML
                contentWidth={width}
                source={{html: item?.title?.toUpperCase()}}
                baseStyle={tagsStyle}
              />
            </View>
            <CustomText
              text={moment(time).fromNow(true)}
              style={[TextStyles.bold8DarkWhite]}
            />
          </View>
        </TouchableOpacity>
        {(index == 0 || index == featuredData?.length - 1) &&
          !adHeight &&
          showAds && (
            <BannerAd
              unitId={adUnitId}
              size="370x50"
              onAdFailedToLoad={() => setAdHeight(true)}
              requestOptions={{
                requestNonPersonalizedAdsOnly: true,
              }}
            />
          )}
      </>
    );
  };

  const renderClassicFeatured = ({item, index}) => {
    const originalMoment = moment(item.datetime, 'DD MMMM, YYYY HH:mm:ss');
    const targetDateString = originalMoment.format('YYYY-MM-DDTHH:mm:ss');
    let time = moment.tz(
      item.datetime.includes('T') ? item.datetime : targetDateString,
      'Europe/London',
    );
    return (
      <>
        <TouchableOpacity
          onPress={() =>
            NavigationService.navigate('ArticleScreen', {
              detail: item,
              showADSscr :true
            })
          }
          activeOpacity={0.6}
          key={index}
          style={[
            SpaceStyles.vertical1,
            SpaceStyles.rowFlex,
            SpaceStyles.spaceHorizontal,
          ]}>
          <FastImage
            style={CommonStyles.newsSmallImage}
            source={{
              uri: item?.image,
              priority: FastImage.priority.normal,
            }}
            resizeMode={FastImage.resizeMode.stretch}
          />
          <View style={CommonStyles.newsSecondDesign}>
            <View style={[SpaceStyles.width55]}>
              <HTML
                contentWidth={width}
                source={{html: item?.title?.toUpperCase()}}
                baseStyle={newDesignStyle}
              />
            </View>
            <View style={[SpaceStyles.top1]}>
              <CustomText
                text={moment(time).fromNow(true)}
                style={[TextStyles.bold8Brown]}
              />
            </View>
          </View>
        </TouchableOpacity>
        {(index == 0 || index == featuredData?.length - 1) &&
          !adHeight &&
          showAds && (
            <BannerAd
              unitId={adUnitId}
              size="370x50"
              onAdFailedToLoad={() => setAdHeight(true)}
              requestOptions={{
                requestNonPersonalizedAdsOnly: true,
              }}
            />
          )}
      </>
    );
  };

  return (
    <>
      {featureLoading ? (
        <PostSkeleton />
      ) : (
        <FlatList
          ref={flatListRefFeatured}
          data={featuredData}
          showsVerticalScrollIndicator={false}
          renderItem={
            themeData == MODERN ? renderModernFeatured : renderClassicFeatured
          }
          keyboardShouldPersistTaps="handled"
          keyExtractor={(_, index) => index.toString()}
          contentContainerStyle={
            themeData == MODERN ? SpaceStyles.spaceHorizontal : null
          }
          ItemSeparatorComponent={() => {
            return (
              <View
                style={
                  themeData == MODERN
                    ? CommonStyles.lineView
                    : CommonStyles.newsLineView
                }
              />
            );
          }}
          refreshControl={
            <RefreshControl
              refreshing={refreshLoading}
              tintColor={[PRIMARY_COLOR]}
              colors={[PRIMARY_COLOR]}
              onRefresh={() => isRefresh()}
            />
          }
          onMomentumScrollEnd={({nativeEvent}) => {
            if (isCloseToBottom(nativeEvent)) {
              handleScrollEnd();
            }
          }}
          scrollEventThrottle={400}
          ListFooterComponent={renderPagingLoader}
        />
      )}
    </>
  );
}

export default Featured;
