import React, {useState} from 'react';
import {
  Image,
  View,
  FlatList,
  RefreshControl,
  Platform,
  useWindowDimensions,
} from 'react-native';
import CustomText from '../../components/CustomText';
import NavigationService from '../../navigation/NavigationService';
import CommonStyles from '../../style/CommonStyles';
import SpaceStyles from '../../style/SpaceStyles';
import TextStyles from '../../style/TextStyles';
import {BLACK, PRIMARY_COLOR, WHITE} from '../../constants/Colors';
import {shadow} from '../../constants/Images';
import {useSelector} from 'react-redux';
import PostSkeleton from '../../components/Skeleton/PostSkeleton';
import BottomLoader from '../../components/BottomLoader';
import HTML from 'react-native-render-html';
import FastImage from 'react-native-fast-image';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {MODERN} from '../../constants/GlobalFunction';
import {BannerAd, BannerAdSize, TestIds} from 'react-native-google-mobile-ads';

var moment = require('moment-timezone');
moment().tz('Europe/London').format();

function LatestNews({isRefresh, isScroll, page, flatListRefNews}) {
  const isIos = Platform.OS === 'ios';
  // const adUnitId = TestIds.BANNER;
  const adUnitId = isIos
    ? 'ca-app-pub-7858948897424048/2642863605'
    : 'ca-app-pub-7858948897424048/8272548515';

  const newsData = useSelector(state => state.news.newsData);
  const newsLoading = useSelector(state => state.loading.newsLoading);
  const refreshLoading = useSelector(state => state.loading.refreshLoading);
  const newsScrollLoading = useSelector(
    state => state.loading.newsScrollLoading,
  );
  const newsCountData = useSelector(state => state.news.newsCountData);
  const themeData = useSelector(state => state.theme.themeData);
  const showAds = useSelector(state => state.ads.AdsData);
  const {width} = useWindowDimensions();

  const [adHeight, setAdHeight] = useState(false);

  const tagsStyle = {
    fontFamily: isIos ? 'Montserrat-Black' : 'Gotham Black Regular',
    fontSize: 13,
    color: WHITE,
    lineHeight: isIos ? 18 : 16,
    fontWeight: '900',
  };

  const newDesignStyle = {
    fontFamily: isIos ? 'Montserrat-Bold' : 'GothamBold',
    fontSize: 12,
    color: BLACK,
    lineHeight: isIos ? 18 : 16,
    fontWeight: '900',
  };

  const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
    const paddingToBottom = 20;
    return (
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom
    );
  };

  const handleScrollEnd = () => {
    if (!newsScrollLoading && newsCountData > page) {
      isScroll();
    }
  };

  const renderPagingLoader = () => {
    if (newsScrollLoading) {
      return <BottomLoader />;
    } else {
      return null;
    }
  };

  const renderModernNews = ({item, index}) => {
    let time = moment.tz(item.date, 'Europe/London');
    return (
      <>
        <TouchableOpacity
          onPress={() =>
            NavigationService.navigate('ArticleScreen', {
              detail: item,
              showADSscr:true
            })
          }
          activeOpacity={0.6}
          key={index}
          style={SpaceStyles.vertical1}>
          <FastImage
            style={CommonStyles.videosBannerStyle}
            source={{
              uri: item?.featured_image_src,
              priority: FastImage.priority.normal,
            }}
            resizeMode={FastImage.resizeMode.stretch}
          />
          <Image
            source={shadow}
            resizeMode={'stretch'}
            style={CommonStyles.positionDarkview}
          />
          <View style={CommonStyles.textPositionOnImage}>
            <View style={SpaceStyles.vertical1}>
              <HTML
                contentWidth={width}
                source={{html: item?.title?.rendered?.toUpperCase()}}
                baseStyle={tagsStyle}
              />
            </View>
            <CustomText
              text={moment(time).fromNow(true)}
              style={[TextStyles.bold8DarkWhite]}
            />
          </View>
        </TouchableOpacity>
        <View style={{width: '10%'}}>
          {(index == 0 || index % 6 == 0) && !adHeight && showAds && (
            <BannerAd
              unitId={adUnitId}
              size="370x50"
              onAdFailedToLoad={() => setAdHeight(true)}
              requestOptions={{
                requestNonPersonalizedAdsOnly: true,
              }}
            />
          )}
        </View>
      </>
    );
  };

  const renderClassicNews = ({item, index}) => {
    let time = moment.tz(item.date, 'Europe/London');
    return (
      <>
        <TouchableOpacity
          onPress={() =>
            NavigationService.navigate('ArticleScreen', {
              detail: item,
              showADSscr:true

            })
          }
          activeOpacity={0.6}
          key={index}
          style={[
            SpaceStyles.vertical1,
            SpaceStyles.rowFlex,
            SpaceStyles.spaceHorizontal,
          ]}>
          <FastImage
            style={CommonStyles.newsSmallImage}
            source={{
              uri: item?.featured_image_src,
              priority: FastImage.priority.normal,
            }}
            resizeMode={FastImage.resizeMode.stretch}
          />
          <View style={CommonStyles.newsSecondDesign}>
            <View style={[SpaceStyles.width55]}>
              <HTML
                contentWidth={width}
                source={{html: item?.title?.rendered?.toUpperCase()}}
                baseStyle={newDesignStyle}
              />
            </View>
            <View style={[SpaceStyles.top1]}>
              <CustomText
                text={moment(time).fromNow()}
                style={[TextStyles.bold8Brown]}
              />
            </View>
          </View>
        </TouchableOpacity>
        {(index == 0 || index % 6 == 0) && !adHeight && showAds && (
          <BannerAd
            unitId={adUnitId}
            size="370x50"
            onAdFailedToLoad={() => setAdHeight(true)}
            requestOptions={{
              requestNonPersonalizedAdsOnly: true,
            }}
          />
        )}
      </>
    );
  };

  return (
    <>
      {newsLoading ? (
        <PostSkeleton />
      ) : (
        <FlatList
          data={newsData}
          ref={flatListRefNews}
          showsVerticalScrollIndicator={false}
          renderItem={
            themeData == MODERN ? renderModernNews : renderClassicNews
          }
          keyExtractor={(_, index) => index.toString()}
          keyboardShouldPersistTaps="handled"
          contentContainerStyle={
            themeData == MODERN ? SpaceStyles.spaceHorizontal : null
          }
          ItemSeparatorComponent={() => {
            return (
              <View
                style={themeData == MODERN ? null : CommonStyles.newsLineView}
              />
            );
          }}
          refreshControl={
            <RefreshControl
              refreshing={refreshLoading}
              tintColor={[PRIMARY_COLOR]}
              colors={[PRIMARY_COLOR]}
              onRefresh={() => isRefresh()}
            />
          }
          onMomentumScrollEnd={({nativeEvent}) => {
            if (isCloseToBottom(nativeEvent)) {
              handleScrollEnd();
            }
          }}
          scrollEventThrottle={400}
          ListFooterComponent={renderPagingLoader}
        />
      )}
    </>
  );
}

export default LatestNews;
