import React, {useState} from 'react';
import {
  View,
  FlatList,
  RefreshControl,
  Platform,
  useWindowDimensions,
} from 'react-native';
import CommonStyles from '../../style/CommonStyles';
import SpaceStyles from '../../style/SpaceStyles';
import {DARK_BLACK, PRIMARY_COLOR} from '../../constants/Colors';
import {useSelector} from 'react-redux';
import PostSkeleton from '../../components/Skeleton/PostSkeleton';
import BottomLoader from '../../components/BottomLoader';
import NavigationService from '../../navigation/NavigationService';
import FastImage from 'react-native-fast-image';
import HTML from 'react-native-render-html';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {BannerAd, BannerAdSize, TestIds} from 'react-native-google-mobile-ads';

function Videos({isRefresh, isScroll, page, flatListRefVideo}) {
  const isIos = Platform.OS === 'ios';
  // const adUnitId = TestIds.BANNER;
  const adUnitId = isIos
    ? 'ca-app-pub-7858948897424048/2642863605'
    : 'ca-app-pub-7858948897424048/8272548515';

  const videoData = useSelector(state => state.video.videoData);
  const videoLoading = useSelector(state => state.loading.videoLoading);
  const refreshLoading = useSelector(state => state.loading.refreshLoading);
  const videoScrollLoading = useSelector(
    state => state.loading.videoScrollLoading,
  );
  const videoCountData = useSelector(state => state.video.videoCountData);
  const showAds = useSelector(state => state.ads.AdsData);
  const {width} = useWindowDimensions();

  const [adHeight, setAdHeight] = useState(false);

  const tagsStyle = {
    fontFamily: isIos ? 'Montserrat-Black' : 'Gotham Black Regular',
    fontSize: 13,
    color: DARK_BLACK,
    lineHeight: isIos ? 18 : 16,
    fontWeight: '900',
  };

  const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
    const paddingToBottom = 20;
    return (
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom
    );
  };

  const handleScrollEnd = () => {
    if (!videoScrollLoading && videoCountData > page) {
      isScroll();
    }
  };

  const renderPagingLoader = () => {
    if (videoScrollLoading) {
      return <BottomLoader />;
    } else {
      return null;
    }
  };

  const renderVideo = ({item, index}) => {
    return (
      <>
        <TouchableOpacity
          onPress={() =>
            NavigationService.navigate('VideoDetailScreen', {
              videoDetail: item,
            })
          }
          key={index}
          style={SpaceStyles.spaceVertical}>
          <FastImage
            style={CommonStyles.videosBannerStyle}
            source={{
              uri: item?.featured_image_src,
              priority: FastImage.priority.normal,
            }}
            resizeMode={FastImage.resizeMode.stretch}
          />
          <View style={SpaceStyles.top2}>
            <HTML
              contentWidth={width}
              source={{html: item?.title?.rendered?.toUpperCase()}}
              baseStyle={tagsStyle}
            />
          </View>
        </TouchableOpacity>
        {(index == 0 || index % 2 == 0) && !adHeight && showAds && (
          <BannerAd
            unitId={adUnitId}
            size="370x50"
            onAdFailedToLoad={() => setAdHeight(true)}
            requestOptions={{
              requestNonPersonalizedAdsOnly: true,
            }}
          />
        )}
      </>
    );
  };

  return (
    <>
      {videoLoading ? (
        <PostSkeleton />
      ) : (
        <FlatList
          data={videoData}
          ref={flatListRefVideo}
          showsVerticalScrollIndicator={false}
          renderItem={renderVideo}
          keyExtractor={(_, index) => index.toString()}
          keyboardShouldPersistTaps="handled"
          contentContainerStyle={SpaceStyles.spaceHorizontal}
          ItemSeparatorComponent={() => {
            return <View style={CommonStyles.lineView} />;
          }}
          refreshControl={
            <RefreshControl
              refreshing={refreshLoading}
              tintColor={[PRIMARY_COLOR]}
              colors={[PRIMARY_COLOR]}
              onRefresh={() => isRefresh()}
            />
          }
          onMomentumScrollEnd={({nativeEvent}) => {
            if (isCloseToBottom(nativeEvent)) {
              handleScrollEnd();
            }
          }}
          scrollEventThrottle={400}
          ListFooterComponent={renderPagingLoader}
        />
      )}
    </>
  );
}

export default Videos;
