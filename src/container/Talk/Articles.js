import React, {useState} from 'react';
import {
  Image,
  View,
  FlatList,
  RefreshControl,
  Platform,
  useWindowDimensions,
} from 'react-native';
import CustomText from '../../components/CustomText';
import NavigationService from '../../navigation/NavigationService';
import CommonStyles from '../../style/CommonStyles';
import SpaceStyles from '../../style/SpaceStyles';
import TextStyles from '../../style/TextStyles';
import {BLACK, PRIMARY_COLOR, WHITE} from '../../constants/Colors';
import {shadow} from '../../constants/Images';
import {useSelector} from 'react-redux';
import PostSkeleton from '../../components/Skeleton/PostSkeleton';
import BottomLoader from '../../components/BottomLoader';
import HTML from 'react-native-render-html';
import FastImage from 'react-native-fast-image';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {MODERN} from '../../constants/GlobalFunction';
var moment = require('moment-timezone');
moment().tz('Europe/London').format();
import {BannerAd, BannerAdSize, TestIds} from 'react-native-google-mobile-ads';

function Articles({isRefresh, isScroll, page, flatListRefArticle}) {
  const isIos = Platform.OS === 'ios';
  // const adUnitId = TestIds.BANNER;
  const adUnitId = isIos
    ? 'ca-app-pub-7858948897424048/2642863605'
    : 'ca-app-pub-7858948897424048/8272548515';

  const articlesData = useSelector(state => state.articles.articlesData);
  const articlesLoading = useSelector(state => state.loading.articlesLoading);
  const refreshLoading = useSelector(state => state.loading.refreshLoading);
  const articlesScrollLoading = useSelector(
    state => state.loading.articlesScrollLoading,
  );
  const articlesCountData = useSelector(
    state => state.articles.articlesCountData,
  );
  const themeData = useSelector(state => state.theme.themeData);
  const showAds = useSelector(state => state.ads.AdsData);
  const {width} = useWindowDimensions();

  const [adHeight, setAdHeight] = useState(false);

  const tagsStyle = {
    fontFamily: isIos ? 'Montserrat-Black' : 'Gotham Black Regular',
    fontSize: 13,
    color: WHITE,
    lineHeight: isIos ? 18 : 16,
    fontWeight: '900',
  };

  const newDesignStyle = {
    fontFamily: isIos ? 'Montserrat-Bold' : 'GothamBold',
    fontSize: 12,
    color: BLACK,
    lineHeight: isIos ? 18 : 16,
    fontWeight: '900',
  };

  const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
    const paddingToBottom = 20;
    return (
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom
    );
  };

  const handleScrollEnd = () => {
    if (!articlesScrollLoading && articlesCountData > page) {
      isScroll();
    }
  };

  const renderPagingLoader = () => {
    if (articlesScrollLoading) {
      return <BottomLoader />;
    } else {
      return null;
    }
  };

  const renderModernArticles = ({item, index}) => {
    let time = moment.tz(item.date, 'Europe/London');
    return (
      <>
        <TouchableOpacity
          onPress={() =>
            NavigationService.navigate('ArticleScreen', {
              detail: item,
              showADSscr:true

            })
          }
          activeOpacity={0.6}
          key={index}
          style={SpaceStyles.vertical1}>
          <FastImage
            style={CommonStyles.videosBannerStyle}
            source={{
              uri: item?.featured_image_src,
              priority: FastImage.priority.normal,
            }}
            resizeMode={FastImage.resizeMode.stretch}
          />
          <Image
            source={shadow}
            resizeMode={'stretch'}
            style={CommonStyles.positionDarkview}
          />
          <View style={CommonStyles.textPositionOnImage}>
            <View style={SpaceStyles.vertical1}>
              <HTML
                contentWidth={width}
                source={{html: item?.title?.rendered?.toUpperCase()}}
                baseStyle={tagsStyle}
              />
            </View>
            <CustomText
              text={moment(time).fromNow(true)}
              style={[TextStyles.bold8DarkWhite]}
            />
          </View>
        </TouchableOpacity>
        {(index == 0 || index % 6 == 0) && !adHeight && showAds && (
          <BannerAd
            unitId={adUnitId}
            size="370x50"
            onAdFailedToLoad={error => {
              setAdHeight(true);
            }}
            requestOptions={{
              requestNonPersonalizedAdsOnly: true,
            }}
          />
        )}
      </>
    );
  };

  const renderClassicArticles = ({item, index}) => {
    let time = moment.tz(item.date, 'Europe/London');
    return (
      <>
        <TouchableOpacity
          onPress={() =>
            NavigationService.navigate('ArticleScreen', {
              detail: item,
              showADSscr:true

            })
          }
          activeOpacity={0.6}
          key={index}
          style={[
            SpaceStyles.vertical1,
            SpaceStyles.rowFlex,
            SpaceStyles.spaceHorizontal,
          ]}>
          <FastImage
            style={CommonStyles.newsSmallImage}
            source={{
              uri: item?.featured_image_src,
              priority: FastImage.priority.normal,
            }}
            resizeMode={FastImage.resizeMode.stretch}
          />
          <View style={CommonStyles.newsSecondDesign}>
            <View style={[SpaceStyles.width55]}>
              <HTML
                contentWidth={width}
                source={{html: item?.title?.rendered?.toUpperCase()}}
                baseStyle={newDesignStyle}
              />
            </View>
            <View style={[SpaceStyles.top1]}>
              <CustomText
                text={moment(time).fromNow()}
                style={[TextStyles.bold8Brown]}
              />
            </View>
          </View>
        </TouchableOpacity>
        {(index == 0 || index % 6 == 0) && !adHeight && showAds && (
          <BannerAd
            unitId={adUnitId}
            size="370x50"
            onAdFailedToLoad={() => setAdHeight(true)}
            requestOptions={{
              requestNonPersonalizedAdsOnly: true,
            }}
          />
        )}
      </>
    );
  };

  return (
    <>
      {articlesLoading ? (
        <PostSkeleton />
      ) : (
        <FlatList
          data={articlesData}
          ref={flatListRefArticle}
          showsVerticalScrollIndicator={false}
          renderItem={
            themeData == MODERN ? renderModernArticles : renderClassicArticles
          }
          keyExtractor={(_, index) => index.toString()}
          keyboardShouldPersistTaps="handled"
          contentContainerStyle={
            themeData == MODERN ? SpaceStyles.spaceHorizontal : null
          }
          ItemSeparatorComponent={() => {
            return (
              <View
                style={themeData == MODERN ? null : CommonStyles.newsLineView}
              />
            );
          }}
          refreshControl={
            <RefreshControl
              refreshing={refreshLoading}
              tintColor={[PRIMARY_COLOR]}
              colors={[PRIMARY_COLOR]}
              onRefresh={() => isRefresh()}
            />
          }
          onMomentumScrollEnd={({nativeEvent}) => {
            if (isCloseToBottom(nativeEvent)) {
              handleScrollEnd();
            }
          }}
          scrollEventThrottle={400}
          ListFooterComponent={renderPagingLoader}
        />
      )}
    </>
  );
}

export default Articles;
