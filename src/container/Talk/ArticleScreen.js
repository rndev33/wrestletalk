import React, { useState, useEffect } from 'react';
import cheerio from 'cheerio';
import {
  View,
  Image,
  TouchableOpacity,
  Text,
  ScrollView,
  Share,
  Platform,
  FlatList,
  BackHandler,
  SafeAreaView,
  useWindowDimensions,
  Linking,
  ActivityIndicator,
  Modal,
} from 'react-native';
import DOMParser from 'react-native-html-parser';

import CustomText from '../../components/CustomText';
import HeaderComponent from '../../components/HeaderComponent';
import { shadow, shareIcon } from '../../constants/Images';
import CommonStyles from '../../style/CommonStyles';
import SpaceStyles from '../../style/SpaceStyles';
import TextStyles from '../../style/TextStyles';
import HTML, { TChildrenRenderer } from 'react-native-render-html';
import { MAIN_BLACK, PRIMARY_COLOR, WHITE } from '../../constants/Colors';
import CustomButton from '../../components/CustomButton';
import {
  getAllCommentApiCall,
  getTagsApi,
} from '../../redux/services/talkServices';
import { Variables } from '../../constants/Variables';
import MathjaxLoad from '../../components/MathjaxLoad';
import constants from '../../constants';
import { BannerAd, TestIds } from 'react-native-google-mobile-ads';
import { getData } from '../../constants/GlobalFunction';
import YoutubePlayer from 'react-native-youtube-iframe';
import { useSelector } from 'react-redux';
var moment = require('moment-timezone');
import HTMLView from 'react-native-htmlview';
import RenderHtml from 'react-native-render-html';

moment().tz('Europe/London').format();

function ArticleScreen(props) {
  const isIos = Platform.OS === 'ios';
  const adUnitId = TestIds.BANNER;
  // const adUnitId = isIos
  //   ? 'ca-app-pub-7858948897424048/2642863605'
  //   : 'ca-app-pub-7858948897424048/8272548515';
  const { navigation, route } = props;
  const [allComment, setAllComment] = useState('');
  const [show, setshow] = useState(false);
  const [quote, setquote] = useState('');
  const [rstromg, setrstromg] = useState('');
  const [ulrag, setultg] = useState('');
  const [token, setToken] = useState('');
  const [adHeight, setAdHeight] = useState(false);
  const [loading, setloading] = useState(false);
  const [detail, setDetail] = useState(route?.params?.detail);
  const WIDTH = constants.BaseStyle.DEVICE_WIDTH / 100;
  const showAds = useSelector(state => state.ads.AdsData);
  const { width } = useWindowDimensions();
  const [data, setData] = useState();
  let count = 0;
  // console.log("-=-=--=-=->>detaildetaildetaildetail>>>",detail?.title)
  useEffect(() => {
    const backAction = () => {
      backNavigate();
      return true;
    };
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );
    return () => backHandler.remove();
  }, []);

  const backNavigate = () => {
    setDetail('');
    navigation.goBack();
  };

  useEffect(() => {
    getComment();
    getToken();
    getTags();
  }, []);

  function AdComponent() {
    return (
      <View style={{ marginTop: 20 }}>
        <BannerAd
          unitId={adUnitId}
          size="370x200"
          onAdFailedToLoad={() => setAdHeight(true)}
          requestOptions={{
            requestNonPersonalizedAdsOnly: true,
          }}
        />
      </View>
    );
  }
  function PRenderer({ TDefaultRenderer, tnode, ...defaultRendererProps }) {
    count++;
    // console.log('=======tnodetnodetnode=>>>>>>>', tnode?.init?.domNode?.next?.next, '\n\n');
    return (
      <View>
        <TDefaultRenderer tnode={tnode} {...defaultRendererProps}>
          <TChildrenRenderer tchildren={tnode.children} />
          {count % 3 == 0 && tnode?.init?.domNode?.next?.next !== null ? <AdComponent /> : null}
        </TDefaultRenderer>
      </View>
    );
  }
  function ARenderer({ TDefaultRenderer, TText, tnode, ...defaultRendererProps }) {
    // console.log(
    //   '0-0-0-0-0--0-0-0-0->>>>',
    //   tnode?.init?.textNode?.parent?.attribs?.href,

    // );
    return (<Text onPress={() => {
      Linking.openURL(tnode?.init?.textNode?.parent?.attribs?.href);
    }} style={[{
      fontWeight: tnode?.__nativeStyles?.fontWeight,
      fontSize: tnode?.__nativeStyles?.fontSize,
      lineHeight: tnode?.__nativeStyles?.fontSize
    }, { color: '#BB291E', textDecorationLine: "none", }]}>
      {tnode?.init?.textNode?.data}
    </Text>
      // <TouchableOpacity
      // style={{alignItems:"center",justifyContent:'flex-end',backgroundColor:'transparent'}}

      //   onPress={() => {
      //     console.log(
      //       '0-0-0-0-0--0-0-0-0->>>>',
      //       tnode?.__nativeStyles

      //     );

      //     // Linking.openURL(tnode?.init?.textNode?.parent?.attribs?.href);
      //   }}>

      //   <Text style={[{fontWeight:tnode?.__nativeStyles?.fontWeight,
      //     fontSize:tnode?.__nativeStyles?.fontSize,
      //     lineHeight:tnode?.__nativeStyles?.fontSize
      //     }, {color: 'darkred',textDecorationLine:"none",}]}>
      //     {tnode?.init?.textNode?.data}
      //   </Text>

      // </TouchableOpacity>
    );
  }
  function blockquoteRender({
    TDefaultRenderer,
    tnode,
    ...defaultRendererProps
  }) {
    return (
      <MathjaxLoad
        html={`<!DOCTYPE html>
      <html>
      <body>

      ${quote.includes('instagram')
            ? `${quote}`
            : `${quote}`
          }

      <p><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script></p>
      <p><script async src="//www.instagram.com/embed.js"></script></p>
      </body>
      </html>`}
        style={{ backgroundColor: 'red', flex: 1 }}
      />
    );
    // <TouchableOpacity
    //   onPress={() => {
    //     setshow(true);
    //   }}>
    //   <Text>Click to see</Text>
    // </TouchableOpacity>
    // );
  }
  function strongRender({
    TDefaultRenderer,
    tnode,
    ...defaultRendererProps
  }) {
    return (
      <MathjaxLoad
        html={`<!DOCTYPE html>
      <html>
      <body>

      ${rstromg}

      <p><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script></p>
      <p><script async src="//www.instagram.com/embed.js"></script></p>
      </body>
      </html>`}
        style={{ backgroundColor: 'red', flex: 1 }}
      />
    );
    // <TouchableOpacity
    //   onPress={() => {
    //     setshow(true);
    //   }}>
    //   <Text>Click to see</Text>
    // </TouchableOpacity>
    // );
  }
  function imageRender({ TDefaultRenderer, tnode, ...defaultRendererProps }) {
    console.log("routerouterTDefaultRendereroute>>>> ", tnode?.init?.domNode?.attribs)

    return (
      <Image
        style={{ height: parseInt(tnode?.init?.domNode?.attribs?.height), width: '100%' }}
        source={{ uri: tnode?.init?.domNode?.attribs?.src }}
      />

    );
  }
  // function strongRender({ TDefaultRenderer, tnode, ...defaultRendererProps }) {
  //   console.log("routerouterTDefaultRendereroute>>>> ||||", tnode?.children?.slice(0, 2))

  //   return (
  //     <View style={{height:200}}>

     
  //     <TDefaultRenderer tnode={tnode} {...defaultRendererProps}>
  //       <TChildrenRenderer  tchildren={tnode.children} />
  //     </TDefaultRenderer>
  //     </View>

  //   );
  // }

  // console.log("routerouteroute>>>> ",route?. params?.showADSscr)
  const renderers = {
    p: PRenderer,
    a: ARenderer,
    blockquote: blockquoteRender,
    // strong: strongRender,
    // img: imageRender,
    // script: scriptRender,
  };

  const getTags = () => {
    setloading(false);

    getTagsApi()
      .then(res => {
        // console.log("-=-res?.datares?.datares?.data",res?.data)
        // setData(res?.data || {});
      })
      .catch(() => { })
      .finally(() => {
        setloading(false);
      });
  };
  const getToken = async () => {
    getData(Variables.accessToken)
      .then(res => {
        setToken(JSON.parse(res));
      })
      .catch(() => { });
  };

  const getComment = () => {
    getAllCommentApiCall(`?post=${route?.params?.detail.id}`)
      .then(res => {
        setAllComment(res.data);
      })
      .catch(() => { });
  };

  const commentStyle = {
    fontFamily: isIos ? 'Montserrat-Medium' : 'GothamMedium',
    fontSize: 12,
    color: MAIN_BLACK,
    lineHeight: isIos ? 17 : 15,
    fontWeight: '900',
  };

  const titleStyle = {
    fontFamily: isIos ? 'Montserrat-Black' : 'Gotham Black Regular',
    fontSize: 14,
    color: WHITE,
    letterSpacing: 0.5,
    lineHeight: isIos ? 19 : 17,
    fontWeight: '900',
  };

  const shareArticle = async (title, link) => {
    const regex = /(<([^>]+)>)/gi;
    try {
      const result = await Share.share({
        message: title?.split(' -')[0] + `\n` + link,
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
        } else {
        }
      } else if (result.action === Share.dismissedAction) {
      }
    } catch (error) {
      alert(error.message);
    }
  };

  const renderComment = ({ item }) => {
    return (
      <>
        <View style={[SpaceStyles.alignSpaceBlock, SpaceStyles.top2]}>
          <CustomText
            text={item?.author_name}
            style={[TextStyles.bold14MainBlack]}
          />
          <CustomText
            text={moment(item?.date).format('MMM DD, hh:mm a')}
            style={[TextStyles.medium10FullGray]}
          />
        </View>
        <HTML
          contentWidth={width}
          source={{ html: item?.content?.rendered?.split('</p>')[0] }}
          baseStyle={commentStyle}
        />
        <View style={[SpaceStyles.alignSpaceBlock, SpaceStyles.top2]}>
          <CustomText text={'REPLY'} style={[TextStyles.bold10Black]} />
          <View style={SpaceStyles.rowFlex}>
            <Image source={likeIcon} />
            <CustomText
              text={'454'}
              style={[TextStyles.bold12Black, SpaceStyles.left1]}
            />
            <Image source={disLikeIcon} style={SpaceStyles.left2} />
            <CustomText
              text={'454'}
              style={[TextStyles.bold10GrayText, SpaceStyles.left1]}
            />
          </View>
        </View>

        <View style={[SpaceStyles.rowFlex, SpaceStyles.top2]}>
          <Image source={downCommentArrow} />
          <View
            style={[
              SpaceStyles.alignSpaceBlock,
              SpaceStyles.top1,
              SpaceStyles.left2,
            ]}>
            <CustomText
              text={'Tim Burr'}
              style={[TextStyles.bold14LightBlack]}
            />
          </View>
        </View>

        <View style={SpaceStyles.left5}>
          <CustomText
            text={
              'His contract is said to be a bit of a surprise since so many WWE contracts have time added because of injuries or other reasons.'
            }
            style={[
              TextStyles.medium12LightBlack,
              SpaceStyles.top1,
              { lineHeight: 18 },
            ]}
          />
          <View style={[SpaceStyles.alignSpaceBlock, SpaceStyles.top2]}>
            <CustomText text={'REPLY'} style={[TextStyles.bold10Black]} />
            <View style={SpaceStyles.rowFlex}>
              <Image source={likeIcon} />
              <CustomText
                text={'454'}
                style={[TextStyles.bold12Black, SpaceStyles.left1]}
              />
              <Image source={disLikeIcon} style={SpaceStyles.left2} />
              <CustomText
                text={'454'}
                style={[TextStyles.bold10GrayText, SpaceStyles.left1]}
              />
            </View>
          </View>
        </View>
      </>
    );
  };
  const resultDate = detail?.datetime ? detail?.datetime : detail?.date;
  const originalMoment = moment(resultDate, 'DD MMMM, YYYY HH:mm:ss');
  const targetDateString = originalMoment.format('YYYY-MM-DDTHH:mm:ss');
  let time = moment.tz(
    resultDate?.includes('T') ? resultDate : targetDateString,
    'Europe/London',
  );

  function renderNode(node, index, siblings, parent, defaultRenderer) {
    if (node.name == 'iframe') {
      const a = node.attribs;
      const iframeHtml = `<iframe src="${a.src}"></iframe>`;
      return (
        <View
          key={index}
          style={{ width: Number(a.width), height: Number(a.height) }}>
          <WebView source={{ html: iframeHtml }} />
        </View>
      );
    }
  }
  let DomParser = require('react-native-html-parser').DOMParser;

  useEffect(() => {
    let html = detail?.content;
    // Your HTML string
    const htmlString =
      route?.params?.detail?.content?.rendered ||
      route?.params?.detail?.content ||
      detail?.content?.rendered ||
      detail?.content;
    console.log("htmlStringhtmlStringhtmlString>>>>", htmlString)
    // Load the HTML string into a cheerio instance
    const $ = cheerio.load(htmlString);

    const blockquotes = $('blockquote');
    const ul = $('ul');
    const strong = $('strong');
    // const script = $('p');
    // console.log('333333>>>>> --', blockquotes.html());


    setultg(ul.html());
    let extractedHTML = '';
    let stronghtml = '';

    // Loop through and append the HTML content of each <blockquote> element
    blockquotes?.each((index, element) => {

      extractedHTML += $.html(element);
    });
    strong?.each((index, element) => {

      stronghtml += $.html(element);
    });
    // console.log("333333>elementelement>>>>  -",extractedHTML)
    setquote(extractedHTML);
    setrstromg(stronghtml);
  }, []);

  const tagsStyles = {
    strong: {
      // color: "red",
      lineHeight: 122,
      // paddingVertical:30,
      
    }
  };
  return (
    <View style={CommonStyles.mainContainer}>
      <SafeAreaView style={{ backgroundColor: PRIMARY_COLOR }} />
      <HeaderComponent arrow={true} onArrow={() => backNavigate()} />
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={SpaceStyles.spaceHorizontal}>
        <View style={SpaceStyles.vertical1}>
          <Image
            resizeMode={'stretch'}
            source={{
              uri: detail?.featured_image_src || detail?.image,
            }}
            style={CommonStyles.videosBannerStyle}
          />
          <Image source={shadow} style={CommonStyles.positionDarkview} />

          <View style={CommonStyles.textPositionOnArticleImage}>
            <HTML
              contentWidth={width}
              source={{
                html:
                  detail?.title?.rendered?.toUpperCase() ||
                  detail?.title?.toUpperCase(),
              }}
              baseStyle={titleStyle}
            />
            <View style={[SpaceStyles.alignSpaceBlock, SpaceStyles.top1]}>
              <View style={SpaceStyles.rowFlex}>
                <CustomText
                  text={moment(time).fromNow(true)}
                  style={TextStyles.bold8DarkWhite}
                />
                <View style={[CommonStyles.verticalLine, SpaceStyles.left2]} />
                <CustomText
                  text={detail?.yoast_head_json?.author || detail?.author}
                  style={[TextStyles.bold8DarkWhite, SpaceStyles.left2]}
                />
              </View>
              <TouchableOpacity
                onPress={() => {
                  shareArticle(
                    detail?.yoast_head_json?.title || detail?.title,
                    detail?.link || detail?.url,
                  );
                }}>
                <Image source={shareIcon} />
              </TouchableOpacity>
            </View>
          </View>
        </View>
        {/* {!adHeight && showAds && (
          <BannerAd
            unitId={adUnitId}
            size="370x200"
            onAdFailedToLoad={() => setAdHeight(true)}
            requestOptions={{
              requestNonPersonalizedAdsOnly: true,
            }}
          />
        )} */}
        {/* <HTMLView value={docc} renderNode={renderNode} /> */}

        {route?.params?.showADSscr ? (
          <RenderHtml
            contentWidth={width}
            source={{
              html:
                route?.params?.detail?.content?.rendered ||
                route?.params?.detail?.content ||
                detail?.content?.rendered ||
                detail?.content,
            }}
            baseStyle={{
              fontFamily: isIos ? 'Montserrat-Black' : 'Gotham Black Regular',
              fontSize: 15,
              color: '#36454F',
              letterSpacing: 0.9,
              lineHeight: isIos ? 19 : 17,
              fontWeight: '300',
            }}
            // tagsStyles={tagsStyles}
            renderers={renderers}
          />
        ) : (
          <MathjaxLoad
            // html={detail?.content?.rendered || detail?.content}
            // html={`<blockquote class=\"twitter-tweet\" data-width=\"500\" data-dnt=\"true\">
            // <p lang=\"en\" dir=\"ltr\">Lexis King&#39;s (Brian Pillman Jr&#39;s) first entrance in <a href=\"https://twitter.com/hashtag/WWENXT?src=hash&amp;ref_src=twsrc%5Etfw\">#WWENXT</a> at <a href=\"https://twitter.com/hashtag/HalloweenHavoc?src=hash&amp;ref_src=twsrc%5Etfw\">#HalloweenHavoc</a> tonight!</p>
            // <p>What do you think about this presentation?<a href=\"https://t.co/H875pSclka\">pic.twitter.com/H875pSclka</a></p>
            // <p>&mdash; WrestleTalk (@WrestleTalk_TV) <a href=\"https://twitter.com/WrestleTalk_TV/status/1716976933710237973?ref_src=twsrc%5Etfw\">October 25, 2023</a></p></blockquote>`}
            html={
              (detail?.content?.rendered || detail?.content)?.includes(
                'related-story',
              )
                ? (
                  (detail?.content?.rendered || detail?.content)
                    ?.split('<aside')[0]
                    ?.split(1200)
                    ?.join(WIDTH * 90)
                    ?.split(675)
                    ?.join(225)
                    ?.split(`width: 1210px`)
                    ?.join(`width:${WIDTH * 90}`) +
                  (detail?.content?.rendered || detail?.content)?.split(
                    '</aside>',
                  )[1]
                )
                  ?.split(1200)
                  ?.join(WIDTH * 90)
                  ?.split(`width: 1210px`)
                  ?.join(`width:${WIDTH * 90}`)
                  ?.split(675)
                  ?.join(225)
                : (detail?.content?.rendered || detail?.content) &&
                (detail?.content?.rendered || detail?.content)
                  ?.split(1200)
                  ?.join(`${WIDTH * 90}`)
                  ?.split(`width: 1210px`)
                  ?.join(`width:${WIDTH * 90}`)
                  ?.split(675)
                  ?.join(225)
            }
            style={{ backgroundColor: 'red' }}
          />
        )}

        {(detail?.ACF?.you_tube?.split('embed/')[1]?.split('?start')[0] ||
          detail?.ACF?.you_tube?.split('embed/')[1]?.split('?feature')[0]) && (
            <YoutubePlayer
              height={230}
              play={false}
              videoId={
                detail?.ACF?.you_tube?.split('embed/')[1]?.split('?start')[0] ||
                detail?.ACF?.you_tube?.split('embed/')[1]?.split('?feature')[0]
              }
            />
          )}

        {detail?.ACF?.is_live_post && (
          <>
            {detail?.ACF?.updates?.map(i => {
              return (
                <>
                  <View
                    style={[CommonStyles.lineView, SpaceStyles.vertical1]}
                  />
                  <CustomText
                    text={i?.time}
                    style={TextStyles.medium14LightBorder}
                  />
                  <CustomText
                    text={i?.heading}
                    style={[TextStyles.bold18LightBlack, { marginTop: 3 }]}
                  />
                  <MathjaxLoad
                    html={i?.content}
                    style={{ backgroundColor: 'transparent' }}
                  />
                </>
              );
            })}
          </>
        )}
        {!adHeight && showAds && (
          <BannerAd
            unitId={adUnitId}
            size="370x50"
            onAdFailedToLoad={() => setAdHeight(true)}
            requestOptions={{
              requestNonPersonalizedAdsOnly: true,
            }}
          />
        )}

        {allComment?.length > 0 && (
          <Text>
            <CustomText
              text={`COMMENTS `}
              style={TextStyles.bold13TitleBlack}
            />
            <CustomText
              text={allComment?.length}
              style={TextStyles.bold13Red}
            />
          </Text>
        )}

        <FlatList
          data={allComment}
          showsVerticalScrollIndicator={false}
          renderItem={renderComment}
          keyboardShouldPersistTaps="handled"
          keyExtractor={(_, index) => index.toString()}
        />

        {/* {show && (
          <MathjaxLoad
            html={detail?.content}
            style={{backgroundColor: 'red'}}
          />
        )} */}
        {token ? (
          <CustomButton
            containerStyle={[
              SpaceStyles.vertical2,
              { backgroundColor: WHITE, borderWidth: 2 },
            ]}
            text={'POST A COMMENT'}
            textStyle={TextStyles.bold12Black}
            onPress={() =>
              navigation.navigate('CommentScreen', {
                detail: detail,
              })
            }
          />
        ) : (
          <CustomButton
            containerStyle={SpaceStyles.vertical2}
            text={'SIGN IN TO COMMENT'}
            onPress={() => navigation.navigate('LoginScreen')}
          />
        )}
      </ScrollView>
      <SafeAreaView />
      <Modal
        animationType="slide"
        transparent={true}
        visible={show}
        onRequestClose={() => { }}>
        {show && (
          <View
            style={{
              flex: 1,
              backgroundColor: 'white',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <TouchableOpacity
              style={{
                position: 'absolute',
                top: 18,
                right: 18,
                borderRadius: 2,
                padding: 8,
                backgroundColor: 'gray',
                zIndex: 9999,
              }}
              onPress={() => {
                setshow(false);
              }}>
              <Text>X</Text>
            </TouchableOpacity>
            <MathjaxLoad
              html={detail?.content}
              style={{ backgroundColor: 'red', flex: 1 }}
            />
          </View>
        )}
      </Modal>
    </View>
  );
}

export default ArticleScreen;
