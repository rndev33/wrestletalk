import React, { useState, useEffect } from "react";
import { View, TouchableOpacity, Alert } from 'react-native'
import CommonStyles from "../../style/CommonStyles";
import HeaderComponent from "../../components/HeaderComponent";
import CustomText from "../../components/CustomText";
import TextStyles from "../../style/TextStyles";
import CustomTextInput from "../../components/CustomTextInput";
import SpaceStyles from "../../style/SpaceStyles";
import axios from "axios";
import { SafeAreaView } from "react-native";
import { PRIMARY_COLOR } from "../../constants/Colors";
import { Variables } from "../../constants/Variables";
import { getData, ToastAlert } from "../../constants/GlobalFunction";
import LoaderModal from "../../components/LoaderModal";
import { getUserDetails } from "../../redux/services/AuthServices";

function CommentScreen({ navigation, route }) {
    const { detail } = route?.params


    const [comment, setComment] = useState('')
    const [loading, setLoading] = useState(false)

    const postComment = async () => {
        setLoading(true)

        let emailData = await getData(Variables.email)
        let email = JSON.parse(emailData)


        const endPoint = `https://wrestletalk.com/wp-json/wp/v2/comments`





        const { data: userInformation } = await getUserDetails(`os=${Platform.OS}&token=${email}`)

        let { user_login } = JSON.parse(`${userInformation}`)


        const data = {
            content: comment,
            post: detail.id,
            author_email: email,
            author_name: user_login
        }

        console.log(data)


        axios.post(endPoint, data)
            .then((res) => {
                console.log(res, "=====comment====");
                setLoading(false)
                setTimeout(() => {
                    ToastAlert('Comment posted')
                }, 500)

            })
            .catch((err) => {
                setLoading(false)
                console.log(err.response, "message error");
                setTimeout(() => {
                    ToastAlert('Error posting comment')
                }, 500)

            });
    }

    return (
        <View style={CommonStyles.mainContainer}>
            <SafeAreaView style={{ backgroundColor: PRIMARY_COLOR }} />
            <HeaderComponent />
            <View style={SpaceStyles.spaceHorizontal}>
                <CustomTextInput
                    placeholder={'Write Comment...'}
                    defaultValue={comment}
                    containerStyle={SpaceStyles.top3}
                    onChangeText={(text) => setComment(text)}
                    numberOfLines={7}
                />

                <View style={[SpaceStyles.alignSpaceBlock, SpaceStyles.top3]}>
                    <TouchableOpacity
                        onPress={() => navigation.goBack()}
                        style={CommonStyles.addFreeButton}>
                        <CustomText
                            text={'BACK TO ARTICLE'}
                            style={[TextStyles.medium12White, { textAlign: 'center' }]}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => postComment()}
                        style={CommonStyles.addFreeButton}>
                        <CustomText
                            text={'SUBMIT COMMENT'}
                            style={[TextStyles.medium12White, { textAlign: 'center' }]}

                        />
                    </TouchableOpacity>
                </View>
            </View>
            <LoaderModal loading={loading} />
        </View>
    )
}

export default CommentScreen