import React from "react";
import { View, FlatList, RefreshControl, Platform, useWindowDimensions } from 'react-native'
import CustomText from "../../components/CustomText";
import CommonStyles from "../../style/CommonStyles";
import SpaceStyles from "../../style/SpaceStyles";
import TextStyles from "../../style/TextStyles";
import { useSelector } from "react-redux";
import { DARK_BLACK, PRIMARY_COLOR } from "../../constants/Colors";
import PostSkeleton from "../../components/Skeleton/PostSkeleton";
import BottomLoader from "../../components/BottomLoader";
import moment from "moment";
import { WebView } from 'react-native-webview';
import HTML from "react-native-render-html";

function Audio({ isRefresh, isScroll, page, flatListRefAudio }) {
    const audioData = useSelector(state => state.audio.audioData);
    const audioLoading = useSelector(state => state.loading.audioLoading);
    const refreshLoading = useSelector(state => state.loading.refreshLoading);
    const audioScrollLoading = useSelector(state => state.loading.audioScrollLoading);
    const audioCountData = useSelector(state => state.audio.audioCountData);
    const isIos = Platform.OS === 'ios'
    const {width}=useWindowDimensions()


    const tagsStyle = {
        fontFamily: isIos ? 'Montserrat-Black' : 'Gotham Black Regular',
        fontSize: 13,
        color: DARK_BLACK,
        lineHeight: isIos ? 18 : 16,
        fontWeight :'900'
    };

    const isCloseToBottom = ({
        layoutMeasurement,
        contentOffset,
        contentSize,
    }) => {
        const paddingToBottom = 20;
        return (
            layoutMeasurement.height + contentOffset.y >=
            contentSize.height - paddingToBottom
        );
    }

    const handleScrollEnd = () => {
        if (!audioScrollLoading && audioCountData > page) {
            isScroll()
        }
    }

    const renderPagingLoader = () => {
        if (audioScrollLoading) {
            return <BottomLoader />;
        } else {
            return null;
        }
    }

    // const Iframe = `<iframe style="border: none; overflow: hidden;" title="Embed Player" src=${item.content?.rendered?.split('src=')[1]?.split(' ')[0]} width="100%" height="80px" frameborder="0" scrolling="no"></iframe>`

    const renderAudio = ({ item, index }) => {
        const Iframe = `<iframe style="border: none; overflow: hidden;" title="Embed Player" src=${item.content?.rendered?.split('src=')[1]?.split(' ')[0]} frameborder="0" scrolling="no"></iframe>`
        // const Iframe = `<iframe style="border: none; overflow: hidden;" title="Embed Player" src=${item.content?.rendered?.split('src=')[1]?.split(' ')[0]} width="100%" height="80px" frameborder="0" scrolling="no"></iframe>`
        return (
            <View
                key={index}
                style={SpaceStyles.spaceVertical}>
                <WebView
                    source={{ html: Iframe }}
                    style={{ height: 180, width: 1100 }}
                    javaScriptEnabled={true}
                    originWhitelist={['*']}
                    domStorageEnabled={true}
                    scrollEnabled={false}
                    automaticallyAdjustContentInsets={true}
                    javaScriptEnabledAndroid={true}
                    startInLoadingState={true}
                    androidHardwareAccelerationDisabled={true}
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                />
                <View style={SpaceStyles.top2}>
                    <HTML
                    contentWidth={width}
                        source={{ html: item?.title?.rendered?.toUpperCase() }}
                        baseStyle={tagsStyle}
                    />
                </View>
                {/* <View style={[SpaceStyles.rowFlex]}>
                    <CustomText
                        text={moment(item?.date).fromNow(true)}
                        style={TextStyles.bold8Brown}
                    />
                    <CustomText
                        text={'    |    '}
                        style={TextStyles.bold8Brown}
                    />
                    <CustomText
                        text={item?.yoast_head_json?.twitter_misc['Written by']}
                        style={TextStyles.bold8Brown}
                    />
                </View> */}
            </View>
        )
    }

    return (
        <>
            {audioLoading ?
                <PostSkeleton />
                :
                <FlatList
                    data={audioData}
                    ref={flatListRefAudio}
                    showsVerticalScrollIndicator={false}
                    renderItem={renderAudio}
                    keyboardShouldPersistTaps="handled"
                    contentContainerStyle={SpaceStyles.spaceHorizontal}
                    keyExtractor={(_, index) => index.toString()}
                    ItemSeparatorComponent={() => {
                        return (
                            <View style={CommonStyles.lineView} />
                        )
                    }}
                    refreshControl={
                        <RefreshControl
                            refreshing={refreshLoading}
                            tintColor={[PRIMARY_COLOR]}
                            colors={[PRIMARY_COLOR]}
                            onRefresh={() => isRefresh()}
                        />
                    }
                    onMomentumScrollEnd={({ nativeEvent }) => {
                        if (isCloseToBottom(nativeEvent)) {
                            handleScrollEnd();
                        }
                    }}
                    scrollEventThrottle={400}
                    ListFooterComponent={renderPagingLoader}
                />
            }
        </>
    )
}

export default Audio