import React, { useState, useEffect } from "react";
import { View, Image, TextInput, Text, FlatList, TouchableOpacity, RefreshControl, Platform, useWindowDimensions } from 'react-native'
import CustomText from "../../components/CustomText";
import { GRAY_TEXT, PRIMARY_COLOR, WHITE } from "../../constants/Colors";
import { cancelIcon, downArrow, noSearchIcon, smallShadow, verticalMenu } from "../../constants/Images";
import CommonStyles from "../../style/CommonStyles";
import SpaceStyles from "../../style/SpaceStyles";
import TextStyles from "../../style/TextStyles";
import SearchSkeleton from "../../components/Skeleton/SearchSkeleton";
import { useDispatch, useSelector } from "react-redux";
import { getAllSearchData } from "../../redux/action/SearchAction";
import BottomLoader from "../../components/BottomLoader";
import HTML from "react-native-render-html";
import NavigationService from "../../navigation/NavigationService";
import { Menu, MenuItem, MenuDivider } from 'react-native-material-menu';
import { ARTICLES, AUDIO, FEATURED, LATEST_NEWS, VIDEOS } from "../../constants/GlobalFunction";
import { SafeAreaView } from "react-native";
var moment = require('moment-timezone');
moment().tz("Europe/London").format();

let searchPage = 1;

function SearchScreen(props) {
    const { navigation, route } = props
    const { searchText } = route?.params
    const isIos = Platform.OS === 'ios'
    const [searchName, setSearchName] = useState('')
    const [isScrollSearch, setIsScrollSearch] = useState(false);
    const [totalSearch, setTotalSearch] = useState(0)
    const [totalResult, setTotalResult] = useState(0)
    const [openOrderMenu, setOpenOrderMenu] = useState(false)
    const [categoriesMenu, setCategoriesMenu] = useState(false)
    const [defaultOrder, setDefaultOrder] = useState('desc')
    const [defaultCategory, setDefaultCategory] = useState('')

    const dispatch = useDispatch()
    const searchData = useSelector(state => state.search.searchData);
    const searchLoading = useSelector(state => state.loading.searchLoading);
    const refreshLoading = useSelector(state => state.loading.refreshLoading);
    const searchScrollLoading = useSelector(state => state.loading.searchScrollLoading);
    const {width}=useWindowDimensions()


    useEffect(() => {
        setSearchName(searchText)
        getSearchFun(false, false, searchText)
    }, [])

    const tagsStyle = {
        fontFamily: isIos ? 'Montserrat-Black' : 'Gotham Black Regular',
        fontSize: 10,
        color: WHITE,
        lineHeight: isIos ? 15 : 13,
        fontWeight :'900'

    };

    useEffect(() => {
        if (searchData?.length == 0) {
            setTotalResult(0)
        }
    }, [searchData])

    const renderSearchData = ({ item, index }) => {
        let time = moment.tz(item.date, 'Europe/London')
        return (
            <TouchableOpacity
                onPress={() => NavigationService.navigate('ArticleScreen', {
                    detail: item,
                    showADSscr :true
                })}
                key={index}
                activeOpacity={0.6}
                style={CommonStyles.searchItemView}>
                <Image
                    style={CommonStyles.searchItem}
                    source={{
                        uri: item?.featured_image_src
                    }}
                    resizeMode={'stretch'}
                />
                <Image source={smallShadow} resizeMode={'stretch'} style={CommonStyles.positionDarkviewSearch} />
                <View style={CommonStyles.positionSearchContent}>
                    <View style={SpaceStyles.vertical1}>
                        <HTML
                        contentWidth={width}
                            source={{ html: item?.title?.rendered?.toUpperCase() }}
                            baseStyle={tagsStyle}
                        />
                    </View>
                    <CustomText
                        text={moment(time).fromNow(true)}
                        style={TextStyles.bold8DarkWhite}
                    />
                </View>
            </TouchableOpacity >
        )
    }

    const renderEmptyComponent = () => {
        return (
            <View style={CommonStyles.noSearchView}>
                <Image
                    source={noSearchIcon}
                    resizeMode={'contain'}
                />
                <CustomText
                    text={'NO RESULTS'}
                    style={[TextStyles.bold13DarkBorder, SpaceStyles.vertical1]}
                />
                <CustomText
                    text={'No articles, podcasts, or videos'}
                    style={TextStyles.medium12LightGray}
                />
                <CustomText
                    text={'match with your search'}
                    style={TextStyles.medium12LightGray}
                />
            </View>
        )
    }

    const getSearchFun = (isRefresh = false, isScrollEvent = false, name = searchName, order = defaultOrder, categoryId = defaultCategory) => {
        let finalQuery = {}

        if (searchScrollLoading && isScrollSearch) {
            return
        }
        if (isRefresh) {
            searchPage = 1
        }

        let skip = searchPage * 1;
        let limit = 10

        finalQuery.page = skip
        finalQuery.per_page = limit
        finalQuery.search = name
        finalQuery.order = order
        if (categoryId != '') {
            finalQuery.categories = categoryId
        }

        let query = ""
        Object.keys(finalQuery).forEach((key, index) => {
            query += `${index === 0 ? "?" : "&"}${key}=${finalQuery[key]}`;
        });
        dispatch(getAllSearchData(query, isRefresh, successSearch, failedSearch, isScrollEvent))
    }

    const failedSearch = () => {
        setIsScrollSearch(true);
    };

    const successSearch = (totalPage, totalCount) => {
        searchPage += 1;
        setIsScrollSearch(false);
        setTotalSearch(totalPage)
        setTotalResult(totalCount)
    };

    const isCloseToBottom = ({
        layoutMeasurement,
        contentOffset,
        contentSize,
    }) => {
        const paddingToBottom = 20;
        return (
            layoutMeasurement.height + contentOffset.y >=
            contentSize.height - paddingToBottom
        );
    }
    const handleScrollEnd = () => {
        if (!searchScrollLoading && totalSearch > searchPage) {
            getSearchFun(false, true)
        }
    }

    const renderPagingLoader = () => {
        if (searchScrollLoading) {
            return <BottomLoader />;
        } else {
            return null;
        }
    }

    return (
        <View style={CommonStyles.mainContainer}>
            <SafeAreaView style={{ backgroundColor: PRIMARY_COLOR }} />
            <View style={CommonStyles.headerView}>
                <View style={CommonStyles.searchInputView}>
                    <TextInput
                        style={[isIos ? TextStyles.medium12MainBlack : TextStyles.medium10GrayText, { flex: 1 }]}
                        placeholder="Search"
                        defaultValue={searchName}
                        placeholderTextColor={GRAY_TEXT}
                        textAlignVertical={'center'}
                        onChangeText={(name) => { searchPage = 1, getSearchFun(false, false, name), setSearchName(name) }}
                    />
                </View>
                <TouchableOpacity
                    onPress={() => { setSearchName(''), navigation.goBack() }}
                    style={CommonStyles.iconStyle}>
                    <Image
                        source={cancelIcon}
                        resizeMode={'contain'}
                    />
                </TouchableOpacity>
                <TouchableOpacity style={CommonStyles.iconStyle}>
                    <Image
                        source={verticalMenu}
                        resizeMode={'contain'}
                    />
                </TouchableOpacity>
            </View>
            <View style={CommonStyles.filterSearchView}>
                <TouchableOpacity
                    onPress={() => setCategoriesMenu(true)}
                    style={SpaceStyles.rowFlex}>
                    <CustomText
                        text={'CATEGORIES'}
                        style={TextStyles.bold10White}
                    />
                    <Image source={downArrow} style={SpaceStyles.left2} />
                </TouchableOpacity>

                <View style={CommonStyles.dateMenuPositionStyle}>
                    <Menu
                        anchor={() => {
                            <Image
                                resizeMode={'contain'}
                                source={downArrow}
                            />
                        }}
                        visible={categoriesMenu}
                        onRequestClose={() => setCategoriesMenu(false)}
                        style={SpaceStyles.width43}
                    >
                        <MenuItem onPress={() => {
                            setCategoriesMenu(false)
                            searchPage = 1
                            setDefaultCategory("27,1")
                            getSearchFun(false, false, searchName, defaultOrder, "27,1")
                        }}
                        >
                            <CustomText
                                text={LATEST_NEWS}
                                style={TextStyles.bold10Black}
                            />
                        </MenuItem>
                        <MenuDivider />
                        <MenuItem onPress={() => {
                            setCategoriesMenu(false)
                            searchPage = 1
                            setDefaultCategory(28)
                            getSearchFun(false, false, searchName, defaultOrder, 28)
                        }}
                        >
                            <CustomText
                                text={ARTICLES}
                                style={TextStyles.bold10Black}
                            />
                        </MenuItem>
                        <MenuDivider />
                        <MenuItem onPress={() => {
                            setCategoriesMenu(false)
                            searchPage = 1
                            setDefaultCategory(255)
                            getSearchFun(false, false, searchName, defaultOrder, 255)
                        }}
                        >
                            <CustomText
                                text={VIDEOS}
                                style={TextStyles.bold10Black}
                            />
                        </MenuItem>
                        <MenuDivider />
                        <MenuItem onPress={() => {
                            setCategoriesMenu(false)
                            searchPage = 1
                            setDefaultCategory(11018)
                            getSearchFun(false, false, searchName, defaultOrder, 11018)
                        }}
                        >
                            <CustomText
                                text={AUDIO}
                                style={TextStyles.bold10Black}
                            />
                        </MenuItem>
                    </Menu>
                </View>

                <TouchableOpacity
                    onPress={() => setOpenOrderMenu(true)}
                    style={SpaceStyles.rowFlex}>
                    <CustomText
                        text={'ORDER'}
                        style={TextStyles.bold10White}
                    />
                    <Image source={downArrow} style={SpaceStyles.left2} />
                </TouchableOpacity>

                <View style={CommonStyles.sizeMenuPositionStyle}>
                    <Menu
                        anchor={() => {
                            <Image
                                resizeMode={'contain'}
                                source={downArrow}
                            />
                        }}
                        visible={openOrderMenu}
                        onRequestClose={() => setOpenOrderMenu(false)}
                        style={SpaceStyles.width43}
                    >
                        <MenuItem onPress={() => {
                            setOpenOrderMenu(false)
                            searchPage = 1
                            setDefaultOrder('desc')
                            getSearchFun(false, false, searchName, 'desc')
                        }}>
                            <CustomText
                                text={'A-Z'}
                                style={TextStyles.bold10Black}
                            />
                        </MenuItem>
                        <MenuDivider />
                        <MenuItem onPress={() => {
                            setOpenOrderMenu(false)
                            searchPage = 1
                            setDefaultOrder('asc')
                            getSearchFun(false, false, searchName, 'asc')
                        }}>
                            <CustomText
                                text={'Z-A'}
                                style={TextStyles.bold10Black}
                            />
                        </MenuItem>
                        <MenuDivider />

                    </Menu>
                </View>
            </View>
            {searchName != '' &&
                <View style={CommonStyles.serachResultView}>
                    <Text>
                        <CustomText
                            text={`ABOUT ${totalResult} RESULTS FOR `}
                            style={TextStyles.medium10GrayText}
                        />
                        <CustomText
                            text={`‘${searchName}’`}
                            style={TextStyles.bold10GrayText}
                        />
                    </Text>
                </View>
            }
            {searchLoading ?
                <SearchSkeleton />
                :
                <FlatList
                    data={searchName == '' ? [] : searchData}
                    keyExtractor={(_, index) => index.toString()}
                    keyboardShouldPersistTaps="handled"
                    showsVerticalScrollIndicator={false}
                    numColumns={2}
                    renderItem={renderSearchData}
                    contentContainerStyle={[SpaceStyles.spaceHorizontal, SpaceStyles.paddingTop2]}
                    ListEmptyComponent={() => renderEmptyComponent()}
                    refreshControl={
                        <RefreshControl
                            refreshing={refreshLoading}
                            tintColor={[PRIMARY_COLOR]}
                            colors={[PRIMARY_COLOR]}
                            onRefresh={() => getSearchFun(true)}
                        />
                    }
                    onMomentumScrollEnd={({ nativeEvent }) => {
                        if (isCloseToBottom(nativeEvent)) {
                            handleScrollEnd();
                        }
                    }}
                    scrollEventThrottle={400}
                    ListFooterComponent={renderPagingLoader}
                />
            }
        </View>
    )
}

export default SearchScreen