import React, {useEffect, useRef, useState} from 'react';
import {View} from 'react-native';
import HeaderComponent from '../../components/HeaderComponent';
import TopTabComponent from '../../components/TopTabComponent';
import {
  ARTICLES,
  AUDIO,
  FEATURED,
  LATEST_NEWS,
  VIDEOS,
} from '../../constants/GlobalFunction';
import CommonStyles from '../../style/CommonStyles';
import Audio from './Audio';
import Featured from './Featured';
import LatestNews from './LatestNews';
import Videos from './Videos';
import {useDispatch, useSelector} from 'react-redux';
import {getAllFeaturedData} from '../../redux/action/FeaturedAction';
import {getAllNewsData} from '../../redux/action/NewsAction';
import {getAllVideoData} from '../../redux/action/VideAction';
import {getAllAudioData} from '../../redux/action/AudioAction';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import {SafeAreaView} from 'react-native';
import {PRIMARY_COLOR} from '../../constants/Colors';
import {localNotificationService} from '../../redux/services/localNotificationService';
import messaging from '@react-native-firebase/messaging';
import {getPostDetailApiCall} from '../../redux/services/talkServices';
import NavigationService from '../../navigation/NavigationService';
import Articles from './Articles';
import {getAllArticlesData} from '../../redux/action/ArticlesAction';
import {getAdsData} from '../../redux/action/AdsAction';

let videoPage = 2;
let newsPage = 2;
let articlesPage = 2;
let audioPage = 2;
let featuredPage = 1;

function TalkScreen(props) {
  const {navigation, route} = props;
  const dispatch = useDispatch();
  const videoScrollLoading = useSelector(
    state => state.loading.videoScrollLoading,
  );
  const newsScrollLoading = useSelector(
    state => state.loading.newsScrollLoading,
  );
  const articlesScrollLoading = useSelector(
    state => state.loading.articlesScrollLoading,
  );
  const audioScrollLoading = useSelector(
    state => state.loading.audioScrollLoading,
  );
  const featuredScrollLoading = useSelector(
    state => state.loading.featuredScrollLoading,
  );

  const [isScrollVideo, setIsScrollVideo] = useState(false);
  const [isScrollNews, setIsScrollNews] = useState(false);
  const [isScrollAudio, setIsScrollAudio] = useState(false);
  const [isScrollFeatured, setIsScrollFeatured] = useState(false);
  const [isScrollArticles, setIsScrollArticles] = useState(false);

  const flatListRefFeatured = useRef(null);
  const flatListRefNews = useRef(null);
  const flatListRefArticle = useRef(null);
  const flatListRefVideo = useRef(null);
  const flatListRefAudio = useRef(null);

  useEffect(() => {
    videoPage = 2;
    newsPage = 2;
    audioPage = 2;
    featuredPage = 2;
    articlesPage = 2;

    dispatch(getAdsData());

    const messageHandler = messaging().onMessage(async remoteMessage => {
      showLocalNotification(remoteMessage);
    });
    localNotificationService.configure(
      onRegister,
      onNotification,
      onOpenNotification,
    );
    return () => {
      messageHandler();
    };
  }, []);

  const onRegister = () => {};
  const onNotification = () => {};
  const onOpenNotification = data => {
    NavigationService.reset('RouteHomeStack');
    getPostDetailApiCall(data?.data?.id).then(res => {
      NavigationService.navigate('ArticleScreen', {
        detail: res.data,
        showADSscr :true
      });
    });
  };

  const showLocalNotification = notificationData => {
    const {messageId, data} = notificationData;
    const {title, body} = notificationData.notification;
    localNotificationService.showNotification(messageId, title, body, data);
  };

  const getFeaturedCall = (isRefresh = false, isScrollEvent = false) => {
    let finalQuery = {};
    if (featuredScrollLoading || isScrollFeatured) {
      return;
    }
    if (isRefresh) {
      featuredPage = 1;
    }

    let skip = featuredPage * 1;
    let limit = 10;

    finalQuery.page = skip;
    finalQuery.per_page = limit;
    finalQuery.categories = 11384;

    let query = '';
    Object.keys(finalQuery).forEach((key, index) => {
      query += `${index === 0 ? '?' : '&'}${key}=${finalQuery[key]}`;
    });
    dispatch(
      getAllFeaturedData(
        query,
        isRefresh,
        successFeatured,
        failedFeatured,
        isScrollEvent,
      ),
    );
  };

  const failedFeatured = () => {
    setIsScrollFeatured(true);
  };

  const successFeatured = () => {
    featuredPage += 1;
    setIsScrollFeatured(false);
  };

  const getNewsCall = (isRefresh = false, isScrollEvent = false) => {
    let finalQuery = {};
    if (newsScrollLoading || isScrollNews) {
      return;
    }
    if (isRefresh) {
      newsPage = 1;
    }

    let skip = newsPage * 1;
    let limit = 10;

    finalQuery.page = skip;
    finalQuery.per_page = limit;
    finalQuery.categories = '27, 1';

    let query = '';
    Object.keys(finalQuery).forEach((key, index) => {
      query += `${index === 0 ? '?' : '&'}${key}=${finalQuery[key]}`;
    });
    dispatch(
      getAllNewsData(query, isRefresh, successNews, failedNews, isScrollEvent),
    );
  };

  const failedNews = () => {
    setIsScrollNews(true);
  };

  const successNews = () => {
    newsPage += 1;
    setIsScrollNews(false);
  };

  const getArticlesCall = (isRefresh = false, isScrollEvent = false) => {
    let finalQuery = {};
    if (articlesScrollLoading || isScrollArticles) {
      return;
    }
    if (isRefresh) {
      articlesPage = 1;
    }

    let skip = articlesPage * 1;
    let limit = 10;

    finalQuery.page = skip;
    finalQuery.per_page = limit;
    finalQuery.categories = 28;

    let query = '';
    Object.keys(finalQuery).forEach((key, index) => {
      query += `${index === 0 ? '?' : '&'}${key}=${finalQuery[key]}`;
    });
    dispatch(
      getAllArticlesData(
        query,
        isRefresh,
        successArticles,
        failedArticles,
        isScrollEvent,
      ),
    );
  };

  const failedArticles = () => {
    setIsScrollArticles(true);
  };

  const successArticles = () => {
    articlesPage += 1;
    setIsScrollArticles(false);
  };

  const getVideoCall = (isRefresh = false, isScrollEvent = false) => {
    let finalQuery = {};

    if (videoScrollLoading || isScrollVideo) {
      return;
    }
    if (isRefresh) {
      videoPage = 1;
    }

    let skip = videoPage * 1;
    let limit = 10;

    finalQuery.categories = 255;
    finalQuery.page = skip;
    finalQuery.per_page = limit;

    let query = '';
    Object.keys(finalQuery).forEach((key, index) => {
      query += `${index === 0 ? '?' : '&'}${key}=${finalQuery[key]}`;
    });
    dispatch(
      getAllVideoData(
        query,
        isRefresh,
        successVideo,
        failedVideo,
        isScrollEvent,
      ),
    );
  };

  const failedVideo = () => {
    setIsScrollVideo(true);
  };

  const successVideo = () => {
    videoPage += 1;
    setIsScrollVideo(false);
  };

  const getAudioCall = (isRefresh = false, isScrollEvent = false) => {
    let finalQuery = {};

    if (audioScrollLoading || isScrollAudio) {
      return;
    }
    if (isRefresh) {
      audioPage = 1;
    }

    let skip = audioPage * 1;
    let limit = 10;

    finalQuery.categories = 11018;
    finalQuery.page = skip;
    finalQuery.per_page = limit;

    let query = '';
    Object.keys(finalQuery).forEach((key, index) => {
      query += `${index === 0 ? '?' : '&'}${key}=${finalQuery[key]}`;
    });
    dispatch(
      getAllAudioData(
        query,
        isRefresh,
        successAudio,
        failedAudio,
        isScrollEvent,
      ),
    );
  };

  const failedAudio = () => {
    setIsScrollAudio(true);
  };

  const successAudio = () => {
    audioPage += 1;
    setIsScrollAudio(false);
  };

  const tabChange = i => {
    if (i == 0) {
      flatListRefFeatured?.current?.scrollToOffset({animated: true, offset: 0});
    }
    if (i == 1) {
      flatListRefNews?.current?.scrollToOffset({animated: true, offset: 0});
    }
    if (i == 2) {
      flatListRefArticle?.current?.scrollToOffset({animated: true, offset: 0});
    }
    if (i == 3) {
      flatListRefVideo?.current?.scrollToOffset({animated: true, offset: 0});
    }
    if (i == 4) {
      flatListRefAudio?.current?.scrollToOffset({animated: true, offset: 0});
    }
  };

  return (
    <View style={CommonStyles.mainContainer}>
      <SafeAreaView style={{backgroundColor: PRIMARY_COLOR}} />
      <HeaderComponent />
      <ScrollableTabView
        initialPage={route?.params?.tab ?? 0}
        onChangeTab={event => tabChange(event.i)}
        renderTabBar={() => <TopTabComponent />}>
        <Featured
          tabLabel={FEATURED}
          isRefresh={() => getFeaturedCall(true)}
          isScroll={() => getFeaturedCall(false, true)}
          page={featuredPage}
          flatListRefFeatured={flatListRefFeatured}
        />
        <LatestNews
          tabLabel={LATEST_NEWS}
          isRefresh={() => getNewsCall(true)}
          isScroll={() => getNewsCall(false, true)}
          page={newsPage}
          flatListRefNews={flatListRefNews}
        />
        <Articles
          tabLabel={ARTICLES}
          isRefresh={() => getArticlesCall(true)}
          isScroll={() => getArticlesCall(false, true)}
          page={articlesPage}
          flatListRefArticle={flatListRefArticle}
        />
        <Videos
          tabLabel={VIDEOS}
          isRefresh={() => getVideoCall(true)}
          isScroll={() => getVideoCall(false, true)}
          page={videoPage}
          flatListRefVideo={flatListRefVideo}
        />
        <Audio
          tabLabel={AUDIO}
          isRefresh={() => getAudioCall(true)}
          isScroll={() => getAudioCall(false, true)}
          page={audioPage}
          flatListRefAudio={flatListRefAudio}
        />
      </ScrollableTabView>
    </View>
  );
}

export default TalkScreen;
