import React from "react";
import { View, Image, TouchableOpacity, ScrollView, Linking, Platform, useWindowDimensions } from 'react-native'
import CustomText from "../../components/CustomText";
import HeaderComponent from "../../components/HeaderComponent";
import { redBackIcon } from "../../constants/Images";
import CommonStyles from "../../style/CommonStyles";
import SpaceStyles from "../../style/SpaceStyles";
import HTML from "react-native-render-html";
import TextStyles from "../../style/TextStyles";
import { GRAY_TEXT, PRIMARY_COLOR } from "../../constants/Colors";
import { SafeAreaView } from "react-native";

function OfferDetailScreen(props) {
    const { navigation, route } = props
    const { detail } = route?.params
    const isIos = Platform.OS === 'ios'
    const {width}=useWindowDimensions()

    const tagsStyle = {
        fontFamily: isIos ? 'Montserrat-Medium' : 'GothamMedium',
        fontSize: 14,
        color: GRAY_TEXT,
        lineHeight: isIos ? 21 : 17,
        fontWeight :'900'
        
    };

    return (
        <View style={CommonStyles.mainContainer}>
            <SafeAreaView style={{ backgroundColor: PRIMARY_COLOR }} />
            <HeaderComponent />
            <ScrollView
                showsVerticalScrollIndicator={false}
                keyboardShouldPersistTaps={'handled'}
                bounces={false}
            >
                <View >
                    <Image
                        style={CommonStyles.mainOfferImageStyle}
                        resizeMode={'cover'}
                        source={{ uri: detail?.yoast_head_json?.og_image[0]?.url }}
                    />
                    <TouchableOpacity style={{ position: 'absolute', top: 10, left: 10 }} onPress={() => navigation.goBack()}>
                        <Image source={redBackIcon} style={{ height: 35, width: 35 }} />
                    </TouchableOpacity>
                </View>


                <View style={CommonStyles.offerDetailCardView}>
                    <Image
                        resizeMode={'stretch'}
                        source={{ uri: detail?.ACF?.banner }}
                        style={CommonStyles.offerMidImage}
                    />
                    <View style={[SpaceStyles.spaceHorizontal, SpaceStyles.top2, { flexDirection: 'row' }]}>
                        <Image
                            source={{ uri: detail?.ACF?.logo }}
                            resizeMode={'contain'}
                            style={CommonStyles.offerLogoStyle}
                        />
                        <View style={[SpaceStyles.width40, SpaceStyles.left5]}>
                            <HTML
                                contentWidth={width}
                                source={{ html: detail?.content?.rendered }}
                                baseStyle={tagsStyle}
                            />
                            <TouchableOpacity
                                onPress={() => Linking.openURL('https://' + detail?.ACF?.link)}
                                style={CommonStyles.moreDetailButton}>
                                <CustomText
                                    text={'MORE DETAILS'}
                                    style={[TextStyles.bold8White, { textAlign: 'center' }]}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </ScrollView>
        </View>
    )
}

export default OfferDetailScreen