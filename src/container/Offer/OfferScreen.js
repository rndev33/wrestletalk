import React, {useEffect, useState} from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  FlatList,
  RefreshControl,
  Platform,
  useWindowDimensions,
} from 'react-native';
import CustomText from '../../components/CustomText';
import HeaderComponent from '../../components/HeaderComponent';
import NavigationService from '../../navigation/NavigationService';
import CommonStyles from '../../style/CommonStyles';
import SpaceStyles from '../../style/SpaceStyles';
import TextStyles from '../../style/TextStyles';
import {useSelector, useDispatch} from 'react-redux';
import {getAllOfferData} from '../../redux/action/OfferAction';
import {GRAY_TEXT, PRIMARY_COLOR} from '../../constants/Colors';
import HTML from 'react-native-render-html';
import PostSkeleton from '../../components/Skeleton/PostSkeleton';
import BottomLoader from '../../components/BottomLoader';
import {SafeAreaView} from 'react-native';

let offerPage = 1;

function OfferScreen(props) {
  const {navigation} = props;
  const dispatch = useDispatch();
  const offerData = useSelector(state => state.offer.offerData);
  const offerLoading = useSelector(state => state.loading.offerLoading);
  const refreshLoading = useSelector(state => state.loading.refreshLoading);
  const offerScrollLoading = useSelector(
    state => state.loading.offerScrollLoading,
  );
  const isIos = Platform.OS === 'ios';
  const {width} = useWindowDimensions();

  const [isScrollOffer, setIsScrollOffer] = useState(false);
  const [totalOffer, setTotalOffer] = useState(0);
  const showAds = useSelector(state => state.ads.AdsData);

  const tagsStyle = {
    fontFamily: isIos ? 'Montserrat-Medium' : 'GothamMedium',
    fontSize: 14,
    color: GRAY_TEXT,
    lineHeight: isIos ? 21 : 17,
    fontWeight: '900',
  };

  useEffect(() => {
    offerPage = 1;
    getOfferCall();
  }, []);

  const getOfferCall = (isRefresh = false, isScrollEvent = false) => {
    let finalQuery = {};

    if (offerScrollLoading || isScrollOffer) {
      return;
    }
    if (isRefresh) {
      offerPage = 1;
    }

    let skip = offerPage * 1;
    let limit = 10;

    finalQuery.categories = 11048;
    finalQuery.page = skip;
    finalQuery.per_page = limit;

    let query = '';
    Object.keys(finalQuery).forEach((key, index) => {
      query += `${index === 0 ? '?' : '&'}${key}=${finalQuery[key]}`;
    });
    dispatch(
      getAllOfferData(
        query,
        isRefresh,
        successOffer,
        failedOffer,
        isScrollEvent,
      ),
    );
  };

  const failedOffer = () => {
    setIsScrollOffer(true);
  };

  const successOffer = total => {
    offerPage += 1;
    setIsScrollOffer(false);
    setTotalOffer(total);
  };

  const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
    const paddingToBottom = 20;
    return (
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom
    );
  };
  const handleScrollEnd = () => {
    if (!offerScrollLoading && totalOffer > offerPage) {
      getOfferCall(false, true);
    }
  };

  const renderPagingLoader = () => {
    if (offerScrollLoading) {
      return <BottomLoader />;
    } else {
      return null;
    }
  };

  const renderOffer = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() =>
          NavigationService.navigate('OfferDetailScreen', {
            detail: item,
          })
        }
        key={index}
        activeOpacity={0.5}
        style={[CommonStyles.offerCardView]}>
        <Image
          resizeMode={'stretch'}
          source={{uri: item?.ACF?.banner}}
          style={CommonStyles.offerTopImage}
        />
        <View
          style={[
            SpaceStyles.spaceHorizontal,
            SpaceStyles.top2,
            {flexDirection: 'row'},
          ]}>
          <Image
            source={{uri: item?.ACF?.logo}}
            resizeMode={'contain'}
            style={CommonStyles.offerLogoStyle}
          />
          <View style={[SpaceStyles.width55, SpaceStyles.left5]}>
            <HTML
              contentWidth={width}
              source={{html: item?.content?.rendered?.split('</p>')[0]}}
              baseStyle={tagsStyle}
            />
            <TouchableOpacity
              onPress={() =>
                NavigationService.navigate('OfferDetailScreen', {
                  detail: item,
                })
              }
              activeOpacity={0.5}
              style={CommonStyles.moreDetailButton}>
              <CustomText
                text={'MORE DETAILS'}
                style={[TextStyles.bold8White, {textAlign: 'center'}]}
              />
            </TouchableOpacity>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={CommonStyles.mainContainer}>
      <SafeAreaView style={{backgroundColor: PRIMARY_COLOR}} />
      <HeaderComponent />
      <CustomText
        text={'Offers'}
        style={[
          TextStyles.bold25TitleBlack,
          SpaceStyles.spaceVertical,
          SpaceStyles.spaceHorizontal,
        ]}
      />
      {offerLoading ? (
        <PostSkeleton offer={true} />
      ) : (
        <FlatList
          data={offerData}
          keyExtractor={(_, index) => index.toString()}
          renderItem={renderOffer}
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="handled"
          contentContainerStyle={[
            SpaceStyles.paddingTop2,
            SpaceStyles.spaceHorizontal,
          ]}
          refreshControl={
            <RefreshControl
              refreshing={refreshLoading}
              tintColor={[PRIMARY_COLOR]}
              colors={[PRIMARY_COLOR]}
              onRefresh={() => getOfferCall(true)}
            />
          }
          onMomentumScrollEnd={({nativeEvent}) => {
            if (isCloseToBottom(nativeEvent)) {
              handleScrollEnd();
            }
          }}
          scrollEventThrottle={400}
          ListFooterComponent={renderPagingLoader}
        />
      )}
    </View>
  );
}

export default OfferScreen;
