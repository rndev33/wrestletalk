import React, {useEffect} from 'react';
import {View, Image, Platform} from 'react-native';
import CustomText from '../../components/CustomText';
import {mainLogo, splashBackground} from '../../constants/Images';
import CommonStyles from '../../style/CommonStyles';
import TextStyles from '../../style/TextStyles';
import DeviceInfo from 'react-native-device-info';
import {useDispatch, useSelector} from 'react-redux';
import {getAllFeaturedData} from '../../redux/action/FeaturedAction';
import {getAllNewsData} from '../../redux/action/NewsAction';
import {getAllVideoData} from '../../redux/action/VideAction';
import {getAllAudioData} from '../../redux/action/AudioAction';
import {addRegisterTokenCall} from '../../redux/services/AuthServices';
import messaging from '@react-native-firebase/messaging';
import axios from 'axios';
import {REGISTER_TOKEN_API} from '../../redux/config/apiEndPoints';
import {getAllArticlesData} from '../../redux/action/ArticlesAction';
import {CLASSIC, getData, MODERN} from '../../constants/GlobalFunction';
import {Variables} from '../../constants/Variables';
import {setThemeData} from '../../redux/action/ThemeAction';

function SplashScreen({navigation}) {
  const dispatch = useDispatch();
  const featuredData = useSelector(state => state.featured.featuredData);
  const newsData = useSelector(state => state.news.newsData);
  const articlesData = useSelector(state => state.articles.articlesData);
  const videoData = useSelector(state => state.video.videoData);
  const audioData = useSelector(state => state.audio.audioData);

  useEffect(() => {
    getThemeCall();
    getFeaturedCall();
    getNewsCall();
    getArticlesCall();
    getVideoCall();
    getAudioCall();
    tokenCall();
  }, []);

  useEffect(() => {
    if (
      featuredData?.length > 0 &&
      newsData?.length > 0 &&
      articlesData?.length > 0 &&
      videoData?.length > 0 &&
      audioData?.length > 0
    ) {
      navigation.replace('RouteHomeStack');
    }
  }, [featuredData, newsData, articlesData, videoData, audioData]);

  const getThemeCall = () => {
    getData(Variables.theme)
      .then(res => {
        if (res == JSON.stringify(MODERN)) {
          dispatch(setThemeData(MODERN));
        } else {
          dispatch(setThemeData(CLASSIC));
        }
      })
      .catch(() => {});
  };

  const tokenCall = async () => {
    const tokenData = await messaging().getToken();
    const endPoint = `https://wrestletalk.com/wp-json/wp/v2/lf_register_push_notifications?token=${tokenData}&os=${
      Platform.OS === 'ios' ? 'iOS' : 'Android'
    }`;
    axios.get(endPoint, '').then(
      response => {},
      error => {},
    );
  };

  const getFeaturedCall = () => {
    let finalQuery = {};

    finalQuery.page = 1;
    finalQuery.per_page = 10;
    finalQuery.categories = 11384;

    let query = '';
    Object.keys(finalQuery).forEach((key, index) => {
      query += `${index === 0 ? '?' : '&'}${key}=${finalQuery[key]}`;
    });
    dispatch(getAllFeaturedData(query));
  };

  const getNewsCall = () => {
    let finalQuery = {};

    finalQuery.page = 1;
    finalQuery.per_page = 10;
    finalQuery.categories = '27, 1';

    let query = '';
    Object.keys(finalQuery).forEach((key, index) => {
      query += `${index === 0 ? '?' : '&'}${key}=${finalQuery[key]}`;
    });
    dispatch(getAllNewsData(query));
  };

  const getArticlesCall = () => {
    let finalQuery = {};

    finalQuery.page = 1;
    finalQuery.per_page = 10;
    finalQuery.categories = 28;

    let query = '';
    Object.keys(finalQuery).forEach((key, index) => {
      query += `${index === 0 ? '?' : '&'}${key}=${finalQuery[key]}`;
    });
    dispatch(getAllArticlesData(query));
  };

  const getVideoCall = () => {
    let finalQuery = {};

    finalQuery.categories = 255;
    finalQuery.page = 1;
    finalQuery.per_page = 10;

    let query = '';
    Object.keys(finalQuery).forEach((key, index) => {
      query += `${index === 0 ? '?' : '&'}${key}=${finalQuery[key]}`;
    });
    dispatch(getAllVideoData(query));
  };

  const getAudioCall = () => {
    let finalQuery = {};

    finalQuery.categories = 11018;
    finalQuery.page = 1;
    finalQuery.per_page = 10;

    let query = '';
    Object.keys(finalQuery).forEach((key, index) => {
      query += `${index === 0 ? '?' : '&'}${key}=${finalQuery[key]}`;
    });
    dispatch(getAllAudioData(query));
  };

  return (
    <View
      style={[
        CommonStyles.mainContainer,
        {alignItems: 'center', justifyContent: 'center'},
      ]}>
      <Image
        resizeMode={'stretch'}
        source={splashBackground}
        style={CommonStyles.splashImageStyle}
      />
      <Image
        resizeMode={'contain'}
        source={mainLogo}
        style={CommonStyles.splashLogoStyle}
      />
      <CustomText
        text={DeviceInfo.getVersion()}
        style={TextStyles.medium16LightBorder}
      />
    </View>
  );
}

export default SplashScreen;
