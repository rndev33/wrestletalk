import React, { useState } from "react";
import { View, Platform } from 'react-native'
import CustomButton from "../../components/CustomButton";
import CustomText from "../../components/CustomText";
import { PRIMARY_COLOR, WHITE } from "../../constants/Colors";
import CommonStyles from "../../style/CommonStyles";
import SpaceStyles from "../../style/SpaceStyles";
import TextStyles from "../../style/TextStyles";
import HeaderComponent from "../../components/HeaderComponent";
import CustomTextInput from "../../components/CustomTextInput";
import NavigationService from "../../navigation/NavigationService";
import { resetPasswordCall } from "../../redux/services/AuthServices";
import { isEmail, isEmpty, ToastAlert } from "../../constants/GlobalFunction";
import LoaderModal from "../../components/LoaderModal";
import { SafeAreaView } from "react-native";

function ForgotPasswordScreen({ navigation }) {

    const [email, setEmail] = useState('')
    const [loading, setLoading] = useState(false)
    const isIos = Platform.OS === 'ios'

    const resetPassword = () => {
        if (!isEmpty(email)) {
            return ToastAlert('Please enter Email')
        }
        if (!isEmail(email)) {
            return ToastAlert('Please enter valid email')
        }
        const data = {
            email: email
        }
        setLoading(true)
        resetPasswordCall(data)
            .then((res) => {
                setLoading(false)
                ToastAlert(res?.data?.message)
                navigation.navigate('SetPasswordScreen')
            })
            .catch((err) => {
                setLoading(false)
                console.log(err, 'errr')
                ToastAlert(err?.message)
            })
    }

    return (
        <View style={CommonStyles.mainContainer}>
            <SafeAreaView style={{ backgroundColor: PRIMARY_COLOR }} />
            <HeaderComponent />
            <LoaderModal loading={loading} />
            <View style={SpaceStyles.spaceHorizontal}>
                <CustomText
                    text={'Forgot Password'}
                    style={[TextStyles.bold16LightBlack, SpaceStyles.top3]}
                />
                <CustomText
                    text={'Email'}
                    style={[TextStyles.medium12LightBorder, SpaceStyles.top2]}
                />
                <CustomTextInput
                    placeholder={'Email'}
                    onChangeText={(text) => setEmail(text)}
                    style={[isIos ? TextStyles.medium12MainBlack : TextStyles.medium10Black, { flex: 1 }]}
                />
                <CustomButton
                    containerStyle={SpaceStyles.top3}
                    text={'SUBMIT'}
                    onPress={() => resetPassword()}
                />
                <CustomButton
                    text={'CANCEL'}
                    textStyle={TextStyles.medium12LightGray}
                    onPress={() => navigation.goBack()}
                    containerStyle={[{ backgroundColor: WHITE }, SpaceStyles.bottom1]}
                />
            </View>
        </View>
    )
}

export default ForgotPasswordScreen