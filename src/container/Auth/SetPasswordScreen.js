import React, { useState } from "react";
import { View, Platform, ScrollView } from 'react-native'
import CustomButton from "../../components/CustomButton";
import CustomText from "../../components/CustomText";
import { PRIMARY_COLOR, WHITE } from "../../constants/Colors";
import CommonStyles from "../../style/CommonStyles";
import SpaceStyles from "../../style/SpaceStyles";
import TextStyles from "../../style/TextStyles";
import HeaderComponent from "../../components/HeaderComponent";
import CustomTextInput from "../../components/CustomTextInput";
import NavigationService from "../../navigation/NavigationService";
import { setPasswordCall } from "../../redux/services/AuthServices";
import { isEmail, isEmpty, ToastAlert } from "../../constants/GlobalFunction";
import LoaderModal from "../../components/LoaderModal";
import { SafeAreaView } from "react-native";

function SetPasswordScreen({ navigation }) {

    const [otp, setOtp] = useState('')
    const [password, setPassword] = useState('')
    const [email, setEmail] = useState('')
    const [loading, setLoading] = useState(false)
    const isIos = Platform.OS === 'ios'

    const validateOtp = () => {
        if (!isEmpty(email)) {
            return ToastAlert('Please enter valid email')
        }
        if (!isEmpty(otp)) {
            return ToastAlert('Please enter valid otp')
        }
        if (!isEmpty(password)) {
            return ToastAlert('Please enter valid password')
        }

        const data = {
            email: email,
            password: password,
            code: otp
        }
        setLoading(true)
        setPasswordCall(data)
            .then((res) => {
                ToastAlert(res?.data?.message)
                setLoading(false)
            })
            .catch((err) => {
                setLoading(false)
                ToastAlert(err.message)
                console.log(err, 'errr')
            })
    }

    return (
        <View style={CommonStyles.mainContainer}>
            <SafeAreaView style={{ backgroundColor: PRIMARY_COLOR }} />
            <HeaderComponent />
            <LoaderModal loading={loading} />
            <ScrollView
                bounces={false}
                keyboardShouldPersistTaps={'handled'}
                showsVerticalScrollIndicator={false}
            >
                <View style={SpaceStyles.spaceHorizontal}>
                    <CustomText
                        text={'Forgot Password'}
                        style={[TextStyles.bold16LightBlack, SpaceStyles.top3]}
                    />
                    <CustomText
                        text={'Email'}
                        style={[TextStyles.medium12LightBorder, SpaceStyles.top2]}
                    />
                    <CustomTextInput
                        placeholder={'Enter Email'}
                        onChangeText={(text) => setEmail(text)}
                        style={[isIos ? TextStyles.medium12MainBlack : TextStyles.medium10Black, { flex: 1 }]}
                    />
                    <CustomText
                        text={'Password'}
                        style={[TextStyles.medium12LightBorder, SpaceStyles.top2]}
                    />
                    <CustomTextInput
                        placeholder={'Enter New Password'}
                        onChangeText={(text) => setPassword(text)}
                        style={[isIos ? TextStyles.medium12MainBlack : TextStyles.medium10Black, { flex: 1 }]}
                    />
                    <CustomText
                        text={'OTP'}
                        style={[TextStyles.medium12LightBorder, SpaceStyles.top2]}
                    />
                    <CustomTextInput
                        placeholder={'Enter OTP'}
                        onChangeText={(text) => setOtp(text)}
                        style={[isIos ? TextStyles.medium12MainBlack : TextStyles.medium10Black, { flex: 1 }]}
                    />
                    <CustomButton
                        containerStyle={SpaceStyles.top3}
                        text={'SUBMIT'}
                        onPress={() => validateOtp()}
                    />
                    <CustomButton
                        text={'CANCEL'}
                        textStyle={TextStyles.medium12LightGray}
                        onPress={() => navigation.goBack()}
                        containerStyle={[{ backgroundColor: WHITE }, SpaceStyles.bottom1]}
                    />
                </View>
            </ScrollView>
        </View>
    )
}

export default SetPasswordScreen