import React, {useEffect, useState} from 'react';
import {View, TouchableOpacity, Platform} from 'react-native';
import CustomButton from '../../components/CustomButton';
import CustomText from '../../components/CustomText';
import {DARK_RED, PRIMARY_COLOR, WHITE} from '../../constants/Colors';
import CommonStyles from '../../style/CommonStyles';
import SpaceStyles from '../../style/SpaceStyles';
import TextStyles from '../../style/TextStyles';
import HeaderComponent from '../../components/HeaderComponent';
import CustomTextInput from '../../components/CustomTextInput';
import NavigationService from '../../navigation/NavigationService';
import {
  getLoginCall,
  getNonceCall,
  getTokenCall,
  getUserDetails,
  getUserToken,
} from '../../redux/services/AuthServices';
import {
  isEmail,
  isEmpty,
  saveData,
  setAccessToken,
  ToastAlert,
} from '../../constants/GlobalFunction';
import LoaderModal from '../../components/LoaderModal';
import {SafeAreaView} from 'react-native';
import {Variables} from '../../constants/Variables';
import AsyncStorage from '@react-native-async-storage/async-storage';

function LoginScreen() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [nonceId, setNonceId] = useState('');
  const [loading, setLoading] = useState(false);
  const isIos = Platform.OS === 'ios';

  useEffect(() => {
    getNonce();
  }, []);

  const getNonce = () => {
    getNonceCall()
      .then(res => {
        setNonceId(res?.data?.nonce);
      })
      .catch(err => {});
  };

  const loginCall = () => {
    if (!isEmpty(email)) {
      return ToastAlert('Please enter Email');
    }
    if (!isEmail(email)) {
      return ToastAlert('Please enter valid email');
    }
    if (!isEmpty(password)) {
      return ToastAlert('Please enter password');
    }
    setLoading(true);

    getLoginCall(`nonce=${nonceId}&username=${email}&password=${password}`)
      .then(async res => {
        console.log('Login', res?.data?.user);
        if (res?.data?.status == 'ok') {
          getToken();
          getTokenForUser(res?.data?.user);
          await saveData(Variables.user, JSON.stringify(res?.data?.user));
        }
        if (res?.data?.error) {
          ToastAlert(res?.data?.error);
          setTimeout(() => {
            setLoading(false);
          }, 1000);
        }
      })
      .catch(err => {
        console.log("objectobjectobjectobject-----",err)

        setLoading(false);
      });
  };
  const getTokenForUser = user => {
    const data = {
      email: user?.email,
      name: user?.displayname,
      wp_user_id: user?.id,
      api_key:
        'qiqIjZ923ucQcaUYsuz3vNBW4af7ZxDRIm65QeG860Aai3sp8mGzHryDvv7W47RdfvuQw5Dm0hYKOe7DpZ1pMS5Ep96Ufd4EzoUoTlPHh6oX1LQlHyxETT7qARf4eON7',
    };
    getUserToken(data)
      .then(async res => {
        console.log('DETAIL  --- --- -- -- -- - -----',res?.data?.user,'\n\n', JSON.stringify(res?.data?.token));
        setLoading(false);
        // console.log('DETAIL', res);
        await saveData(Variables.token, JSON.stringify(res?.data?.token));
      })
      .catch(err => {
        console.log('Erroe', err);
      });
  };
  const getToken = () => {
    const data = {
      username: email,
      password: password,
    };
    setLoading(true);
    getTokenCall(data)
      .then(async res => {
        setLoading(false);
        setAccessToken(res.data.token);
        await saveData(Variables.email, JSON.stringify(email));
        ToastAlert('Login Successfully');
        NavigationService.reset('RouteHomeStack');
      })
      .catch(err => {
        setLoading(false);
      });
  };

  return (
    <View style={CommonStyles.mainContainer}>
      <SafeAreaView style={{backgroundColor: PRIMARY_COLOR}} />
      <HeaderComponent />
      <LoaderModal loading={loading} />
      <View style={SpaceStyles.spaceHorizontal}>
        <CustomText
          text={'Login'}
          style={[TextStyles.bold16LightBlack, SpaceStyles.top3]}
        />
        <View style={[SpaceStyles.rowFlex, SpaceStyles.top1]}>
          <CustomText
            text={'Don’t have an account? '}
            style={TextStyles.medium12LightBorder}
          />
          <TouchableOpacity
            onPress={() => NavigationService.navigate('RegisterScreen')}>
            <CustomText
              text={'Register'}
              style={[TextStyles.medium12LightBorder, {color: DARK_RED}]}
            />
          </TouchableOpacity>
        </View>
        <CustomText
          text={'Email'}
          style={[TextStyles.medium12LightBorder, SpaceStyles.top2]}
        />
        <CustomTextInput
          placeholder={'Email'}
          onChangeText={text => setEmail(text)}
          style={[
            isIos ? TextStyles.medium12MainBlack : TextStyles.medium10Black,
            {flex: 1},
          ]}
        />
        <CustomText
          text={'Password'}
          style={[TextStyles.medium12LightBorder, SpaceStyles.top2]}
        />
        <CustomTextInput
          placeholder={'Password'}
          secureTextEntry={true}
          onChangeText={text => setPassword(text)}
          style={[
            isIos ? TextStyles.medium12MainBlack : TextStyles.medium10Black,
            {flex: 1},
          ]}
        />
        <CustomButton
          containerStyle={SpaceStyles.top3}
          text={'LOGIN'}
          onPress={() => loginCall()}
        />
        <CustomButton
          text={'FORGOT PASSWORD'}
          onPress={() => NavigationService.navigate('ForgotPasswordScreen')}
          textStyle={TextStyles.medium12LightGray}
          containerStyle={[{backgroundColor: WHITE}, SpaceStyles.bottom1]}
        />
      </View>
    </View>
  );
}

export default LoginScreen;
