import React, { useState, useEffect } from "react";
import { View, TouchableOpacity, Switch, ScrollView, Linking, Platform, SafeAreaView, Alert } from 'react-native'
import CustomButton from "../../components/CustomButton";
import CustomText from "../../components/CustomText";
import CommonStyles from "../../style/CommonStyles";
import SpaceStyles from "../../style/SpaceStyles";
import TextStyles from "../../style/TextStyles";
import HeaderComponent from "../../components/HeaderComponent";
import NavigationService from "../../navigation/NavigationService";
import { Variables } from "../../constants/Variables";
import messaging from '@react-native-firebase/messaging';
import axios from "axios";
import { PRIMARY_COLOR } from "../../constants/Colors";
import { useDispatch, useSelector } from "react-redux";
import { setThemeData } from "../../redux/action/ThemeAction";
import { CLASSIC, getData, MODERN, saveData } from "../../constants/GlobalFunction";
import { deleteAccountApiCall, getAllNotificationCategory } from "../../redux/services/AuthServices";
import AsyncStorage from "@react-native-async-storage/async-storage";
import * as RNIap from 'react-native-iap';
import { requestSubscription } from 'react-native-iap';


const SKUS = Platform.select({
    ios: ['noads'],
    android: ['disableads']
})

function SettingScreen() {
    const dispatch = useDispatch()
    const [token, setToken] = useState('')
    const [allCategories, setAllCategories] = useState([])
    const themeData = useSelector(state => state.theme.themeData);
    const showAds = useSelector(state => state.ads.AdsData);

    const [podcastsToggle, setPodcastsToggle] = useState(false)
    const [independentsToggle, setIndependentsToggle] = useState(false)
    const [gamingToggle, setGamingToggle] = useState(false)
    const [spoilersToggle, setSpoilersToggle] = useState(false)
    const [mainOnlyToggle, setMainOnlyToggle] = useState(false)
    const [aewToggle, setAewToggle] = useState(false)
    const [wweMainToggle, setWweMainToggle] = useState(false)
    const [breakingToggle, setBreakingToggle] = useState(false)
    const [wrestlingToggle, setWrestlingToggle] = useState(false)
    const [ratingsToggle, setRatingsToggle] = useState(false)
    const [businessToggle, setBusinessToggle] = useState(false)
    const [wweToggle, setWweToggle] = useState(false)
    const [britishToggle, setBritishToggle] = useState(false)
    const [mexicoToggle, setMexicoToggle] = useState(false)
    const [japanToggle, setJapanToggle] = useState(false)
    const [njpwToggle, setNjpwToggle] = useState(false)
    const [rOHToggle, setROHToggle] = useState(false)
    const [nXTOnlyToggle, setNXTOnlyToggle] = useState(false)
    const [statisticsToggle, setStatisticsToggle] = useState(false)
    const [nXTToggle, setNXTToggle] = useState(false)
    const [impactToggle, setimpactToggle] = useState(false)
    const [partsfunknownToggle, setPartsfunknownToggle] = useState(false)
    const [wrestleTalkNewsToggle, setWrestleTalkNewsToggle] = useState(false)
    const [articlesToggle, setArticlesToggle] = useState(false)
    const [majorNXTToggle, setMajorNXTToggle] = useState(false)
    const [subscriptions, setSubscriptions] = useState([])
    const [offerToken, setOfferToken] = useState('')




    const hideAdsOnPurchaseRestore = async () => {
        await saveData(Variables.showAd, JSON.stringify(false))
    }
    /**
     * This method looks for any previous purchases 
     */
    const checkToRestorePurchase = async () => {

        try {
            const availablePurchases = await RNIap.getAvailablePurchases()
            if (!availablePurchases || availablePurchases.length === 0) {
                Alert.alert('Error', 'This account does not have an active subscription to restore. Please purchase the Go Ad Free pack first')
                return
            }
            const sortedAvailablePurchases = availablePurchases.sort(
                (a, b) => b.transactionDate - a.transactionDate
            );
            const latestAvailableReceipt = sortedAvailablePurchases[0].transactionReceipt;
            //If you have any available receipt this means a purchase happened 
            //If your products are not auto renewable then please parse the receipt and check for it's validity 

            if (latestAvailableReceipt) {

                Alert.alert('Success', 'Your purchase has been successfully restored. Please restart the app to remove ads from your device')

                hideAdsOnPurchaseRestore()
            }


        } catch (error) {
            console.log(error)
        }

    }

    useEffect(() => {
        getToken()
        getNotificationCategory()
        getSubscriptionsPlan()
    }, [])

    const getSubscriptionsPlan = async () => {
        let subscriptions = await RNIap.getSubscriptions({ skus: SKUS })
        console.log('subscriptions',subscriptions);
        if (Platform.OS === 'android') {
            setOfferToken = subscriptions[0].subscriptionOfferDetails[0].offerToken
        }
        setSubscriptions(subscriptions)
    }

    const getToken = async () => {
        getData(Variables.accessToken).then((res) => {
            setToken(JSON.parse(res))
        }).catch(() => {

        })
    }

    const deleteAccount = () => {
        Alert.alert(
            'Delete Account',
            'Are you sure? You want to delete account?',
            [
                {
                    text: 'Cancel',
                    onPress: () => {
                        return null;
                    },
                },
                {
                    text: 'Confirm',
                    onPress: async () => {
                        getData(Variables.email).then((res) => {
                            deleteAccountApiCall(JSON.parse(res)).then((res) => {
                                AsyncStorage.clear();
                                NavigationService.reset('RouteHomeStack')
                            }).catch((err) => {
                            })
                        }).catch(() => {

                        })
                    },
                },
            ],
            { cancelable: false },
        );
    }

    const getNotificationCategory = async () => {
        const tokenData = await messaging().getToken();
        getAllNotificationCategory(`${tokenData}`)
            .then((response) => {
                let temp = []
                response?.data?.categories?.map((i) => {
                    if (i.title.match("App Notifications")) {
                        if (i?.title == 'App Notifications - PPV Spoilers') {
                            i.check = 0
                            temp.splice(0, 0, i)
                            setSpoilersToggle(!i?.exclude)
                        }
                        if (i?.title == 'App Notifications - Major Wrestling News') {
                            i.check = 1
                            temp.splice(1, 0, i)
                            setBreakingToggle(!i?.exclude)
                        }
                        if (i?.title == 'App Notifications - WWE') {
                            i.check = 2
                            temp.splice(2, 0, i)
                            setWweToggle(!i?.exclude)
                        }
                        if (i?.title == 'App Notifications - Major WWE News') {
                            i.check = 3
                            temp.splice(3, 0, i)
                            setWweMainToggle(!i?.exclude)
                        }
                        if (i?.title == 'App Notifications - AEW') {
                            i.check = 4
                            temp.splice(4, 0, i)
                            setAewToggle(!i?.exclude)
                        }
                        if (i?.title == 'App Notifications - Major AEW News') {
                            i.check = 5
                            temp.splice(5, 0, i)
                            setMainOnlyToggle(!i?.exclude)
                        }
                        if (i?.title == 'App Notifications - NXT') {
                            i.check = 6
                            temp.splice(6, 0, i)
                            setNXTToggle(!i?.exclude)
                        }
                        if (i?.title == 'App Notifications - Major NXT News') {
                            i.check = 7
                            temp.splice(7, 0, i)
                            setNXTOnlyToggle(!i?.exclude)
                        }
                        if (i?.title == 'App Notifications - ROH') {
                            i.check = 8
                            temp.splice(8, 0, i)
                            setROHToggle(!i?.exclude)
                        }
                        if (i?.title == 'App Notifications - Impact') {
                            i.check = 9
                            temp.splice(9, 0, i)
                            setimpactToggle(!i?.exclude)
                        }
                        if (i?.title == 'App Notifications - NJPW') {
                            i.check = 10
                            temp.splice(10, 0, i)
                            setNjpwToggle(!i?.exclude)
                        }
                        if (i?.title == 'App Notifications - Japan') {
                            i.check = 11
                            temp.splice(11, 0, i)
                            setJapanToggle(!i?.exclude)
                        }
                        if (i?.title == 'App Notifications - Mexico') {
                            i.check = 12
                            temp.splice(12, 0, i)
                            setMexicoToggle(!i?.exclude)
                        }
                        if (i?.title == 'App Notifications - British Wrestling') {
                            i.check = 13
                            temp.splice(13, 0, i)
                            setBritishToggle(!i?.exclude)
                        }
                        if (i?.title == 'App Notifications - Independents') {
                            i.check = 14
                            temp.splice(14, 0, i)
                            setIndependentsToggle(!i?.exclude)
                        }
                        if (i?.title == 'App Notifications - Business and finance news') {
                            i.check = 15
                            temp.splice(15, 0, i)
                            setBusinessToggle(!i?.exclude)
                        }
                        if (i?.title == 'App Notifications - Ratings and demos') {
                            i.check = 16
                            temp.splice(16, 0, i)
                            setRatingsToggle(!i?.exclude)
                        }
                        if (i?.title == 'App Notifications - Wrestling gaming') {
                            i.check = 17
                            temp.splice(17, 0, i)
                            setWrestlingToggle(!i?.exclude)
                        }
                        if (i?.title == 'App Notifications - Wrestling media and influencers') {
                            i.check = 18
                            temp.splice(18, 0, i)
                            setGamingToggle(!i?.exclude)
                        }
                        if (i?.title == 'App Notifications - Wrestling statistics') {
                            i.check = 19
                            temp.splice(19, 0, i)
                            setStatisticsToggle(!i?.exclude)
                        }
                        if (i?.title == 'App Notifications - Articles and features') {
                            i.check = 20
                            temp.splice(20, 0, i)
                            setArticlesToggle(!i?.exclude)
                        }
                        if (i?.title == 'App Notifications - WrestleTalk News Videos') {
                            i.check = 21
                            temp.splice(21, 0, i)
                            setWrestleTalkNewsToggle(!i?.exclude)
                        }
                        if (i?.title == 'App Notifications - Partsfunknown') {
                            i.check = 22
                            temp.splice(22, 0, i)
                            setPartsfunknownToggle(!i?.exclude)
                        }
                        if (i?.title == 'App Notifications - Podcasts') {
                            i.check = 23
                            temp.splice(23, 0, i)
                            setPodcastsToggle(!i?.exclude)
                        }

                    }
                })
                temp?.sort((a, b) => (a.check > b.check) ? 1 : -1)
                setAllCategories(temp)
            }).catch(() => {

            })
    }

    const setSwichFun = async (value, id, title) => {

        if (title == 'App Notifications - Podcasts') {
            setPodcastsToggle(!podcastsToggle)
        }
        if (title == 'App Notifications - Independents') {
            setIndependentsToggle(!independentsToggle)
        }
        if (title == 'App Notifications - Wrestling media and influencers') {
            setGamingToggle(!gamingToggle)
        }
        if (title == 'App Notifications - PPV Spoilers') {
            setSpoilersToggle(!spoilersToggle)
        }
        if (title == 'App Notifications - Major AEW News') {
            setMainOnlyToggle(!mainOnlyToggle)
        }
        if (title == 'App Notifications - AEW') {
            setAewToggle(!aewToggle)
        }
        if (title == 'App Notifications - Major WWE News') {
            setWweMainToggle(!wweMainToggle)
        }
        if (title == 'App Notifications - Major Wrestling News') {
            setBreakingToggle(!breakingToggle)
        }
        if (title == 'App Notifications - Wrestling gaming') {
            setWrestlingToggle(!wrestlingToggle)
        }
        if (title == 'App Notifications - Ratings and demos') {
            setRatingsToggle(!ratingsToggle)
        }
        if (title == 'App Notifications - Business and finance news') {
            setBusinessToggle(!businessToggle)
        }
        if (title == 'App Notifications - WWE') {
            setWweToggle(!wweToggle)
        }
        if (title == 'App Notifications - British Wrestling') {
            setBritishToggle(!britishToggle)
        }
        if (title == 'App Notifications - Mexico') {
            setMexicoToggle(!mexicoToggle)
        }
        if (title == 'App Notifications - Japan') {
            setJapanToggle(!japanToggle)
        }
        if (title == 'App Notifications - NJPW') {
            setNjpwToggle(!njpwToggle)
        }
        if (title == 'App Notifications - Impact') {
            setimpactToggle(!impactToggle)
        }
        if (title == 'App Notifications - ROH') {
            setROHToggle(!rOHToggle)
        }
        if (title == 'App Notifications - Wrestling statistics') {
            setStatisticsToggle(!statisticsToggle)
        }
        if (title == 'App Notifications - NXT') {
            setNXTToggle(!nXTToggle)
        }
        if (title == 'App Notifications - Partsfunknown') {
            setPartsfunknownToggle(!partsfunknownToggle)
        }
        if (title == 'App Notifications - WrestleTalk News Videos') {
            setWrestleTalkNewsToggle(!wrestleTalkNewsToggle)
        }
        if (title == 'App Notifications - Articles and features') {
            setArticlesToggle(!articlesToggle)
        }
        if (title == 'App Notifications - Major NXT News') {
            setMajorNXTToggle(!majorNXTToggle)
        }

        const tokenData = await messaging().getToken();
        console.log({ tokenData })
        const endPoint = `https://wrestletalk.com/wp-json/wp/v2/lf_categories_notifications?token=${tokenData}&category_id=${id}&os=${Platform.OS === 'ios' ? 'iOS' : 'Android'}`

        console.log(endPoint)
        axios.post(endPoint, '')
            .then((res) => {
                console.log(res)
            }, (err) => {
                console.log(err)
            });
    }

    const toggleModernSwitch = (data) => {
        if (data) {
            dispatch(setThemeData(MODERN))
            saveData(Variables.theme, JSON.stringify(MODERN))
        }
        else {
            dispatch(setThemeData(CLASSIC))
            saveData(Variables.theme, JSON.stringify(CLASSIC))
        }
    }

    const purchaseSubscription = async () => {
        if (Platform.OS === 'ios') {
            await requestSubscription({ sku: Platform.OS === 'ios' ? 'takeoffads' : 'disableads' })
        } else {
            await requestSubscription({
                subscriptionOffers: [
                    { sku: Platform.OS === 'ios' ? 'takeoffads' : 'disableads', offerToken: offerToken },
                ],
            })
        }
    }

    const getUpdatedNotificationNames = (oldName) => {

        switch (oldName) {
            case 'Breaking News':
                return 'Major Wrestling News'
            case 'WWE Main Only':
                return 'Major WWE News'
            case 'AEW Main Only':
                return 'Major AEW News'
            default:
                return oldName
        }
    }


    return (
        <View style={CommonStyles.mainContainer}>
            <SafeAreaView style={{ backgroundColor: PRIMARY_COLOR }} />
            <HeaderComponent />
            {showAds &&
                <View style={[{ backgroundColor: '#DEDEDE' }, SpaceStyles.paddingVertical2]}>
                    <View style={[SpaceStyles.spaceHorizontal, SpaceStyles.alignSpaceBlock]}>
                        <CustomText
                            text={'Go Ad Free'}
                            style={TextStyles.medium16}
                        />
                        <TouchableOpacity
                            activeOpacity={0.6}
                            onPress={purchaseSubscription}
                            style={CommonStyles.premiunButton}>
                            <CustomText
                                text={'JUST £1.99 PER MONTH'}
                                style={[TextStyles.medium12White, { textAlign: 'center' }]}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
            }
            <ScrollView showsVerticalScrollIndicator={false} style={[SpaceStyles.spaceHorizontal]}>
                <CustomText
                    text={'Settings'}
                    style={[TextStyles.bold25TitleBlack, SpaceStyles.top3]}
                />
                {!token &&
                    <View style={[SpaceStyles.alignSpaceBlock, SpaceStyles.top3]}>
                        <TouchableOpacity
                            onPress={() => NavigationService.navigate('RegisterScreen')}
                            style={CommonStyles.addFreeButton}>
                            <CustomText
                                text={'REGISTER'}
                                style={[TextStyles.medium12White, { textAlign: 'center' }]}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => NavigationService.navigate('LoginScreen')}
                            style={CommonStyles.addFreeButton}>
                            <CustomText
                                text={'LOGIN'}
                                style={[TextStyles.medium12White, { textAlign: 'center' }]}
                            />
                        </TouchableOpacity>
                    </View>
                }

                <CustomText
                    text={'Design'}
                    style={[TextStyles.bold16LightBlack, SpaceStyles.top3]}
                />
                <CustomText
                    text={`Choose how you'd like to view the app`}
                    style={[TextStyles.medium12LightBorder, SpaceStyles.top1]}
                />

                <View style={[SpaceStyles.alignSpaceBlock, SpaceStyles.spaceVertical]}>
                    <View style={[SpaceStyles.alignSpaceBlock, SpaceStyles.width35]}>
                        <CustomText
                            text={'Modern'}
                            style={[TextStyles.bold12LightGray]}
                        />
                        <Switch
                            value={themeData == MODERN}
                            onValueChange={(data) => toggleModernSwitch(data)}
                        />
                    </View>
                    <View style={[SpaceStyles.alignSpaceBlock, SpaceStyles.width35]}>
                        <CustomText
                            text={'Classic'}
                            style={[TextStyles.bold12LightGray]}
                        />
                        <Switch
                            value={themeData == CLASSIC}
                            onValueChange={(data) => toggleModernSwitch(!data)}
                        />
                    </View>
                </View>

                <View style={[CommonStyles.lineView, { borderColor: '#DCDCDC' }]} />

                <CustomText
                    text={'Notifications'}
                    style={[TextStyles.bold16LightBlack, SpaceStyles.top3]}
                />
                <CustomText
                    text={'For important news for these following topics'}
                    style={[TextStyles.medium12LightBorder, SpaceStyles.top1]}
                />

                <View style={SpaceStyles.top3}>
                    {allCategories?.map((i) => {
                        return (
                            <View>
                                <View style={[SpaceStyles.alignSpaceBlock]}>
                                    <CustomText
                                        text={i?.title.replace("App Notifications - ", "")}
                                        style={[TextStyles.bold12LightGray]}
                                    />
                                    <Switch
                                        value={
                                            i?.title == 'App Notifications - Major NXT News' ? majorNXTToggle : i?.title == 'App Notifications - Podcasts' ? podcastsToggle : i.title == 'App Notifications - Independents' ?
                                                independentsToggle : i?.title == 'App Notifications - Wrestling media and influencers' ?
                                                    gamingToggle : i?.title == 'App Notifications - PPV Spoilers' ? spoilersToggle :
                                                        i?.title == 'App Notifications - Major AEW News' ? mainOnlyToggle :
                                                            i?.title == 'App Notifications - AEW' ? aewToggle :
                                                                i?.title == 'App Notifications - Major WWE News' ? wweMainToggle :
                                                                    i?.title == 'App Notifications - Major Wrestling News' ? breakingToggle :
                                                                        i?.title == 'App Notifications - Wrestling gaming' ? wrestlingToggle :
                                                                            i?.title == 'App Notifications - Ratings and demos' ? ratingsToggle :
                                                                                i?.title == 'App Notifications - Business and finance news' ? businessToggle :
                                                                                    i?.title == 'App Notifications - WWE' ? wweToggle :
                                                                                        i?.title == 'App Notifications - British Wrestling' ? britishToggle :
                                                                                            i?.title == 'App Notifications - Mexico' ? mexicoToggle :
                                                                                                i?.title == 'App Notifications - Japan' ? japanToggle :
                                                                                                    i?.title == 'App Notifications - NJPW' ? njpwToggle :
                                                                                                    i?.title == 'App Notifications - Impact' ? impactToggle :
                                                                                                        i?.title == 'App Notifications - ROH' ? rOHToggle :
                                                                                                            i?.title == 'App Notifications - Major NXT News' ? nXTOnlyToggle :
                                                                                                                i?.title == 'App Notifications - Wrestling statistics' ? statisticsToggle :
                                                                                                                    i?.title == 'App Notifications - NXT' ? nXTToggle :
                                                                                                                        i?.title == 'App Notifications - Partsfunknown' ? partsfunknownToggle :
                                                                                                                            i?.title == 'App Notifications - WrestleTalk News Videos' ? wrestleTalkNewsToggle : articlesToggle

                                        }
                                        onValueChange={(switchValue) => setSwichFun(switchValue, i?.id, i?.title)}
                                    />
                                </View>
                                <View style={[CommonStyles.lineView, SpaceStyles.vertical2, { borderColor: '#DCDCDC' }]} />
                            </View>
                        )
                    })}
                </View>



                {/* <View style={[CommonStyles.lineView, SpaceStyles.top2, { borderColor: '#DCDCDC' }]} /> */}
                {/* <View style={[SpaceStyles.alignSpaceBlock]}>
                    <View style={SpaceStyles.rowFlex}>
                        <Image source={grayStarIcon} />
                        <CustomText
                            text={'The Rock'}
                            style={[TextStyles.bold12LightGray, SpaceStyles.left3]}
                        />
                    </View>
                    <Switch
                        value={switchValue}
                        onValueChange={(switchValue) => setSwitchValue(switchValue)} />
                </View>
                <View style={[CommonStyles.lineView, SpaceStyles.vertical2, { borderColor: '#DCDCDC' }]} />
                <View style={[SpaceStyles.alignSpaceBlock]}>
                    <View style={SpaceStyles.rowFlex}>
                        <Image source={grayStarIcon} />
                        <CustomText
                            text={'John Cena'}
                            style={[TextStyles.bold12LightGray, SpaceStyles.left3]}
                        />
                    </View>
                    <Switch
                        value={switchValue}
                        onValueChange={(switchValue) => setSwitchValue(switchValue)} />
                </View> */}

                {/* <CustomButton
                    containerStyle={SpaceStyles.top3}
                    text={'ADD FAVORITES'}
                    onPress={() => NavigationService.navigate('FavoriteScreen')}
                /> */}
                {/* 
                <CustomText
                    text={'Mute Notifications?'}
                    style={[TextStyles.medium12LightBorder, SpaceStyles.top5]}
                />
                <CustomTextInput
                    placeholder={'Mute for 7 days'}
                    editable={false}
                /> */}

                {/* <View style={[CommonStyles.lineView, SpaceStyles.vertical3, { borderColor: '#DCDCDC' }]} /> */}

                <CustomButton
                    text={'RESTORE PURCHASE'}
                    onPress={checkToRestorePurchase}
                />

                <View style={[SpaceStyles.vertical1]} />


                <CustomButton
                    text={'CONTACT US'}
                    onPress={() => Linking.openURL('mailto:support@wrestletalk.com')}
                />

                <TouchableOpacity onPress={() => Linking.openURL('https://wrestletalk.com/privacy-policy/')}>
                    <CustomText
                        text={'PRIVACY POLICY'}
                        style={[TextStyles.bold12LightGray, SpaceStyles.left3, SpaceStyles.spaceVertical, { alignSelf: 'center' }]}
                    />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => Linking.openURL('https://wrestletalk.com/terms-of-use-eula/')}>
                    <CustomText
                        text={'TERMS OF USE (EULA)'}
                        style={[TextStyles.bold12LightGray, SpaceStyles.left3, SpaceStyles.bottom2, SpaceStyles.top1, { alignSelf: 'center' }]}
                    />
                </TouchableOpacity>

                {token != '' &&
                    <TouchableOpacity onPress={() => deleteAccount()}>
                        <CustomText
                            text={'DELETE ACCOUNT'}
                            style={[TextStyles.bold12LightGray, SpaceStyles.left3, SpaceStyles.spaceVertical, { alignSelf: 'center' }]}
                        />
                    </TouchableOpacity>
                }
            </ScrollView>
        </View>
    )
}

export default SettingScreen