import React, { useState, useEffect } from "react";
import { View, TouchableOpacity, TextInput, Image, ScrollView } from 'react-native'
import CustomButton from "../../components/CustomButton";
import CustomText from "../../components/CustomText";
import HeaderComponent from "../../components/HeaderComponent";
import { BLACK, DARK_BLACK, LIGHT_GRAY, PRIMARY_COLOR, WHITE } from "../../constants/Colors";
import { cancleIcon, searchGrayIcon } from "../../constants/Images";
import CommonStyles from "../../style/CommonStyles";
import SpaceStyles from "../../style/SpaceStyles";
import TextStyles from "../../style/TextStyles";
import { useDispatch, useSelector } from "react-redux";
import { getAllFavouriteData, setFavouriteData } from "../../redux/action/FavouriteAction";
import { Instagram, Twitter } from "react-native-socials";
import InstagramLogin from 'react-native-instagram-login';
import { SafeAreaView } from "react-native";

function FavoriteScreen(props) {
    const { navigation, route } = props

    const dispatch = useDispatch()
    const favouriteData = useSelector(state => state.Favourite.FavouriteData);

    const [popularData, setPopularData] = useState([])
    const [showSelected, setShowSelected] = useState(false)
    const [searchName, setSearchName] = useState('')
    const [searchData, setSearchData] = useState([])
    const [isLogedin, setIsLogedin] = useState(false)

    useEffect(() => {
        getFavourite()
    }, [])

    useEffect(() => {
        setPopularData(favouriteData)
    }, [popularData])

    const getFavourite = () => {
        dispatch(getAllFavouriteData())
    }

    const checkPopular = (index) => {
        let newData = Object.assign([], popularData);
        newData?.map((i, count) => {
            if (count == index) {
                i.check = !i.check
            }
        })
        setPopularData(newData)
        let arr = []
        newData.map((i) => {
            if (i.check) {
                arr.push(i)
            }
        })
        if (arr.length > 0) {
            setShowSelected(true)
        } else {
            setShowSelected(false)
        }
    }

    const searchFun = (text) => {
        setSearchName(text)
        if (text != '') {
            let filteredData = popularData?.filter(function (item) {
                return item?.name.toLowerCase().match(text?.toLowerCase());
            });
            setSearchData(filteredData)
        } else {
            setSearchData([])
        }
    }

    const onSearchClick = (name, index) => {
        let newData = Object.assign([], popularData);
        newData?.map((i, count) => {
            if (i.name == name) {
                i.check = !i.check
            }
        })
        setPopularData(newData)
        let arr = []
        newData.map((i) => {
            if (i.check) {
                arr.push(i)
            }
        })
        if (arr.length > 0) {
            setShowSelected(true)
        } else {
            setShowSelected(false)
        }

        let newSearch = Object.assign([], searchData);
        newSearch?.map((i, count) => {
            if (count == index) {
                i.check = !i.check
            }
        })
        setSearchData(newSearch)
    }

    const onApplyFilter = () => {
        let array = []
        popularData.map((i) => {
            if (i.check) {
                array.push(i)
            }
        })
    }

    return (
        <View style={CommonStyles.mainContainer}>
            <SafeAreaView style={{ backgroundColor: PRIMARY_COLOR }} />
            <HeaderComponent />
            <ScrollView
                showsVerticalScrollIndicator={false}
                keyboardShouldPersistTaps={'handled'}
                style={[SpaceStyles.top2, SpaceStyles.spaceHorizontal]}
            >
                <CustomText
                    text={'Favorites'}
                    style={[TextStyles.bold25TitleBlack, SpaceStyles.vertical2]}
                />

                <Twitter
                    consumerKey="PMNBvOKdpZswTowkN2tr3p7L3"
                    consumerSecret="Z2vJ3fZtCv5uz8Y40usIoQyTiXl7zApBLFo6nB68NJgzlCwc52"
                    id="1532713385070333952"
                />

                {/* <InstagramEmbed
                    id="B8U12TXAmK"
                    style={{ width: "100%" }}
                /> */}

                <InstagramLogin
                    // ref={ref => (this.instagramLogin = ref)}
                    appId='1187226452038457'
                    appSecret='4b8a021b582f4c32885a5a7e01696a95'
                    //   redirectUrl='your-redirect-Url'
                    scopes={['user_profile', 'user_media']}
                    // onLoginSuccess={() => <Instagram id="Cd9nV0tsvFt" />}
                    onLoginSuccess={() => console.log("In Login")}
                    onLoginFailure={(data) => console.log(data, "Fail---")}
                    modalVisible={true}
                />


                <View style={[CommonStyles.inputView, SpaceStyles.top1]}>
                    <TextInput
                        placeholder="Keyword"
                        defaultValue={searchName}
                        placeholderTextColor={LIGHT_GRAY}
                        style={[TextStyles.medium12LightGray, SpaceStyles.width70]}
                        onChangeText={(name) => searchFun(name)}
                    />
                    <Image
                        resizeMode={'contain'}
                        source={searchGrayIcon}
                    />
                </View>


                <View style={[SpaceStyles.sortRoeWrap, SpaceStyles.top2]}>
                    {searchData?.map((i, index) => {
                        const backgroundColor = i.check ? BLACK : WHITE
                        const color = i.check ? WHITE : DARK_BLACK
                        return (
                            <TouchableOpacity
                                key={index}
                                onPress={() => onSearchClick(i.name, index)}
                                style={[CommonStyles.sortView, { backgroundColor: backgroundColor }]}>
                                <CustomText
                                    text={i.name}
                                    style={[TextStyles.bold12DarkBlack, { color: color }]}
                                />
                            </TouchableOpacity>
                        )
                    })}
                </View>

                {showSelected &&
                    <>
                        <CustomText
                            text={'Selected subjects (min. 3)'}
                            style={[TextStyles.bold15TitleBlack, SpaceStyles.top3]}
                        />

                        <View style={CommonStyles.selectedSubjectedView}>
                            <View style={[SpaceStyles.sortRoeWrap]}>
                                {popularData?.map((i, index) => {
                                    const backgroundColor = i.check ? BLACK : WHITE
                                    const color = i.check ? WHITE : DARK_BLACK
                                    return (
                                        <>
                                            {i?.check &&
                                                <TouchableOpacity
                                                    key={index}
                                                    activeOpacity={0.6}
                                                    onPress={() => { checkPopular(index) }}
                                                    style={[CommonStyles.sortView, SpaceStyles.rowFlex, { backgroundColor: backgroundColor }]}>
                                                    <CustomText
                                                        text={i.name}
                                                        style={[TextStyles.bold12DarkBlack, { color: color }]}
                                                    />
                                                    <Image
                                                        style={SpaceStyles.left2}
                                                        source={cancleIcon}
                                                        resizeMode={'contain'}
                                                    />
                                                </TouchableOpacity>
                                            }
                                        </>
                                    )
                                })}
                            </View>
                        </View>
                    </>
                }

                <CustomText
                    text={'Popular Subjects'}
                    style={[TextStyles.bold15TitleBlack, SpaceStyles.top3]}
                />

                <View style={[SpaceStyles.sortRoeWrap, SpaceStyles.top2]}>
                    {popularData?.map((i, index) => {
                        const backgroundColor = i.check ? BLACK : WHITE
                        const color = i.check ? WHITE : DARK_BLACK
                        return (
                            <TouchableOpacity
                                key={index}
                                onPress={() => { checkPopular(index) }}
                                style={[CommonStyles.sortView, { backgroundColor: backgroundColor }]}>
                                <CustomText
                                    text={i.name}
                                    style={[TextStyles.bold12DarkBlack, { color: color }]}
                                />
                            </TouchableOpacity>
                        )
                    })}
                </View>


                <CustomButton
                    containerStyle={[SpaceStyles.top5, SpaceStyles.bottom2]}
                    text={'SAVE CHANGES'}
                    onPress={() => onApplyFilter()}
                />
            </ScrollView>
        </View>
    )
}

export default FavoriteScreen