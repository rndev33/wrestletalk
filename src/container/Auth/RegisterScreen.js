import React, {useState, useEffect} from 'react';
import {
  View,
  TouchableOpacity,
  Platform,
  SafeAreaView,
  ScrollView,
} from 'react-native';
import CustomButton from '../../components/CustomButton';
import CustomText from '../../components/CustomText';
import CommonStyles from '../../style/CommonStyles';
import SpaceStyles from '../../style/SpaceStyles';
import TextStyles from '../../style/TextStyles';
import HeaderComponent from '../../components/HeaderComponent';
import CustomTextInput from '../../components/CustomTextInput';
import NavigationService from '../../navigation/NavigationService';
import {getNonceCall, registerApiCall} from '../../redux/services/AuthServices';
import {
  saveData,
  setAccessToken,
  ToastAlert,
} from '../../constants/GlobalFunction';
import LoaderModal from '../../components/LoaderModal';
import {DARK_RED, PRIMARY_COLOR} from '../../constants/Colors';
import {Variables} from '../../constants/Variables';

function RegisterScreen() {
  const [nonceId, setNonceId] = useState('');
  const [firstName, setFirstName] = useState('');
  const [surname, setSurname] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);
  const isIos = Platform.OS === 'ios';

  useEffect(() => {
    //  getNonce()
  }, []);

  const getNonce = () => {
    getNonceCall()
      .then(res => {
        setNonceId(res?.data?.nonce);
      })
      .catch(err => {});
  };

  const registerCall = async () => {
    if (firstName == '') {
      return ToastAlert('Please enter First Name');
    }
    if (surname == '') {
      return ToastAlert('Please enter Surname');
    }
    if (email == '') {
      return ToastAlert('Please enter Email');
    }
    if (password == '') {
      return ToastAlert('Please enter Password');
    }

    setLoading(true);

    let _nonceResponse = await getNonceCall();

    let _nonce = _nonceResponse.data.nonce;

    const formdata = new FormData();
    formdata.append('first_name', firstName);
    formdata.append('last_name', surname);
    formdata.append('password', password);
    formdata.append('email', email);

    registerApiCall(formdata)
      .then(async res => {
        setLoading(false);
        const response = JSON.parse(`${res.data}`);
        if (response?.status === 'success') {
          //  setAccessToken(res?.data?.cookie)
          await saveData(Variables.email, JSON.stringify(email));
          ToastAlert('Register Successful');
        }
        if (res?.data?.error) {
          ToastAlert(res?.data?.error);
        }
      })
      .catch(err => {
        setLoading(false);
      });
  };

  return (
    <View style={CommonStyles.mainContainer}>
      <SafeAreaView style={{backgroundColor: PRIMARY_COLOR}} />
      <HeaderComponent />
      <LoaderModal loading={loading} />
      <ScrollView
        bounces={false}
        keyboardShouldPersistTaps={'handled'}
        showsVerticalScrollIndicator={false}>
        <View style={SpaceStyles.spaceHorizontal}>
          <CustomText
            text={'Register'}
            style={[TextStyles.bold16LightBlack, SpaceStyles.top3]}
          />
          <View style={[SpaceStyles.rowFlex, SpaceStyles.top1]}>
            <CustomText
              text={'Already registered? '}
              style={TextStyles.medium12LightBorder}
            />
            <TouchableOpacity
              onPress={() => NavigationService.navigate('LoginScreen')}>
              <CustomText
                text={'Log in'}
                style={[TextStyles.medium12LightBorder, {color: DARK_RED}]}
              />
            </TouchableOpacity>
          </View>
          <CustomText
            text={'First Name'}
            style={[TextStyles.medium12LightBorder, SpaceStyles.top2]}
          />
          <CustomTextInput
            placeholder={'First Name'}
            onChangeText={text => setFirstName(text)}
            style={[
              isIos ? TextStyles.medium12MainBlack : TextStyles.medium10Black,
              {flex: 1},
            ]}
          />
          <CustomText
            text={'Surname'}
            style={[TextStyles.medium12LightBorder, SpaceStyles.top2]}
          />
          <CustomTextInput
            placeholder={'Surname'}
            onChangeText={text => setSurname(text)}
            style={[
              isIos ? TextStyles.medium12MainBlack : TextStyles.medium10Black,
              {flex: 1},
            ]}
          />
          <CustomText
            text={'Email'}
            style={[TextStyles.medium12LightBorder, SpaceStyles.top2]}
          />
          <CustomTextInput
            placeholder={'Email'}
            onChangeText={text => setEmail(text)}
            style={[
              isIos ? TextStyles.medium12MainBlack : TextStyles.medium10Black,
              {flex: 1},
            ]}
          />
          <CustomText
            text={'Password'}
            style={[TextStyles.medium12LightBorder, SpaceStyles.top2]}
          />
          <CustomTextInput
            placeholder={'Password'}
            onChangeText={text => setPassword(text)}
            style={[
              isIos ? TextStyles.medium12MainBlack : TextStyles.medium10Black,
              {flex: 1},
            ]}
            secureTextEntry={true}
          />
          <CustomButton
            onPress={() => registerCall()}
            containerStyle={SpaceStyles.top3}
            text={'REGISTER'}
          />
        </View>
      </ScrollView>
    </View>
  );
}

export default RegisterScreen;
