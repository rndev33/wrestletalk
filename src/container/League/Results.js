import React, { useEffect, useRef, useState } from "react";
import { Image, View, TouchableOpacity, ScrollView,StyleSheet } from 'react-native'
import CustomText from "../../components/CustomText";
import COLORS, { BACK_TABLE, LIGHT_BLACK, SKY_BLUE, WHITE } from "../../constants/Colors";
import { downArrow, dummyBanner, selectedRound, unSelectRound } from "../../constants/Images";
import CommonStyles from "../../style/CommonStyles";
import SpaceStyles from "../../style/SpaceStyles";
import TextStyles from "../../style/TextStyles";
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from "../../constants";
import { FlatList } from "react-native-gesture-handler";
import SelectionModalize from "./SharedModalize";
import { getResultsData, getResultsDataById } from "../../redux/services/leagueServices";
import LoaderModal from "../../components/LoaderModal";
import LoginComp from "./LoginComponent";

function Results({
    token
}) {
    const [loading, setLoading] = useState(false);
    const [seasons, setSeasons] = useState([])
    const [currentSeason, setCurrentSeason] = useState(null)
    const [seasonSelected, setSeasonSelected] = useState('')
    const [eventSelected, setEventSelected] = useState('')
    const pickerSeasonModalRef = useRef(null);
    const [eventMatches, setEventMatches] = useState([])
    const closeSeasonModalPicker = () => {
        pickerSeasonModalRef?.current?.close();
      };
      const openSeasonModalPicker = () => {
        pickerSeasonModalRef?.current?.open();
      };
      const pickerEventModalRef = useRef(null);
    const closeEventModalPicker = () => {
        pickerEventModalRef?.current?.close();
      };
      const openEventModalPicker = () => {
        pickerEventModalRef?.current?.open();
      };

    useEffect(() => {
        getData();
      }, []);
    
      const getData = () => {
        setLoading(true);
        getResultsData()
          .then(res => {
            setSeasons(res?.data?.seasons)
            setSeasonSelected(res?.data?.seasons[0].title)
            setEventSelected(res?.data?.current_season.events[0].title)
            setEventMatches(res?.data?.current_season?.events[0].matches)
            setCurrentSeason(res?.data?.current_season)
          })
          .catch(err => {})
          .finally((res) =>{
            setLoading(false)
          } );
      };
        const onSeasonItemClick = (item) => {
          setSeasonSelected(item?.title)
          setLoading(true)
          getResultsDataById(item?.id).then((res)=>{
              setCurrentSeason(res?.data?.current_season)
              setEventSelected(res?.data?.current_season?.events[0]?.title)
              setEventMatches(res?.data?.current_season?.events[0].matches)
              setLoading(false)
          })
          closeSeasonModalPicker()
        };

    const fetchEventMatches =  (id, sData) => {
        const s = currentSeason || sData
        s?.events?.map((item)=>{
            if(item.id == id){
                setEventMatches(item?.matches)
            }
        })
      }
      if(!token){
        return <LoginComp />
      }

    const renderResultRound = ({ item, index }) => {
        return (
            <View
                key={index}
                style={[SpaceStyles.paddingBottom5]}
            >
                <Image
                                    resizeMode={'cover'}
                                    source={{uri:item.image}}
                                    style={[Styles.bannerStyle,{borderRadius:8,marginBottom:10}]}
                                    // style={CommonStyles.currentRoundBannerStyle}
                                    />
                <CustomText
                    text={item?.title}
                    style={[TextStyles.medium12LightBlack, SpaceStyles.top2, SpaceStyles.bottom1]}
                />
                
                {item?.choices?.map((i, playerIndex) => {
                    const backGroundColor = i == item.answer ? SKY_BLUE : BACK_TABLE
                    const textColor = (i == item.answer) ? WHITE : LIGHT_BLACK
                    const icon = i == item.answer ? selectedRound : unSelectRound
                    return (
                                 <LinearGradient
                                        colors={item.prediction ? ['rgba(195, 14, 19, 1)', 'rgba(241, 71, 2, 1)']:[backGroundColor, backGroundColor]}
                                        start={{ x: 0.0, y: 1.0 }}
                                        end={{ x: 1.0, y: 1.0 }}
                                        style={[CommonStyles.playerView]}>
                            {/* <View style={SpaceStyles.rowFlex}> */}
                            <View style={[SpaceStyles.rowFlex]}>
                                <Image
                                    resizeMode={'contain'}
                                    source={icon}
                                    style={i.result && {tintColor:WHITE}}
                                />
                                <CustomText
                                    text={i}
                                    style={[TextStyles.medium12LightBlack, SpaceStyles.left3, { color: textColor }]}
                                />
                                </View>
                                <View> 
                                    {i == item.answer && <CustomText
                                    text={i == item.answer ? '+ 1 point' : '+ 0 point'}
                                    style={[TextStyles.medium12LightBlack, SpaceStyles.left3, { color: textColor }]}
                                />}
                                </View>
                            </LinearGradient>
                    )
                })}
            </View>
        )
    }
 
      const onEventItemClick = (item) => {
        setEventSelected(item?.title)
        fetchEventMatches(item?.id)
        closeEventModalPicker()
      };
        return (
            <>
            <LoaderModal loading={loading} />
            <ScrollView>
            <Image
                resizeMode={'stretch'}
                source={dummyBanner}
                style={CommonStyles.currentRoundBannerStyle}
            />
               <CustomText
                    text={currentSeason?.title}
                    style={[TextStyles.bold18LightBlack, SpaceStyles.top2, { alignSelf: 'center' }]}
                />
                <View style={[SpaceStyles.rowFlex, SpaceStyles.vertical1, { alignSelf: 'center' }]}>
                    <CustomText
                        text={'You scored'}
                        style={TextStyles.medium10CloseText}
                    />
                    <LinearGradient
                        colors={['rgba(195, 14, 19, 1)', 'rgba(241, 71, 2, 1)']}
                        start={{ x: 0.0, y: 1.0 }}
                        end={{ x: 1.0, y: 1.0 }}
                        style={CommonStyles.timerView}
                    >
                        <CustomText
                            text={'4 pts'}
                            style={TextStyles.bold10White}
                        />
                    </LinearGradient>
                </View>

                <TouchableOpacity onPress={()=>openSeasonModalPicker()} style={[{borderWidth:1,borderColor:COLORS.LIGHT_BORDER,alignSelf:'center',padding:10,marginTop:20,width:'90%'},SpaceStyles.alignSpaceBlock]}>
                 <CustomText
                    text={seasonSelected}
                    style={[TextStyles.bold14LightBlack, { alignSelf: 'center' }]}
                />
                <Image
                source={downArrow}
                style={{height:8,width:8,tintColor:COLORS.LIGHT_BLACK}}
                resizeMode="contain"
                />
            </TouchableOpacity>

            <TouchableOpacity onPress={()=>openEventModalPicker()} style={[{borderWidth:1,borderColor:COLORS.LIGHT_BORDER,width:'90%',alignSelf:'center',padding:10,marginTop:10, marginBottom:20},SpaceStyles.alignSpaceBlock]}>
                 <CustomText
                    text={eventSelected}
                    style={[TextStyles.bold14LightBlack, { alignSelf: 'center' }]}
                />
                <Image
                source={downArrow}
                style={{height:8,width:8,tintColor:COLORS.LIGHT_BLACK}}
                resizeMode="contain"
                />
            </TouchableOpacity>

                    <FlatList
                    data={eventMatches}
                    ListEmptyComponent={()=><View style={{alignItems:'center',justifyContent:'center'}}>
                    <CustomText
                    text={'No Record Found!'}
                    style={[TextStyles.medium14LightBorder]}
                />
                </View>}
                    keyboardShouldPersistTaps="handled"
                    keyExtractor={(item, index) => index.toString()}
                    showsVerticalScrollIndicator={false}
                    renderItem={renderResultRound}
                    contentContainerStyle={[SpaceStyles.spaceHorizontal, SpaceStyles.paddingBottom2]}
                />
                <View style={{height:100}}/>
                <SelectionModalize
      init={(s) => {
         pickerEventModalRef.current = s;
       }}
       data={currentSeason?.events}
       closeModal={closeEventModalPicker}
       selected={eventSelected}
       onItemClick={onEventItemClick}
     />
      <SelectionModalize
      init={(s) => {
         pickerSeasonModalRef.current = s;
       }}
       data={seasons}
       closeModal={closeSeasonModalPicker}
       selected={seasonSelected}
       onItemClick={onSeasonItemClick}
     />
            </ScrollView>
            </>
        )
    }


const Styles = StyleSheet.create({
wrapper:{paddingBottom:100},
CarouselWrapper:{height:310,paddingHorizontal:10,zIndex:9999},
carouselItemWrapper:{paddingHorizontal:10,width:350},
carouselItemView:{borderRadius:8,overflow:'hidden', shadowColor: "#000",
shadowOffset: {
	width: 0,
	height: 3,
},
shadowOpacity: 0.27,
shadowRadius: 4.65,
elevation: 6,width:'100%'},
bannerStyle:{height:187,width:'100%'},
timmerWrapper:{position:'absolute',marginLeft:0,borderBottomRightRadius:15,borderTopRightRadius:0,borderBottomLeftRadius:0},
contentWrapper:{width:'100%',backgroundColor:COLORS.WHITE},
carouselButtonStyle:{alignSelf:'flex-start', alignItems:'center',backgroundColor:COLORS.SKY_BLUE,paddingVertical:10},
customButtonWrapper:{borderWidth:1, borderColor:Colors.BLACK,borderRadius:5},
columnWrapper:{width:'48%',borderRadius:4, alignItems:'center'},
resultItemWrapper:{width:'100%',paddingHorizontal:20},
resultItemView:{borderRadius:8,overflow:'hidden', shadowColor: "#000",
shadowOffset: {
	width: 0,
	height: 3,
},
shadowOpacity: 0.27,
shadowRadius: 4.65,
elevation: 6,backgroundColor:COLORS.WHITE},
seasonItemView:{width:'100%',paddingHorizontal:20,marginVertical:10},
seasonButton:{backgroundColor:COLORS.BLACK,borderRadius:5},

})

export default Results