import React, {useEffect, useState} from 'react';
import {
  Image,
  View,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
} from 'react-native';
import CustomText from '../../components/CustomText';
import COLORS from '../../constants/Colors';
import {dummyBanner} from '../../constants/Images';
import CommonStyles from '../../style/CommonStyles';
import SpaceStyles from '../../style/SpaceStyles';
import TextStyles from '../../style/TextStyles';
import LinearGradient from 'react-native-linear-gradient';
import {Colors} from '../../constants';
import {FlatList} from 'react-native-gesture-handler';
import {getDashboardData} from '../../redux/services/leagueServices';
import LoaderModal from '../../components/LoaderModal';
import moment from 'moment';
import {addLeadingZero, getTimeDiff} from '../../redux/services/HelperServices';
import useInterval from '../../components/interval';
import LoginComp from './LoginComponent';

function Dashboard({screen,token}) {
  const [loading, setLoading] = useState(false);
  const [alldata, setAllData] = useState([]);
  const [seasons, setSeasons] = useState([]);
  const [upcEvents, setUpcEvents] = useState([]);
  const [prevEvent, setPrevEvents] = useState({});
console.log("propspropspropsprops",token)
  useEffect(() => {
    getAllData();
  }, []);

  const getAllData = () => {
    setLoading(true);
    getDashboardData()
      .then(res => {
        setAllData(res?.data || []);
        setSeasons(res?.data?.seasons || []);
        setUpcEvents(res?.data?.upcomming_events || []);
        setPrevEvents(res?.data?.prev_event || {});
      })
      .catch(err => {})
      .finally(() => setLoading(false));
  };
if(!token){
  return <LoginComp />
}
  return (
    <>
      <LoaderModal loading={loading} />
      <ScrollView contentContainerStyle={Styles.wrapper}>
        <CustomText
          text={'Upcoming Events'}
          style={[
            TextStyles.bold18LightBlack,
            SpaceStyles.spaceVertical,
            SpaceStyles.spaceHorizontal,
          ]}
        />
        <FlatList
          data={upcEvents}
          horizontal={true}
          nestedScrollEnabled={true}
          // alignItems={'flex-start'}
          contentContainerStyle={Styles.CarouselWrapper}
          showsHorizontalScrollIndicator={false}
          renderItem={({item, index}) => {
            return <EventCard item={item} length={upcEvents?.length} />;
          }}
        />
        <View
          style={[
            SpaceStyles.alignSpaceBlock,
            SpaceStyles.spaceVertical,
            SpaceStyles.spaceHorizontal,
          ]}>
          <CustomText text={'Position'} style={[TextStyles.bold18LightBlack]} />
          <TouchableOpacity style={Styles.customButtonWrapper}>
            <CustomText
              text={'Full leaderboard'}
              style={[
                TextStyles.medium11LightBlack,
                SpaceStyles.horizontal3,
                SpaceStyles.vertical1,
              ]}
            />
          </TouchableOpacity>
        </View>
        <View style={[SpaceStyles.spaceHorizontal]}>
          <LinearGradient
            colors={['rgba(195, 14, 19, 1)', 'rgba(241, 71, 2, 1)']}
            start={{x: 0.0, y: 1.0}}
            end={{x: 1.0, y: 1.0}}
            style={[
              SpaceStyles.horizontal3,
              SpaceStyles.paddingVertical2,
              SpaceStyles.rowJustify,
            ]}>
            <CustomText
              text={'In the current season'}
              style={TextStyles.bold10White}
            />
            <CustomText
              text={alldata?.prev_events_in_current_seasons}
              style={TextStyles.bold10White}
            />
          </LinearGradient>
        </View>
        <View
          style={[
            SpaceStyles.alignSpaceBlock,
            SpaceStyles.spaceVertical,
            SpaceStyles.spaceHorizontal,
          ]}>
          <View>
            <CustomText
              text={'PREV EVENT'}
              style={[TextStyles.medium10CloseText]}
            />
            <CustomText
              text={prevEvent?.title}
              style={[TextStyles.bold18LightBlack]}
            />
          </View>
          <TouchableOpacity style={Styles.customButtonWrapper}>
            <CustomText
              text={'Results'}
              style={[
                TextStyles.medium11LightBlack,
                SpaceStyles.horizontal3,
                SpaceStyles.vertical1,
              ]}
            />
          </TouchableOpacity>
        </View>
        <View
          style={[SpaceStyles.spaceHorizontal, SpaceStyles.alignSpaceBlock]}>
          <View
            style={[
              Styles.columnWrapper,
              SpaceStyles.horizontal3,
              SpaceStyles.paddingVertical2,
              {backgroundColor: COLORS.SKY_BLUE},
            ]}>
            <CustomText
              text={'Event Points'}
              style={[TextStyles.bold12White]}
            />
            <CustomText
              text={alldata?.prev_event_points}
              style={[TextStyles.bold25TitleWhite, SpaceStyles.paddingTop2]}
            />
          </View>
          <LinearGradient
            colors={['rgba(195, 14, 19, 1)', 'rgba(241, 71, 2, 1)']}
            start={{x: 0.0, y: 1.0}}
            end={{x: 1.0, y: 1.0}}
            style={[
              Styles.columnWrapper,
              SpaceStyles.horizontal3,
              SpaceStyles.paddingVertical2,
            ]}>
            <CustomText
              text={'Total Season Points'}
              style={[TextStyles.bold12White]}
            />
            <CustomText
              text={alldata?.total_season_points}
              style={[TextStyles.bold25TitleWhite, SpaceStyles.paddingTop2]}
            />
          </LinearGradient>
        </View>

        <View>
          <FlatList
            data={prevEvent?.matches}
            contentContainerStyle={[SpaceStyles.paddingTop2]}
            renderItem={({item}) => {
              return (
                <View
                  style={[
                    Styles.resultItemWrapper,
                    SpaceStyles.paddingBottom2,
                  ]}>
                  <View
                    style={[
                      Styles.resultItemView,
                      SpaceStyles.rowJustify,
                      SpaceStyles.horizontal3,
                      SpaceStyles.paddingVertical2,
                    ]}>
                    <View style={{width: '90%'}}>
                      <CustomText
                        text={item.title}
                        style={[TextStyles.bold14LightBlack]}
                      />
                      <CustomText
                        text={`Winner - ${item?.answer}`}
                        style={[
                          TextStyles.medium12LightBorder,
                          {paddingTop: 10},
                        ]}
                      />
                      <CustomText
                        text={`Your choice - ${item?.prediction ? answer : ''}`}
                        style={[
                          TextStyles.medium12LightBorder,
                          {paddingTop: 10},
                        ]}
                      />
                    </View>
                    <View>
                      <CustomText
                        text={`+${item?.points}`}
                        style={[TextStyles.bold14LightBlack]}
                      />
                    </View>
                  </View>
                </View>
              );
            }}
          />
        </View>
        <View
          style={[
            SpaceStyles.alignSpaceBlock,
            SpaceStyles.spaceVertical,
            SpaceStyles.spaceHorizontal,
          ]}>
          <CustomText
            text={'Past Season'}
            style={[TextStyles.bold18LightBlack]}
          />
          <TouchableOpacity style={Styles.customButtonWrapper}>
            <CustomText
              text={'All Seasons'}
              style={[
                TextStyles.medium11LightBlack,
                SpaceStyles.horizontal3,
                SpaceStyles.vertical1,
              ]}
            />
          </TouchableOpacity>
        </View>

        <View>
          <FlatList
            data={seasons}
            // contentContainerStyle={[SpaceStyles.paddingTop2]}
            renderItem={({item}) => {
              return (
                <View style={[Styles.seasonItemView, SpaceStyles.rowJustify]}>
                  <View style={SpaceStyles.rowFlex}>
                    <View style={[SpaceStyles.width28]}>
                      <CustomText
                        text={`${moment(item?.start_date_time).format(
                          'MMM',
                        )}-${moment(item?.end_date_time).format('MMM YYYY')}`}
                        style={[
                          TextStyles.medium14Review,
                          SpaceStyles.vertical1,
                        ]}
                      />
                    </View>
                    <View style={[SpaceStyles.width28]}>
                      <CustomText
                        text={item?.title}
                        style={[
                          TextStyles.medium14Review,
                          SpaceStyles.vertical1,
                        ]}
                      />
                    </View>
                  </View>
                  <TouchableOpacity style={Styles.seasonButton}>
                    <CustomText
                      text={'Leaderboard'}
                      style={[
                        TextStyles.medium12White,
                        SpaceStyles.horizontal3,
                        SpaceStyles.vertical1,
                      ]}
                    />
                  </TouchableOpacity>
                </View>
              );
            }}
          />
        </View>
      </ScrollView>
    </>
  );
}

const EventCard = ({item, length}) => {
  const [totalTimeLeft, setTotalTimeLeft] = useState({
    days: '00',
    hours: '00',
    minutes: '00',
    seconds: '00',
  });

  useInterval(() => {
    const diff = getTimeDiff(item?.deadline_timestamp, true);
    if (diff.asSeconds() > 0) {
      const {days, hours, minutes, seconds, months} = diff._data;
      setTotalTimeLeft({
        days:
          months > 0
            ? addLeadingZero(days + days * months)
            : addLeadingZero(days),
        hours: addLeadingZero(hours),
        minutes: addLeadingZero(minutes),
        seconds: addLeadingZero(seconds),
      });
    } else {
    }
  }, 1000);
  return (
    <View
      style={[Styles.carouselItemWrapper, {width: length === 1 ? 365 : 350}]}>
      <View style={Styles.carouselItemView}>
        <Image
          resizeMode={'cover'}
          source={dummyBanner}
          style={Styles.bannerStyle}
        />
        <LinearGradient
          colors={['rgba(195, 14, 19, 1)', 'rgba(241, 71, 2, 1)']}
          start={{x: 0.0, y: 1.0}}
          end={{x: 1.0, y: 1.0}}
          style={[
            CommonStyles.timerView,
            Styles.timmerWrapper,
            {width: '35%'},
          ]}>
          <CustomText
            text={`${totalTimeLeft?.days}d:${totalTimeLeft?.hours}h:${totalTimeLeft?.minutes}m:${totalTimeLeft?.seconds}s`}
            style={TextStyles.bold10White}
          />
        </LinearGradient>
        <View style={[Styles.contentWrapper, SpaceStyles.paddingBottom2]}>
          <CustomText
            text={item?.title}
            style={[
              TextStyles.bold18LightBlack,
              SpaceStyles.spaceVertical,
              SpaceStyles.spaceHorizontal,
            ]}
          />
          <TouchableOpacity
            style={[
              SpaceStyles.spaceHorizontal,
              Styles.carouselButtonStyle,
              SpaceStyles.horizontal3,
            ]}>
            <CustomText
              text={'Event Prediction'}
              style={[TextStyles.bold12White]}
            />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const Styles = StyleSheet.create({
  wrapper: {paddingBottom: 100},
  CarouselWrapper: {height: 310, paddingHorizontal: 10, zIndex: 9999},
  carouselItemWrapper: {paddingHorizontal: 10},
  carouselItemView: {
    borderRadius: 8,
    overflow: 'hidden',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
    width: '100%',
  },
  bannerStyle: {height: 187, width: '100%'},
  timmerWrapper: {
    position: 'absolute',
    marginLeft: 0,
    borderBottomRightRadius: 15,
    borderTopRightRadius: 0,
    borderBottomLeftRadius: 0,
  },
  contentWrapper: {width: '100%', backgroundColor: COLORS.WHITE},
  carouselButtonStyle: {
    alignSelf: 'flex-start',
    alignItems: 'center',
    backgroundColor: COLORS.SKY_BLUE,
    paddingVertical: 10,
  },
  customButtonWrapper: {
    borderWidth: 1,
    borderColor: Colors.BLACK,
    borderRadius: 5,
  },
  columnWrapper: {width: '48%', borderRadius: 4, alignItems: 'center'},
  resultItemWrapper: {width: '100%', paddingHorizontal: 20},
  resultItemView: {
    borderRadius: 8,
    overflow: 'hidden',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
    backgroundColor: COLORS.WHITE,
  },
  seasonItemView: {width: '100%', paddingHorizontal: 20, marginVertical: 10},
  seasonButton: {backgroundColor: COLORS.BLACK, borderRadius: 5},
});

export default Dashboard;
