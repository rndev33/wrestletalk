import React, { useRef, useState } from "react";
import { Image, View, TouchableOpacity, ScrollView,StyleSheet } from 'react-native'
import CustomButton from "../../components/CustomButton";
import CustomText from "../../components/CustomText";
import COLORS, { BACK_TABLE, LIGHT_BLACK, SKY_BLUE, TOMATO, WHITE } from "../../constants/Colors";
import { downArrow, dummyBanner, selectedRound, unSelectRound } from "../../constants/Images";
import CommonStyles from "../../style/CommonStyles";
import SpaceStyles from "../../style/SpaceStyles";
import TextStyles from "../../style/TextStyles";
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from "../../constants";
import { FlatList } from "react-native-gesture-handler";
import SelectionModalize from "./SharedModalize";
import LoginComp from "./LoginComponent";

function CurrentRound({
    token
}) {
    const data = [{
        title: 'SmackDown Women’s Championship Match',
        image:dummyBanner,
        activated:false,
        playerData: [{
            name: 'Becky Lynch (c)',
            check: true,
            result:true
        }, {
            name: 'Bianca Belair',
            check: false,
            result:false
        }]
    }, {
        title: 'United States Championship Match',
        image:dummyBanner,
        activated:true,
        playerData: [{
            name: 'Jeff Hardy',
            check: false,
            result:true
        }, {
            name: 'Damien Prist',
            check: true,
            result:false
        }, {
            name: 'Prist Sheamus',
            check: false,
            result:false
        }]
    }]
    const [resultData, setResultData] = useState(data)
    const actions = ['Extreme Rules 2021', 'Survivor Series', 'WWE Royal Rumbles 2023'];
        const [selected, setSelected] = useState(actions[0])
        const pickerMenuModalRef = useRef(null);
        const closeModalPicker = () => {
            pickerMenuModalRef?.current?.close();
          };
          const openModalPicker = () => {
            pickerMenuModalRef?.current?.open();
          };

          const onItemClickF = (item) => {
            setSelected(item)
            closeModalPicker()
          };

    const renderCurrentRound = ({ item, index }) => {

        return (
            <View
                key={index}
                style={[SpaceStyles.paddingBottom5]}
            >
                <View style={{marginBottom:10,borderRadius:8,overflow: 'hidden'}}>
                <Image
                                    resizeMode={'cover'}
                                    source={item.image}
                                    style={[Styles.bannerStyle]}
                                    // style={CommonStyles.currentRoundBannerStyle}
                                    />
                                     <LinearGradient
                                        colors={item.activated ? ['rgba(195, 14, 19, 1)', 'rgba(241, 71, 2, 1)']:['#ffffff', '#ffffff']}
                                        start={{ x: 0.0, y: 1.0 }}
                                        end={{ x: 1.0, y: 1.0 }}
                                        style={[CommonStyles.playerView,{position:'absolute',bottom:0,right:0,borderTopLeftRadius:8}]}>
                                            <CustomText
                    text={item.activated ? 'Joker Pick Activated' : 'Joker Pick?'}
                    style={[item.activated ?TextStyles.medium12White : TextStyles.medium12Orange, ]}
                />
                                        </LinearGradient>
                                    
                                    </View>
                <CustomText
                    text={item?.title}
                    style={[TextStyles.medium12LightBlack, SpaceStyles.top2, SpaceStyles.bottom1]}
                />
                
                {item?.playerData?.map((i, playerIndex) => {
                    const backGroundColor = i.check ? SKY_BLUE : BACK_TABLE
                    const textColor = i.check ? WHITE : LIGHT_BLACK
                    const icon = i.check ? selectedRound : unSelectRound
                    return (
                        <TouchableOpacity
                            onPress={() => {
                                let array = Object.assign([], item?.playerData);
                                array?.map((i, index) => {
                                    if (index == playerIndex) {
                                        i.check = true
                                    }
                                    else {
                                        i.check = false
                                    }
                                })
                                resultData[index].playerData = array
                                setResultData(Object.assign([], resultData))
                            }}
                            activeOpacity={0.6}
                            style={[CommonStyles.playerView, { backgroundColor: backGroundColor }]}>
                            <View style={SpaceStyles.rowFlex}>
                                <Image
                                    resizeMode={'contain'}
                                    source={icon}
                                />
                                <CustomText
                                    text={i?.name}
                                    style={[TextStyles.medium12LightBlack, SpaceStyles.left3, { color: textColor }]}
                                />
                            </View>
                        </TouchableOpacity>
                    )
                })}
            </View>
        )
    }

    const loginComponent = () => {
        return (
            <View style={{ alignItems: 'center' }}>
                <CustomText
                    text={'Wrestle League'}
                    style={[TextStyles.bold18LightBlack, SpaceStyles.top5]}
                />
                <CustomText
                    text={'Login via Patreon to join the Wrestle League  and go head-to-head with fellow patrons and the WrestleTalk team.'}
                    style={[TextStyles.medium12BackText, SpaceStyles.spaceVertical, SpaceStyles.width70, { textAlign: 'center', lineHeight: 18 }]}
                />
                <CustomButton
                    text={'BECOME A PATRON'}
                    containerStyle={SpaceStyles.spaceVertical}
                />
                <CustomButton
                    text={'LOGIN WITH PATREON'}
                    containerStyle={{ backgroundColor: TOMATO }}
                />
            </View>
        )
    }
if(!token){
  return <LoginComp />
}
        return (
            <ScrollView>
            <Image
                resizeMode={'stretch'}
                source={dummyBanner}
                style={CommonStyles.currentRoundBannerStyle}
            />
            <TouchableOpacity onPress={()=>openModalPicker()} style={[{borderWidth:1,borderColor:COLORS.LIGHT_BORDER,width:200,alignSelf:'center',padding:10,marginTop:20},SpaceStyles.alignSpaceBlock]}>
                 <CustomText
                    text={selected}
                    style={[TextStyles.bold14LightBlack, { alignSelf: 'center' }]}
                />
                <Image
                source={downArrow}
                style={{height:8,width:8,tintColor:COLORS.LIGHT_BLACK}}
                resizeMode="contain"
                />
            </TouchableOpacity>
                {/* <CustomText
                    text={'Extreme Rules 2021'}
                    style={[TextStyles.bold18LightBlack, SpaceStyles.top2, { alignSelf: 'center' }]}
                /> */}
                <View style={[SpaceStyles.rowFlex, SpaceStyles.vertical1, { alignSelf: 'center' }]}>
                    <CustomText
                        text={'This round closes in'}
                        style={TextStyles.medium10CloseText}
                    />
                    <LinearGradient
                        colors={['rgba(195, 14, 19, 1)', 'rgba(241, 71, 2, 1)']}
                        start={{ x: 0.0, y: 1.0 }}
                        end={{ x: 1.0, y: 1.0 }}
                        style={CommonStyles.timerView}
                    >
                        <CustomText
                            text={'2d:11h:35m:28s'}
                            style={TextStyles.bold10White}
                        />
                    </LinearGradient>
                </View>
                <FlatList
                    data={resultData}
                    scrollEnabled={false}
                    keyboardShouldPersistTaps="handled"
                    keyExtractor={(item, index) => index.toString()}
                    showsVerticalScrollIndicator={false}
                    renderItem={renderCurrentRound}
                    contentContainerStyle={[SpaceStyles.spaceHorizontal,]}
                />
                <CustomButton
                    text={'SUBMIT'}
                    containerStyle={[SpaceStyles.spaceHorizontal]}
                />
                <View style={{height:100}}/>
                <SelectionModalize
                 init={(s) => {
                    pickerMenuModalRef.current = s;
                  }}
                  data={actions}
                  closeModal={closeModalPicker}
                  selected={selected}
                  onItemClick={onItemClickF}
                />
            </ScrollView>
        )
}


const Styles = StyleSheet.create({
wrapper:{paddingBottom:100},
CarouselWrapper:{height:310,paddingHorizontal:10,zIndex:9999},
carouselItemWrapper:{paddingHorizontal:10,width:350},
carouselItemView:{borderRadius:8,overflow:'hidden', shadowColor: "#000",
shadowOffset: {
	width: 0,
	height: 3,
},
shadowOpacity: 0.27,
shadowRadius: 4.65,
elevation: 6,width:'100%'},
bannerStyle:{height:187,width:'100%'},
timmerWrapper:{position:'absolute',marginLeft:0,borderBottomRightRadius:15,borderTopRightRadius:0,borderBottomLeftRadius:0},
contentWrapper:{width:'100%',backgroundColor:COLORS.WHITE},
carouselButtonStyle:{alignSelf:'flex-start', alignItems:'center',backgroundColor:COLORS.SKY_BLUE,paddingVertical:10},
customButtonWrapper:{borderWidth:1, borderColor:Colors.BLACK,borderRadius:5},
columnWrapper:{width:'48%',borderRadius:4, alignItems:'center'},
resultItemWrapper:{width:'100%',paddingHorizontal:20},
resultItemView:{borderRadius:8,overflow:'hidden', shadowColor: "#000",
shadowOffset: {
	width: 0,
	height: 3,
},
shadowOpacity: 0.27,
shadowRadius: 4.65,
elevation: 6,backgroundColor:COLORS.WHITE},
seasonItemView:{width:'100%',paddingHorizontal:20,marginVertical:10},
seasonButton:{backgroundColor:COLORS.BLACK,borderRadius:5},

})

export default CurrentRound