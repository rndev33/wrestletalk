import React, {  } from "react";
import { Image, View, TouchableOpacity } from 'react-native'
import CustomText from "../../components/CustomText";
import SpaceStyles from "../../style/SpaceStyles";
import TextStyles from "../../style/TextStyles";
import { Modalize } from 'react-native-modalize';
import { Portal } from "react-native-portalize";
import { cancleIcon } from "../../constants/Images";
import COLORS from "../../constants/Colors";

const SelectionModalize = (props) => {
    return(
        <Portal>
          <Modalize ref={props.init} adjustToContentHeight={true}
          HeaderComponent={()=>
          <TouchableOpacity style={[{width:20,alignSelf:'flex-end'},SpaceStyles.horizontal3,SpaceStyles.top2]} onPress={()=>props.closeModal()}>
            <Image
            source={cancleIcon}
            style={[{height:13,width:13,tintColor:COLORS.BLACK,alignSelf:'flex-end',}]}
            resizeMode="contain"
            />
          </TouchableOpacity>}
          >
            <View style={SpaceStyles.horizontal3}>
              {props?.data?.map((item) => {
                let emptyOption = item?.title == 'Select Season' || item?.title == 'Select Event'
                return (
                    <>{
                        emptyOption ? <></> : <TouchableOpacity
                        disabled={false}
                        onPress={() =>props.onItemClick(item)}
                        style={[{
                          borderBottomWidth: 1,
                          borderBottomColor:COLORS.ULTRA_LIGHT_GRAY
                        }]}>
                            <CustomText
                    text={item?.title}
                    style={[props.selected == item?.title ? TextStyles.medium12Orange : TextStyles.medium12LightBlack, SpaceStyles.top2, SpaceStyles.bottom2, SpaceStyles.bottom1]}
                />
                      </TouchableOpacity>
                    }</>
                 
                );
              })}
            </View>
          </Modalize>
      </Portal>
    )
}

export default SelectionModalize