import React, { useEffect } from "react";
import { Image, View, FlatList } from 'react-native'
import CustomText from "../../components/CustomText";
import { BACK_TABLE, WHITE } from "../../constants/Colors";
import { dummyBanner } from "../../constants/Images";
import CommonStyles from "../../style/CommonStyles";
import SpaceStyles from "../../style/SpaceStyles";
import TextStyles from "../../style/TextStyles";

function PastSeasons({

}) {
    const data = [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}]

    const renderStandingsData = ({ item, index }) => {
        const color = index % 2 == 0 ? WHITE : BACK_TABLE
        return (
            <View
                key={index}
                style={[CommonStyles.standingBlock, { backgroundColor: color }]}>
                <View style={[SpaceStyles.width55, SpaceStyles.paddingLeft3]}>
                    <CustomText
                        text={`Wrestle League Season ${12 - index}`}
                        style={TextStyles.medium12StadingText}
                    />
                </View>
                <View style={SpaceStyles.width20}>
                    <CustomText
                        text={'18/12/2021'}
                        style={TextStyles.medium12StadingText}
                        numberOfLines={1}
                    />
                </View>
            </View>
        )
    }
    return (
        <>
            <Image
                resizeMode={'stretch'}
                source={dummyBanner}
                style={CommonStyles.currentRoundBannerStyle}
            />
            <CustomText
                text={'Past Seasons'}
                style={[TextStyles.bold18LightBlack, SpaceStyles.top2, { alignSelf: 'center' }]}
            />
            <View style={[SpaceStyles.horizontal10, SpaceStyles.alignSpaceBlock, SpaceStyles.spaceVertical]}>
                <View style={[SpaceStyles.width55, SpaceStyles.paddingLeft3]}>
                    <CustomText
                        text={'Seasons'}
                        style={TextStyles.bold12LightBlack}
                    />
                </View>
                <View style={SpaceStyles.width20}>
                    <CustomText
                        text={'End Date'}
                        style={TextStyles.bold12LightBlack}
                    />
                </View>
            </View>
            <FlatList
                data={data}
                keyboardShouldPersistTaps="handled"
                keyExtractor={(item, index) => index.toString()}
                showsVerticalScrollIndicator={false}
                renderItem={renderStandingsData}
                contentContainerStyle={[SpaceStyles.horizontal10, SpaceStyles.paddingBottom2]}
            />
        </>
    )
}

export default PastSeasons