import React, { useState } from "react";
import { Image, View, FlatList, TouchableOpacity } from 'react-native'
import CustomText from "../../components/CustomText";
import { dummyBanner } from "../../constants/Images";
import CommonStyles from "../../style/CommonStyles";
import SpaceStyles from "../../style/SpaceStyles";
import TextStyles from "../../style/TextStyles";
import LinearGradient from 'react-native-linear-gradient';
import { STANDING_TEXT, WHITE } from "../../constants/Colors";

function Standings({

}) {
    const data = [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}]

    const [activeIndex, setActiveIndex] = useState(null)

    const renderStandingsData = ({ item, index }) => {
        const backGroundColor = activeIndex == index ? ['rgba(195, 14, 19, 1)', 'rgba(241, 71, 2, 1)'] : ['#ffffff', '#ffffff']
        const textColor = activeIndex == index ? WHITE : STANDING_TEXT
        return (
            <LinearGradient
                colors={backGroundColor}
                start={{ x: 0.0, y: 1.0 }}
                end={{ x: 1.0, y: 1.0 }}
                key={index}
            >
                <TouchableOpacity
                    style={CommonStyles.standingBlock}
                    onPress={() => setActiveIndex(index)}
                    activeOpacity={0.6}
                >
                    <View style={[SpaceStyles.width15, { alignItems: 'center' }]}>
                        <CustomText
                            text={index + 1}
                            style={[TextStyles.medium12StadingText, { color: textColor }]}
                        />
                    </View>
                    <View style={SpaceStyles.width45}>
                        <CustomText
                            text={'Terminator1000'}
                            style={[TextStyles.medium12StadingText, { color: textColor }]}
                            numberOfLines={1}
                        />
                    </View>
                    <View style={[SpaceStyles.width15, { alignItems: 'center' }]}>
                        <CustomText
                            text={'398'}
                            style={[TextStyles.medium12StadingText, { color: textColor }]}
                        />
                    </View>
                </TouchableOpacity>
            </LinearGradient>
        )
    }
    return (
        <>
            <Image
                resizeMode={'stretch'}
                source={dummyBanner}
                style={CommonStyles.currentRoundBannerStyle}
            />
            <CustomText
                text={'Standings'}
                style={[TextStyles.bold18LightBlack, SpaceStyles.top2, { alignSelf: 'center' }]}
            />
            <View style={[SpaceStyles.horizontal10, SpaceStyles.alignSpaceBlock, SpaceStyles.spaceVertical]}>
                <View style={[SpaceStyles.width15, { alignItems: 'center' }]}>
                    <CustomText
                        text={'Rank'}
                        style={TextStyles.bold12LightBlack}
                    />
                </View>
                <View style={SpaceStyles.width45}>
                    <CustomText
                        text={'Username'}
                        style={TextStyles.bold12LightBlack}
                    />
                </View>
                <View style={[SpaceStyles.width15, { alignItems: 'center' }]}>
                    <CustomText
                        text={'Points'}
                        style={TextStyles.bold12LightBlack}
                    />
                </View>
            </View>
            <FlatList
                data={data}
                keyboardShouldPersistTaps="handled"
                keyExtractor={(item, index) => index.toString()}
                showsVerticalScrollIndicator={false}
                renderItem={renderStandingsData}
                contentContainerStyle={[SpaceStyles.horizontal10, SpaceStyles.paddingBottom2]}
            />
        </>
    )
}

export default Standings