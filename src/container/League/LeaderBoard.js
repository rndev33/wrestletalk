import React, { useEffect, useRef, useState } from "react";
import { Image, View, TouchableOpacity, ScrollView,StyleSheet,Portal } from 'react-native'
import CustomButton from "../../components/CustomButton";
import CustomText from "../../components/CustomText";
import COLORS, { BACK_TABLE, LIGHT_BLACK, SKY_BLUE, TOMATO, WHITE } from "../../constants/Colors";
import { downArrow, dummyBanner, selectedRound, unSelectRound } from "../../constants/Images";
import CommonStyles from "../../style/CommonStyles";
import SpaceStyles from "../../style/SpaceStyles";
import TextStyles from "../../style/TextStyles";
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from "../../constants";
import { FlatList } from "react-native-gesture-handler";
import { Modalize } from 'react-native-modalize';
import SelectionModalize from "./SharedModalize";
import { getLeaderBoardData } from "../../redux/services/leagueServices";
import LoaderModal from "../../components/LoaderModal";
import LoginComp from "./LoginComponent";

function LeaderBoard({
    token
}) {
    const [loading, setLoading] = useState(false);
    const [data, setData] = useState([])
    useEffect(() => {
        getData();
      }, []);
    
      const getData = () => {
        setLoading(true);
        getLeaderBoardData()
          .then(res => {
            setData(res?.data?.scores)
          })
          .catch(err => {})
          .finally(() => setLoading(false));
      };

      if(!token){
        return <LoginComp />
      }
    return (
        <>
        <LoaderModal loading={loading} />
        <ScrollView>
        <Image
                resizeMode={'stretch'}
                source={dummyBanner}
                style={CommonStyles.currentRoundBannerStyle}
            />
            <CustomText
                text={'Leaderboard'}
                style={[TextStyles.bold18LightBlack, SpaceStyles.top2,SpaceStyles.paddingBottom2, { alignSelf: 'center' }]}
            />
            <View style={[SpaceStyles.alignSpaceBlock, SpaceStyles.spaceHorizontal,SpaceStyles.spaceVertical,SpaceStyles.horizontal3]}>
                <View style={[{width:'20%'}]}>
                <CustomText
                text={'Rank'}
                style={[TextStyles.bold14MainBlack,]}
            />
                </View>
                <View style={[{width:'60%'}]}>
                <CustomText
                text={'Username'}
                style={[TextStyles.bold14MainBlack,]}
            />
                </View>
                <View style={[{width:'20%',alignItems:'flex-end'}]}>
                <CustomText
                text={'Points'}
                style={[TextStyles.bold14MainBlack,]}
            />
                </View>
            </View>
            <FlatList data={data}
            scrollEnabled={false}
            ListEmptyComponent={()=><View style={{alignItems:'center',justifyContent:'center'}}>
                <CustomText
                text={loading ? 'Loading, please wait...' : 'No Record Found!'}
                style={[TextStyles.medium14LightBorder]}
            />
            </View>
            }
            renderItem={({item})=>{
                return(
                    <LinearGradient
                    colors={item.is_me ? ['rgba(195, 14, 19, 1)', 'rgba(241, 71, 2, 1)'] : ['transparent','transparent']}
                    start={{ x: 0.0, y: 1.0 }}
                    end={{ x: 1.0, y: 1.0 }} style={[SpaceStyles.alignSpaceBlock, SpaceStyles.spaceHorizontal,SpaceStyles.paddingVertical2,SpaceStyles.horizontal3]}>
                <View style={[{width:'20%'}]}>
                <CustomText
                text={item.rank}
                style={[item.is_me ? TextStyles.medium14ReviewWhite :TextStyles.medium14LightBorder,]}
            />
                </View>
                <View style={[{width:'60%',flexDirection:'row',alignItems:'center'}]}>
                <CustomText
                text={item.name}
                style={[item.is_me ? TextStyles.medium14ReviewWhite :TextStyles.medium14LightBorder,]}
            />
            {item.is_team && <LinearGradient
                    colors={['rgba(195, 14, 19, 1)', 'rgba(241, 71, 2, 1)']}
                    start={{ x: 0.0, y: 1.0 }}
                    end={{ x: 1.0, y: 1.0 }} style={{paddingVertical:6,paddingHorizontal:10,borderRadius:4,marginLeft:5}}>
                        <CustomText
                text={'WT Team'}
                style={[TextStyles.medium12White]}
            />
                    </LinearGradient>}
                </View>
                <View style={[{width:'20%',alignItems:'flex-end'}]}>
                <CustomText
                text={item.points}
                style={[item.is_me ? TextStyles.medium14ReviewWhite :TextStyles.medium14LightBorder,]}
            />
                </View>
            </LinearGradient>
                )
            }}
            
            />
            <View style={{height:100}}/>
        </ScrollView>
        </>
    )
}


const Styles = StyleSheet.create({
wrapper:{paddingBottom:100},
CarouselWrapper:{height:310,paddingHorizontal:10,zIndex:9999},
carouselItemWrapper:{paddingHorizontal:10,width:350},
carouselItemView:{borderRadius:8,overflow:'hidden', shadowColor: "#000",
shadowOffset: {
	width: 0,
	height: 3,
},
shadowOpacity: 0.27,
shadowRadius: 4.65,
elevation: 6,width:'100%'},
bannerStyle:{height:187,width:'100%'},
timmerWrapper:{position:'absolute',marginLeft:0,borderBottomRightRadius:15,borderTopRightRadius:0,borderBottomLeftRadius:0},
contentWrapper:{width:'100%',backgroundColor:COLORS.WHITE},
carouselButtonStyle:{alignSelf:'flex-start', alignItems:'center',backgroundColor:COLORS.SKY_BLUE,paddingVertical:10},
customButtonWrapper:{borderWidth:1, borderColor:Colors.BLACK,borderRadius:5},
columnWrapper:{width:'48%',borderRadius:4, alignItems:'center'},
resultItemWrapper:{width:'100%',paddingHorizontal:20},
resultItemView:{borderRadius:8,overflow:'hidden', shadowColor: "#000",
shadowOffset: {
	width: 0,
	height: 3,
},
shadowOpacity: 0.27,
shadowRadius: 4.65,
elevation: 6,backgroundColor:COLORS.WHITE},
seasonItemView:{width:'100%',paddingHorizontal:20,marginVertical:10},
seasonButton:{backgroundColor:COLORS.BLACK,borderRadius:5},

})

export default LeaderBoard