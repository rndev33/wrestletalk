import React, { useEffect, useRef, useState } from "react";
import { Image, View, ScrollView,StyleSheet, TouchableOpacity } from 'react-native'
import CustomText from "../../components/CustomText";
import COLORS, {  } from "../../constants/Colors";
import { downArrow, dummyBanner } from "../../constants/Images";
import CommonStyles from "../../style/CommonStyles";
import SpaceStyles from "../../style/SpaceStyles";
import TextStyles from "../../style/TextStyles";
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from "../../constants";
import { FlatList } from "react-native-gesture-handler";
import LoaderModal from "../../components/LoaderModal";
import { getMyScoreData, getResultsData, getResultsDataById } from "../../redux/services/leagueServices";
import SelectionModalize from "./SharedModalize";
import LoginComp from "./LoginComponent";

function MyScoreComponent({
token
}) 
    {

        const [loading, setLoading] = useState(false);
        const [currentSeason, setCurrentSeason] = useState(null)
        const [eventSelected, setEventSelected] = useState('')
        const [totalPoints, setTotalPoints] = useState(0)
        const [eventMatches, setEventMatches] = useState([])
        const [eventPoints, setEventPoints] = useState(0)
        const pickerEventModalRef = useRef(null);
        const closeEventModalPicker = () => {
            pickerEventModalRef?.current?.close();
          };
          const openEventModalPicker = () => {
            pickerEventModalRef?.current?.open();
          };

        
          useEffect(() => {
            getData();
          }, []);
        
          const getData =  () => {
            setLoading(true);
             getMyScoreData()
              .then( res => {
             setCurrentSeason(res?.data?.current_season)
                setEventSelected(res?.data?.current_season?.events[0].title)
                setEventMatches(res?.data?.current_season?.events[0].matches)
                setTotalPoints(res?.data?.total_points)
                let eventPoints = 0
                res?.data?.current_season?.events?.map((item)=>{
                    if(item.id == res?.data?.current_season?.events[0].id){
                        item.matches.map((m)=>{
                            eventPoints = eventPoints+m.points
                        })
                    }
                })
                setEventPoints(eventPoints)
              })
              .catch(err => {})
              .finally(() => setLoading(false));
          };
    
          const onEventItemClick = (item) => {
            setEventSelected(item?.title)
            fetchMatches(item?.id)
            closeEventModalPicker()
          };

          const fetchMatches = (id) => {
            let eventPoints = 0
            currentSeason?.events?.map((item)=>{
                if(item.id == id){
                    setEventMatches(item?.matches)
                    item.matches.map((m)=>{
                        eventPoints = eventPoints+m.points
                    })
                }
            })
            setEventPoints(eventPoints)
          }
          if(!token){
            return <LoginComp />
          }
      
        return (
            <>
            <LoaderModal loading={loading} />
            <ScrollView>
            <Image
                resizeMode={'stretch'}
                source={dummyBanner}
                style={CommonStyles.currentRoundBannerStyle}
            />
                <CustomText
                    text={'My Score'}
                    style={[TextStyles.bold18LightBlack, SpaceStyles.top2,SpaceStyles.paddingBottom2, { alignSelf: 'center' }]}
                />
                <View style={[SpaceStyles.spaceHorizontal,SpaceStyles.alignSpaceBlock]}>
                    <View style={[Styles.columnWrapper,SpaceStyles.horizontal3,SpaceStyles.paddingVertical2, {backgroundColor:COLORS.SKY_BLUE}]}>
                    <CustomText
                    text={'Event Points'}
                    style={[TextStyles.bold12White,]}
                />
                <CustomText
                    text={eventPoints}
                    style={[TextStyles.bold25TitleWhite,SpaceStyles.paddingTop2]}
                />
                    </View>
                    <LinearGradient
                        colors={['rgba(195, 14, 19, 1)', 'rgba(241, 71, 2, 1)']}
                        start={{ x: 0.0, y: 1.0 }}
                        end={{ x: 1.0, y: 1.0 }}
                        style={[Styles.columnWrapper,SpaceStyles.horizontal3,SpaceStyles.paddingVertical2]}>
                        <CustomText
                    text={'Total Season Points'}
                    style={[TextStyles.bold12White,]}
                />
                <CustomText
                    text={totalPoints}
                    style={[TextStyles.bold25TitleWhite,SpaceStyles.paddingTop2]}
                />
                    </LinearGradient>
                </View>

                <CustomText
                    text={currentSeason?.title}
                    style={[TextStyles.bold14LightBlack, SpaceStyles.top2,SpaceStyles.paddingVertical2, { alignSelf: 'center' }]}
                />

<TouchableOpacity onPress={()=>openEventModalPicker()} style={[{borderWidth:1,borderColor:COLORS.LIGHT_BORDER,width:'90%',alignSelf:'center',padding:10,},SpaceStyles.alignSpaceBlock]}>
                 <CustomText
                    text={eventSelected}
                    style={[TextStyles.bold14LightBlack, { alignSelf: 'center' }]}
                />
                <Image
                source={downArrow}
                style={{height:8,width:8,tintColor:COLORS.LIGHT_BLACK}}
                resizeMode="contain"
                />
            </TouchableOpacity>

                <View>
<FlatList
                                data={eventMatches}
                                contentContainerStyle={[SpaceStyles.paddingTop2]}
                                renderItem={({item})=>{

                                    return(
                                        <View style={[Styles.resultItemWrapper,SpaceStyles.paddingBottom2]}>
                                               <View style={[Styles.resultItemView,SpaceStyles.rowJustify,SpaceStyles.horizontal3,SpaceStyles.paddingVertical2]}>
                                        <View>
                <CustomText
                                text={item.title}
                                style={[TextStyles.bold14LightBlack,{width:310,lineHeight:18}]}
                            />
                             <CustomText
                                text={`Winner - ${item?.answer}`}
                                style={[TextStyles.medium12LightBorder,{paddingTop:10}]}
                            />
                             <CustomText
                                text={`Your choice - ${currentSeason?.prediction ? item?.answer : ''}`}
                                style={[TextStyles.medium12LightBorder,{paddingTop:10}]}
                            />
                </View>
                <View>
                <CustomText
                                text={`+${item.points}`}
                                style={[TextStyles.bold14LightBlack,]}
                            />
                </View>
                                    
                                    </View>
                                    </View> 
                                    )
                                }}
                                />
                    
                </View>
                <View style={{height:100}}/>
                <SelectionModalize
      init={(s) => {
         pickerEventModalRef.current = s;
       }}
       data={currentSeason?.events}
       closeModal={closeEventModalPicker}
       selected={eventSelected}
       onItemClick={onEventItemClick}
     />
            </ScrollView>
            </>
        )
    }


const Styles = StyleSheet.create({
wrapper:{paddingBottom:100},
CarouselWrapper:{height:310,paddingHorizontal:10,zIndex:9999},
carouselItemWrapper:{paddingHorizontal:10,width:350},
carouselItemView:{borderRadius:8,overflow:'hidden', shadowColor: "#000",
shadowOffset: {
	width: 0,
	height: 3,
},
shadowOpacity: 0.27,
shadowRadius: 4.65,
elevation: 6,width:'100%'},
bannerStyle:{height:187,width:'100%'},
timmerWrapper:{position:'absolute',marginLeft:0,borderBottomRightRadius:15,borderTopRightRadius:0,borderBottomLeftRadius:0},
contentWrapper:{width:'100%',backgroundColor:COLORS.WHITE},
carouselButtonStyle:{alignSelf:'flex-start', alignItems:'center',backgroundColor:COLORS.SKY_BLUE,paddingVertical:10},
customButtonWrapper:{borderWidth:1, borderColor:Colors.BLACK,borderRadius:5},
columnWrapper:{width:'48%',borderRadius:4, alignItems:'center'},
resultItemWrapper:{width:'100%',paddingHorizontal:20},
resultItemView:{borderRadius:8,overflow:'hidden', shadowColor: "#000",
shadowOffset: {
	width: 0,
	height: 3,
},
shadowOpacity: 0.27,
shadowRadius: 4.65,
elevation: 6,backgroundColor:COLORS.WHITE},
seasonItemView:{width:'100%',paddingHorizontal:20,marginVertical:10},
seasonButton:{backgroundColor:COLORS.BLACK,borderRadius:5},

})

export default MyScoreComponent