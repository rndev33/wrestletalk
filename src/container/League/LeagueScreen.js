import React, { useEffect, useState } from 'react';
import {View} from 'react-native';
import HeaderComponent from '../../components/HeaderComponent';
import TopTabComponent from '../../components/TopTabComponent';
import {
  CURRENT_ROUND,
  RESULTS,
  DASHBOARD,
  MY_SCORE,
  LEADERBOARD,
  getData
} from '../../constants/GlobalFunction';
import CommonStyles from '../../style/CommonStyles';
import CurrentRound from './CurrentRound';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import {SafeAreaView} from 'react-native';
import {PRIMARY_COLOR, TOMATO} from '../../constants/Colors';
import Results from './Results';
import LeaderBoard from './LeaderBoard';
import MyScoreComponent from './MyScore';
import Dashboard from './Dashboard';
import { Variables } from '../../constants/Variables';
import CustomText from '../../components/CustomText';
import TextStyles from '../../style/TextStyles';
import SpaceStyles from '../../style/SpaceStyles';
import CustomButton from '../../components/CustomButton';
import LoginComp from './LoginComponent';

const adUnitId =
  Platform.OS === 'ios'
    ? 'ca-app-pub-7858948897424048/7024961746'
    : 'ca-app-pub-7858948897424048/2828650148';

function LeagueScreen(props) {
  const [tokenn , settokenn] = useState(null)
   
  const {navigation} = props;
  let token = null
  useEffect(()=>{
    get_token()
  },[])
  const get_token = async () =>{
    token = await getData(Variables?.token).then(res => {
      console.log('--- res --- res --- res --- get_toke n', res);
      settokenn(res)
   

        return res;
     
     
    }).catch((err)=>{
      settokenn(null)
      return null;
      

    })
    console.log("tokentokentokentoken ----token >>",token)
    if(!token){
      // navigation.navigate("LoginScreen")
    }
  }

  // if(!tokenn){
  //   return <LoginComp />

  // }
  return (
    <View style={CommonStyles.mainContainer}>
      <SafeAreaView style={{backgroundColor: PRIMARY_COLOR}} />
      <HeaderComponent />

      <ScrollableTabView
        initialPage={0}
        renderTabBar={() => <TopTabComponent isLeagueScreen={true} />}>
          <Dashboard token={tokenn} tabLabel={DASHBOARD}/>
          {/* <CurrentRound screen={'dashboard'}  /> */}
          <CurrentRound token={tokenn} tabLabel={CURRENT_ROUND} />
          <Results token={tokenn} tabLabel={RESULTS}/>
          {/* <CurrentRound screen={'results'} tabLabel={'RESULTS'} /> */}
          <LeaderBoard token={tokenn} tabLabel={LEADERBOARD}/>
          {/* <CurrentRound screen={'leaderboard'} tabLabel={'LEADERBOARD'} /> */}
          <MyScoreComponent token={tokenn} tabLabel={MY_SCORE}/>
          {/* <CurrentRound screen={'myScore'} tabLabel={'MY SCORE'} /> */}
        {/* <CurrentRound tabLabel={CURRENT_ROUND} />
        <CurrentRound tabLabel={RESULTS} />
        <CurrentRound tabLabel={STANDINGS} />
        <CurrentRound tabLabel={PAST_SEASONS} /> */}
      </ScrollableTabView>
    </View>
  );
}

export default LeagueScreen;
