import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { useState } from "react";
import { SafeAreaView } from "react-native";
import { View } from "react-native";
import { WebView } from "react-native-webview";
import HeaderComponent from "../../components/HeaderComponent";
import { PRIMARY_COLOR } from "../../constants/Colors";
import CommonStyles from "../../style/CommonStyles";

const WebViewScreen = ({ navigation }) => {
    const [showAuth, setShowAuth] = React.useState(false)
    const [apiKey, setApiKey] = React.useState(null)
    let webview = null

    const onLoadProgress = ({ nativeEvent }) => {
        if (nativeEvent.progress === 1) {
            onNavigationStateChange(nativeEvent)
        }
    }

    const onNavigationStateChange = async (navigationState) => {
        const { url } = navigationState

        if (url.includes('connect/key?data=')) {
            const [found, key] = url.match(/connect\/key\?data=([\w]+)/)
            console.log(key, "0000000key0000000");
            if (key) {
                await AsyncStorage.setItem("leagueApiKey", key);
                setApiKey(key)
                setShowAuth(false)
            }
        }
    }

    return (
        <View style={CommonStyles.mainContainer}>
            <SafeAreaView style={{ backgroundColor: PRIMARY_COLOR }} />
            <HeaderComponent />
            <WebView
                ref={ref => webview = ref}
                source={{ uri: 'https://wrestletalk.com/connect/?response_type=key' }}
                showsVerticalScrollIndicator={false}
                startInLoadingState
                onLoadProgress={onLoadProgress}
                onNavigationStateChange={onNavigationStateChange}
                androidHardwareAccelerationDisabled={true}
            />
        </View>
    );
};

export default WebViewScreen;
