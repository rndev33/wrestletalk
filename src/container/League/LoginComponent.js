//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import CustomText from '../../components/CustomText';
import CustomButton from '../../components/CustomButton';
import TextStyles from '../../style/TextStyles';
import SpaceStyles from '../../style/SpaceStyles';
import { useNavigation } from '@react-navigation/native';

// create a component
const LoginComp = ({}) => {
 const navigation =  useNavigation()
    return (
        <View style={{ alignItems: 'center' }}>
            <CustomText
                text={'Login'}
                style={[TextStyles.bold18LightBlack, SpaceStyles.top5]}
            />
            <CustomText
                text={'Login to see contant'}
                style={[TextStyles.medium12BackText, SpaceStyles.spaceVertical, SpaceStyles.width70, { textAlign: 'center', lineHeight: 18 }]}
            />
            <CustomButton
            onPress={()=>{
                navigation.navigate("LoginScreen")
            }}
                text={'LOGIN TO CONTINUE'}
                containerStyle={SpaceStyles.spaceVertical}
            />
        
        </View>
    )
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
});

//make this component available to the app
export default LoginComp;
