import React, {useEffect, useState} from 'react';
import {View, TouchableOpacity, Image, ScrollView} from 'react-native';
import CustomButton from '../../components/CustomButton';
import CustomText from '../../components/CustomText';
import HeaderComponent from '../../components/HeaderComponent';
import {BLACK, PRIMARY_COLOR} from '../../constants/Colors';
import {
  DESCRIPTION,
  MAGAZINE,
  REVIEWS,
  SHIPPING_DELIVERY,
  SIZE_CHART,
} from '../../constants/GlobalFunction';
import {downArrow, redBackIcon} from '../../constants/Images';
import CommonStyles from '../../style/CommonStyles';
import SpaceStyles from '../../style/SpaceStyles';
import TextStyles from '../../style/TextStyles';
import {Menu, MenuItem, MenuDivider} from 'react-native-material-menu';
import {addCartApiCall} from '../../redux/services/shopServices';
import {useDispatch} from 'react-redux';
import {getAllCartData} from '../../redux/action/CartAction';
import LoaderModal from '../../components/LoaderModal';
import ProductDescription from './ProductDetail/ProductDescription';
import ProductSizeChart from './ProductDetail/ProductSizeChart';
import ProductShippingDelivery from './ProductDetail/ProductShippingDelivery';
import ProductReview from './ProductDetail/ProductReview';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import TopTabComponent from '../../components/TopTabComponent';
import {SafeAreaView} from 'react-native';

function ProductDetailScreen(props) {
  const {navigation} = props;
  const dispatch = useDispatch();
  const {productDetail, screen} = props?.route?.params;

  const [openColorMenu, setOpenColorMenu] = useState(false);
  const [openSizeMenu, setOpenSizeMenu] = useState(false);
  const [colorArray, setColorArray] = useState([]);
  const [sizeArray, setSizeArray] = useState([]);
  const [defaultColor, setDefaultColor] = useState('');
  const [defaultSize, setDefaultSize] = useState('');
  const [defaultPrice, setDefaultPrice] = useState('');
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    getColorCall();
    getSizeCall();
  }, []);

  useEffect(() => {
    getPriceCall();
  }, [defaultColor, defaultSize]);

  const getColorCall = () => {
    let array = [];
    productDetail?.variants?.map(i => {
      array.push(i.option1);
    });
    let newArray = [...new Set(array)];
    setColorArray(newArray);
    setDefaultColor(newArray[0]);
  };

  const getSizeCall = () => {
    let array = [];
    productDetail?.variants?.map(i => {
      array.push(i.option2);
    });
    let newArray = [...new Set(array)];
    setSizeArray(newArray);
    setDefaultSize(newArray[0]);
  };

  const getPriceCall = () => {
    productDetail?.variants?.map(i => {
      if (i.option1 == defaultColor && i.option2 == defaultSize) {
        setDefaultPrice(i.price);
      }
    });
  };

  const addToCart = () => {
    productDetail?.variants?.map(i => {
      if (i.option1 == defaultColor && i.option2 == defaultSize) {
        const data = {
          items: [
            {
              id: i.id,
              quantity: 1,
            },
          ],
        };
        setLoading(true);
        addCartApiCall(data)
          .then(res => {
            dispatch(getAllCartData());
            setLoading(false);
          })
          .catch(err => {
            setLoading(false);
          });
      }
    });
  };

  return (
    <View style={CommonStyles.mainContainer}>
      <SafeAreaView style={{backgroundColor: PRIMARY_COLOR}} />
      <HeaderComponent />
      {loading && <LoaderModal loading={loading} />}
      <View style={CommonStyles.productDetailContainer}>
        <Image
          source={{
            uri:
              screen == MAGAZINE
                ? productDetail?.images[0]?.src
                : productDetail?.image?.src,
          }}
          resizeMode={'contain'}
          style={{alignSelf: 'center', height: 185, width: 185}}
        />
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={CommonStyles.detailBackArrow}>
          <Image source={redBackIcon} style={{height: 35, width: 35}} />
        </TouchableOpacity>
        {/* <TouchableOpacity style={CommonStyles.nextArrow}>
                        <Image source={rightRoundArrow} style={{ height: 35, width: 35 }} />
                    </TouchableOpacity> */}

        <View style={[SpaceStyles.alignSpaceBlock, SpaceStyles.top2]}>
          <CustomText
            text={productDetail?.title}
            style={[TextStyles.medium14LightBorder, SpaceStyles.width70]}
          />
          <CustomText
            text={`£${defaultPrice}`}
            style={[TextStyles.bold16LightBlack]}
          />
        </View>

        <View style={[SpaceStyles.alignSpaceBlock, SpaceStyles.top2]}>
          <TouchableOpacity
            onPress={() => setOpenColorMenu(true)}
            style={CommonStyles.sizeBoxView}>
            <CustomText text={defaultColor} style={[TextStyles.bold10Black]} />
            <Image
              source={downArrow}
              resizeMode={'contain'}
              style={{tintColor: BLACK}}
            />
          </TouchableOpacity>
          <View style={CommonStyles.colorMenuPositionStyle}>
            <Menu
              anchor={() => {
                <Image
                  resizeMode={'contain'}
                  source={downArrow}
                  style={{tintColor: BLACK}}
                />;
              }}
              visible={openColorMenu}
              onRequestClose={() => setOpenColorMenu(false)}
              style={SpaceStyles.width43}>
              {colorArray?.map(i => {
                return (
                  <>
                    <MenuItem
                      onPress={() => (
                        setDefaultColor(i), setOpenColorMenu(false)
                      )}>
                      <CustomText text={i} style={TextStyles.bold10Black} />
                    </MenuItem>
                    <MenuDivider />
                  </>
                );
              })}
            </Menu>
          </View>
          {defaultSize != null && (
            <>
              <TouchableOpacity
                onPress={() => setOpenSizeMenu(true)}
                style={CommonStyles.sizeBoxView}>
                <CustomText
                  text={defaultSize}
                  style={[TextStyles.bold10Black]}
                />
                <Image source={downArrow} style={{tintColor: BLACK}} />
              </TouchableOpacity>
              <View style={CommonStyles.sizeMenuPositionStyle}>
                <Menu
                  anchor={() => {
                    <Image
                      resizeMode={'contain'}
                      source={downArrow}
                      style={{tintColor: BLACK}}
                    />;
                  }}
                  visible={openSizeMenu}
                  onRequestClose={() => setOpenSizeMenu(false)}
                  style={SpaceStyles.width43}>
                  {sizeArray?.map(i => {
                    return (
                      <>
                        <MenuItem
                          onPress={() => (
                            setDefaultSize(i), setOpenSizeMenu(false)
                          )}>
                          <CustomText text={i} style={TextStyles.bold10Black} />
                        </MenuItem>
                        <MenuDivider />
                      </>
                    );
                  })}
                </Menu>
              </View>
            </>
          )}
        </View>
        <CustomButton
          containerStyle={SpaceStyles.spaceVertical}
          text={'ADD TO CART'}
          onPress={() => addToCart()}
        />
      </View>
      <ScrollableTabView
        initialPage={0}
        renderTabBar={() => <TopTabComponent />}>
        <ProductDescription
          tabLabel={DESCRIPTION}
          productDetail={productDetail}
        />
        <ProductReview tabLabel={REVIEWS} />
        <ProductSizeChart tabLabel={SIZE_CHART} />
        <ProductShippingDelivery tabLabel={SHIPPING_DELIVERY} />
      </ScrollableTabView>
    </View>
  );
}

export default ProductDetailScreen;
