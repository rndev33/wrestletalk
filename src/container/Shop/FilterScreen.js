import React, { useState, useEffect } from "react";
import { View, TouchableOpacity, TextInput, Image, Platform } from 'react-native'
import CustomButton from "../../components/CustomButton";
import CustomText from "../../components/CustomText";
import HeaderComponent from "../../components/HeaderComponent";
import { BLACK, DARK_BLACK, LIGHT_GRAY, PRIMARY_COLOR, WHITE } from "../../constants/Colors";
import { searchGrayIcon } from "../../constants/Images";
import NavigationService from "../../navigation/NavigationService";
import CommonStyles from "../../style/CommonStyles";
import SpaceStyles from "../../style/SpaceStyles";
import TextStyles from "../../style/TextStyles";
import { useSelector, useDispatch } from "react-redux";
import { getAllTshirtData } from "../../redux/action/TshirtAction";
import { getAllHoodieData } from "../../redux/action/HoodieAction";
import { getAllMugsData } from "../../redux/action/MugsAction";
import { getAllMagazineData } from "../../redux/action/MagazineAction";
import { SafeAreaView } from "react-native";

function FilterScreen(props) {
    const { navigation, route } = props
    const { activeIndex } = route.params
    const [data, setData] = useState([{ name: 'Price', check: false }, { name: 'Last added', check: false }])
    const dispatch = useDispatch()

    const tshirtFilterData = useSelector(state => state.tshirt.tshirtFilterData);
    const hoodieFilterData = useSelector(state => state.hoodie.hoodieFilterData);
    const mugsFilterData = useSelector(state => state.mugs.mugsFilterData);
    const magazineFilterData = useSelector(state => state.magazine.magazineFilterData);

    const [lowestPrice, setLowestPrice] = useState('')
    const [highestPrice, setHighestPrice] = useState('')
    const [searchName, setSearchName] = useState('')
    const [lastAdded, setLastAdded] = useState(false)

    useEffect(() => {
        if (activeIndex == 0) {
            setSearchName(tshirtFilterData?.search)
            setLowestPrice(tshirtFilterData?.lowestPrice)
            setHighestPrice(tshirtFilterData?.highestPrice)
            setLastAdded(tshirtFilterData?.lastAdded)

            if (tshirtFilterData.lastAdded) {
                let newData = Object.assign([], data);
                newData[1].check = true
                setData(Object.assign([], newData))
            }
            if (tshirtFilterData?.lowestPrice != '' && tshirtFilterData?.highestPrice != '') {
                let newData = Object.assign([], data);
                newData[0].check = true
                setData(Object.assign([], newData))
            }
        }
        if (activeIndex == 1) {
            setSearchName(hoodieFilterData?.search)
            setLowestPrice(hoodieFilterData?.lowestPrice)
            setHighestPrice(hoodieFilterData?.highestPrice)
            setLastAdded(hoodieFilterData?.lastAdded)

            if (hoodieFilterData.lastAdded) {
                let newData = Object.assign([], data);
                newData[1].check = true
                setData(Object.assign([], newData))
            }
            if (hoodieFilterData?.lowestPrice != '' && hoodieFilterData?.highestPrice != '') {
                let newData = Object.assign([], data);
                newData[0].check = true
                setData(Object.assign([], newData))
            }
        }
        if (activeIndex == 2) {
            setSearchName(mugsFilterData?.search)
            setLowestPrice(mugsFilterData?.lowestPrice)
            setHighestPrice(mugsFilterData?.highestPrice)
            setLastAdded(mugsFilterData?.lastAdded)

            if (mugsFilterData.lastAdded) {
                let newData = Object.assign([], data);
                newData[1].check = true
                setData(Object.assign([], newData))
            }
            if (mugsFilterData?.lowestPrice != '' && mugsFilterData?.highestPrice != '') {
                let newData = Object.assign([], data);
                newData[0].check = true
                setData(Object.assign([], newData))
            }
        }
        if (activeIndex == 3) {
            setSearchName(magazineFilterData?.search)
            setLowestPrice(magazineFilterData?.lowestPrice)
            setHighestPrice(magazineFilterData?.highestPrice)
            setLastAdded(magazineFilterData?.lastAdded)

            if (magazineFilterData.lastAdded) {
                let newData = Object.assign([], data);
                newData[1].check = true
                setData(Object.assign([], newData))
            }
            if (magazineFilterData?.lowestPrice != '' && magazineFilterData?.highestPrice != '') {
                let newData = Object.assign([], data);
                newData[0].check = true
                setData(Object.assign([], newData))
            }
        }
    }, [])

    const onApplyFilter = () => {
        if (activeIndex == 0) {
            dispatch(getAllTshirtData(lowestPrice, highestPrice, lastAdded, searchName))
        }
        if (activeIndex == 1) {
            dispatch(getAllHoodieData(lowestPrice, highestPrice, lastAdded, searchName))
        }
        if (activeIndex == 2) {
            dispatch(getAllMugsData(lowestPrice, highestPrice, lastAdded, searchName))
        }
        if (activeIndex == 3) {
            dispatch(getAllMagazineData(lowestPrice, highestPrice, lastAdded, searchName))
        }
        navigation.goBack()
    }

    return (
        <View style={CommonStyles.mainContainer}>
            <SafeAreaView style={{ backgroundColor: PRIMARY_COLOR }} />
            <HeaderComponent />
            <View style={[SpaceStyles.top2, SpaceStyles.spaceHorizontal]}>
                <CustomText
                    text={'Filter'}
                    style={[TextStyles.bold25TitleBlack]}
                />
                <CustomText
                    text={'Search'}
                    style={[TextStyles.bold15TitleBlack, SpaceStyles.top3]}
                />

                <View style={[CommonStyles.inputView, SpaceStyles.top1]}>
                    <TextInput
                        placeholder="Keyword"
                        defaultValue={searchName}
                        placeholderTextColor={LIGHT_GRAY}
                        style={[TextStyles.medium12LightGray, SpaceStyles.width70, { flex: 1 }]}
                        onChangeText={(name) => setSearchName(name)}
                        textAlignVertical={'center'}
                    />
                    <Image source={searchGrayIcon} />
                </View>

                <CustomText
                    text={'Sort by'}
                    style={[TextStyles.bold15TitleBlack, SpaceStyles.top3]}
                />

                <View style={[SpaceStyles.sortRoeWrap, SpaceStyles.top2]}>
                    {data?.map((i, index) => {
                        const backgroundColor = i.check ? BLACK : WHITE
                        const color = i.check ? WHITE : DARK_BLACK
                        return (
                            <TouchableOpacity
                                key={index}
                                onPress={() => {
                                    let newData = Object.assign([], data);
                                    newData?.map((i, count) => {
                                        if (count == index) {
                                            i.check = !i.check
                                        }
                                    })
                                    if (newData[1]?.check) {
                                        setLastAdded(true)
                                    }
                                    else {
                                        setLastAdded(false)
                                    }
                                    if (!newData[0]?.check) {
                                        setLowestPrice('')
                                        setHighestPrice('')
                                    }
                                    setData(Object.assign([], newData))
                                }}
                                style={[CommonStyles.sortView, { backgroundColor: backgroundColor }]}>
                                <CustomText
                                    text={i.name}
                                    style={[TextStyles.bold12DarkBlack, { color: color }]}
                                />
                            </TouchableOpacity>
                        )
                    })}
                </View>
                <CustomText
                    text={'Price Range'}
                    style={[TextStyles.bold15TitleBlack, SpaceStyles.top5]}
                />

                <View style={[SpaceStyles.alignSpaceBlock, SpaceStyles.top2]}>
                    <View style={[CommonStyles.priceInput]}>
                        <CustomText
                            text={'£'}
                            style={[TextStyles.bold12LightBlack]}
                        />
                        <TextInput
                            placeholder="Lowest"
                            defaultValue={lowestPrice}
                            placeholderTextColor={'#7C7C80'}
                            maxLength={5}
                            style={[TextStyles.medium12LightBlack, SpaceStyles.width40, SpaceStyles.left3]}
                            onChangeText={(name) => setLowestPrice(name)}
                            textAlignVertical={'center'}
                        />
                    </View>
                    <View style={[CommonStyles.priceInput]}>
                        <CustomText
                            text={'£'}
                            style={[TextStyles.bold12LightBlack]}
                        />
                        <TextInput
                            placeholder="Highest"
                            defaultValue={highestPrice}
                            placeholderTextColor={'#7C7C80'}
                            maxLength={5}
                            style={[TextStyles.medium12LightBlack, SpaceStyles.width40, SpaceStyles.left3]}
                            onChangeText={(name) => setHighestPrice(name)}
                            textAlignVertical={'center'}
                        />
                    </View>
                </View>

                <CustomButton
                    containerStyle={SpaceStyles.top5}
                    text={'APPLY FILTER'}
                    onPress={() => onApplyFilter()}
                />
                <CustomButton
                    text={'CONTINUE SHOPPING'}
                    textStyle={TextStyles.medium12LightGray}
                    containerStyle={{ backgroundColor: WHITE }}
                    onPress={() => navigation.goBack()}
                />
            </View>
        </View>
    )
}

export default FilterScreen