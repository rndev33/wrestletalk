import React, {useEffect} from 'react';
import {View, TouchableOpacity, Image, ScrollView} from 'react-native';
import CustomButton from '../../components/CustomButton';
import CustomText from '../../components/CustomText';
import HeaderComponent from '../../components/HeaderComponent';
import {INPUT_COLOR, PRIMARY_COLOR, WHITE} from '../../constants/Colors';
import {minusRedIcon, plusRedIcon} from '../../constants/Images';
import NavigationService from '../../navigation/NavigationService';
import {updateCartApiCall} from '../../redux/services/shopServices';
import CommonStyles from '../../style/CommonStyles';
import SpaceStyles from '../../style/SpaceStyles';
import TextStyles from '../../style/TextStyles';
import {useDispatch, useSelector} from 'react-redux';
import {getAllCartData} from '../../redux/action/CartAction';
import {SafeAreaView} from 'react-native';

function CartScreen(props) {
  const {navigation} = props;
  const dispatch = useDispatch();
  const cartData = useSelector(state => state.cart.cartData);

  useEffect(() => {
    getCart();
  }, []);

  const updateCart = (id, quantity) => {
    const data = {
      updates: {
        id: quantity,
      },
    };
    data.updates[id] = data.updates['id'];
    delete data.updates['id'];
    updateCartApiCall(data)
      .then(res => {
        getCart();
      })
      .catch(err => {});
  };

  const getCart = () => {
    dispatch(getAllCartData());
  };

  const minusFun = item => {
    updateCart(item.id, item?.quantity - 1);
  };

  const plusFun = item => {
    updateCart(item.id, item?.quantity + 1);
  };

  const ItemComponent = (item, index) => {
    return (
      <View key={index} style={SpaceStyles.spaceHorizontal}>
        <View style={[SpaceStyles.rowJustify, SpaceStyles.vertical3]}>
          <View style={SpaceStyles.row}>
            <Image
              resizeMode={'contain'}
              source={{uri: item?.image}}
              style={CommonStyles.cartImageStyle}
            />
            <View style={SpaceStyles.left3}>
              <CustomText
                text={item?.product_title}
                style={SpaceStyles.width40}
              />
              <View style={CommonStyles.labelView}>
                <CustomText
                  text={item?.variant_options?.join('/')}
                  style={TextStyles.bold8White}
                />
              </View>
              <View style={CommonStyles.quantityView}>
                <TouchableOpacity
                  onPress={() => minusFun(item)}
                  style={CommonStyles.quantitySubView}>
                  <Image source={minusRedIcon} resizeMode={'contain'} />
                </TouchableOpacity>

                <CustomText
                  text={item?.quantity}
                  style={TextStyles.bold8DarkBlack}
                />
                <TouchableOpacity
                  onPress={() => plusFun(item)}
                  style={CommonStyles.quantitySubView}>
                  <Image source={plusRedIcon} resizeMode={'contain'} />
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <CustomText
            text={`£${item?.price / 100}`}
            style={TextStyles.bold12LightBlack}
          />
        </View>
        <View style={[CommonStyles.lineView, {borderColor: INPUT_COLOR}]} />
      </View>
    );
  };

  const checkout = () => {
    let array = [];
    cartData?.items?.map(i => {
      array.push({
        variantId: i.variant_id,
        quantity: i.quantity,
      });
    });

    var result = array.reduce(function (r, e) {
      r[e.variantId] = e.quantity;
      return r;
    }, {});

    NavigationService.navigate('WebViewCheckoutScreen', {
      link: JSON.stringify(result).replace(/[{}""]/g, ''),
    });
  };

  return (
    <View style={CommonStyles.mainContainer}>
      <SafeAreaView style={{backgroundColor: PRIMARY_COLOR}} />
      <HeaderComponent />

      <CustomText
        text={'Cart'}
        style={[
          TextStyles.bold25TitleBlack,
          SpaceStyles.spaceHorizontal,
          SpaceStyles.spaceVertical,
        ]}
      />
      <ScrollView
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps={'handled'}>
        {cartData?.items?.map((i, index) => {
          return ItemComponent(i, index);
        })}
        <View style={SpaceStyles.spaceHorizontal}>
          <View style={[SpaceStyles.alignSpaceBlock, SpaceStyles.top2]}>
            <CustomText text={'Total'} style={TextStyles.medium12LightBlack} />
            <CustomText
              text={`£${cartData?.total_price / 100}`}
              style={TextStyles.bold12LightBlack}
            />
          </View>
          <CustomButton
            containerStyle={SpaceStyles.top5}
            text={'CHECKOUT'}
            onPress={() => checkout()}
          />
          <CustomButton
            text={'CONTINUE SHOPPING'}
            textStyle={TextStyles.medium12LightGray}
            containerStyle={[{backgroundColor: WHITE}, SpaceStyles.bottom1]}
            onPress={() => navigation.goBack()}
          />
        </View>
      </ScrollView>
    </View>
  );
}

export default CartScreen;
