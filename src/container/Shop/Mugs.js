import React from "react";
import { View, FlatList, TouchableOpacity } from 'react-native'
import CustomText from "../../components/CustomText";
import CommonStyles from "../../style/CommonStyles";
import SpaceStyles from "../../style/SpaceStyles";
import TextStyles from "../../style/TextStyles";
import FastImage from 'react-native-fast-image'
import NavigationService from "../../navigation/NavigationService";

function Mugs({ mugsData, addToCart = () => { } }) {

    const renderMugs = ({ item, index }) => {
        return (
            <TouchableOpacity
                activeOpacity={0.6}
                onPress={() => NavigationService.navigate('ProductDetailScreen', {
                    productDetail: item
                })}
                key={index}
                style={CommonStyles.shopProductView}>
                <FastImage
                    style={CommonStyles.prodctImageStyle}
                    source={{
                        uri: item?.image?.src,
                        priority: FastImage.priority.normal,
                    }}
                    resizeMode={FastImage.resizeMode.contain}
                />
                <View style={SpaceStyles.spaceHorizontal}>
                    <View style={[SpaceStyles.height28, SpaceStyles.top1]}>
                        <CustomText
                            text={item?.title}
                            numberOfLines={2}
                            style={[TextStyles.medium9LightBorder, SpaceStyles.width35, { textAlign: 'center' }]}
                        />
                    </View>
                    <CustomText
                        text={`£${item?.variants[0]?.price}`}
                        style={[TextStyles.bold12DarkBlack, { textAlign: 'center', marginTop: 3 }]}
                    />
                </View>
                <TouchableOpacity
                    onPress={() => addToCart(item?.variants[0]?.id)}
                    style={CommonStyles.addToCartButton}>
                    <CustomText
                        text={'ADD TO CART'}
                        style={[TextStyles.bold8White, { textAlign: 'center' }]}
                    />
                </TouchableOpacity>
            </TouchableOpacity>
        )
    }

    return (
        <>
            <FlatList
                data={mugsData}
                numColumns={2}
                showsVerticalScrollIndicator={false}
                renderItem={renderMugs}
                keyboardShouldPersistTaps="handled"
                ListEmptyComponent={() => {
                    return (
                        <View style={CommonStyles.emptyView}>
                            <CustomText
                                style={TextStyles.bold14LightBorder}
                                text={`No Data Found`}
                            />
                        </View>
                    )
                }}
                contentContainerStyle={[SpaceStyles.spaceHorizontal, SpaceStyles.paddingBottom2]}
            />
        </>
    )
}

export default Mugs