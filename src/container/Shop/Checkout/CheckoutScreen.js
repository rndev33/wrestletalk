import React, {useState} from 'react';
import {View} from 'react-native';
import HeaderComponent from '../../../components/HeaderComponent';
import TopTabComponent from '../../../components/TopTabComponent';
import {
  CONTACT,
  PAYMENT,
  REVIEW,
  SHIPPING,
} from '../../../constants/GlobalFunction';
import CommonStyles from '../../../style/CommonStyles';
import Contact from './Contact';
import Payment from './Payment';
import Review from './Review';
import Shipping from './Shipping';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import {SafeAreaView} from 'react-native';
import {PRIMARY_COLOR} from '../../../constants/Colors';

function CheckoutScreen(props) {
  const {navigation} = props;

  return (
    <View style={CommonStyles.mainContainer}>
      \
      <SafeAreaView style={{backgroundColor: PRIMARY_COLOR}} />
      <HeaderComponent />
      <ScrollableTabView
        initialPage={0}
        renderTabBar={() => <TopTabComponent />}>
        <Contact tabLabel={CONTACT} />
        <Shipping tabLabel={SHIPPING} />
        <Payment tabLabel={PAYMENT} />
        <Review tabLabel={REVIEW} />
      </ScrollableTabView>
    </View>
  );
}

export default CheckoutScreen;
