import React from "react";
import { View, TouchableOpacity, ScrollView } from 'react-native'
import CustomButton from "../../../components/CustomButton";
import CustomText from "../../../components/CustomText";
import CustomTextInput from "../../../components/CustomTextInput";
import { WHITE } from "../../../constants/Colors";
import SpaceStyles from "../../../style/SpaceStyles";
import TextStyles from "../../../style/TextStyles";

function Contact({ }) {

    return (
        <ScrollView
            showsVerticalScrollIndicator={false}
            keyboardShouldPersistTaps='handled'
        >
            <View style={SpaceStyles.spaceHorizontal}>
                <CustomText
                    text={'Contact Information'}
                    style={[TextStyles.bold16LightBlack, SpaceStyles.top3]}
                />
                <View style={[SpaceStyles.rowFlex, SpaceStyles.top1]}>
                    <CustomText
                        text={'Already have an account? '}
                        style={TextStyles.medium10LightBorder}
                    />
                    <TouchableOpacity>
                        <CustomText
                            text={'Log in'}
                            style={TextStyles.medium10DarkRed}
                        />
                    </TouchableOpacity>
                </View>
                <View style={[SpaceStyles.alignSpaceBlock, SpaceStyles.top2]}>
                    <View>
                        <CustomText
                            text={'First Name'}
                            style={TextStyles.medium10LightBorder}
                        />
                        <CustomTextInput
                            placeholder={'First Name'}
                            containerStyle={SpaceStyles.width43}
                        />
                    </View>
                    <View>
                        <CustomText
                            text={'Last Name'}
                            style={TextStyles.medium10LightBorder}
                        />
                        <CustomTextInput
                            placeholder={'Last Name'}
                            containerStyle={SpaceStyles.width43}
                        />
                    </View>
                </View>
                <CustomText
                    text={'Email'}
                    style={[TextStyles.medium10LightBorder, SpaceStyles.top2]}
                />
                <CustomTextInput
                    placeholder={'Email'}
                />
                <CustomText
                    text={'Shipping Address'}
                    style={[TextStyles.bold16LightBlack, SpaceStyles.top3]}
                />
                <CustomText
                    text={'Country/Region'}
                    style={[TextStyles.medium10LightBorder, SpaceStyles.top2]}
                />
                <CustomTextInput
                    placeholder={'Country/Region'}
                />
                <CustomText
                    text={'Address'}
                    style={[TextStyles.medium10LightBorder, SpaceStyles.top2]}
                />
                <CustomTextInput
                    placeholder={'Address'}
                />
                <CustomText
                    text={'Address Details'}
                    style={[TextStyles.medium10LightBorder, SpaceStyles.top2]}
                />
                <CustomTextInput
                    placeholder={'Address Details'}
                />
                <CustomText
                    text={'City'}
                    style={[TextStyles.medium10LightBorder, SpaceStyles.top2]}
                />
                <CustomTextInput
                    placeholder={'City'}
                />
                <View style={[SpaceStyles.alignSpaceBlock, SpaceStyles.top2]}>
                    <View>
                        <CustomText
                            text={'Province'}
                            style={TextStyles.medium10LightBorder}
                        />
                        <CustomTextInput
                            placeholder={'Province'}
                            containerStyle={SpaceStyles.width43}
                        />
                    </View>
                    <View>
                        <CustomText
                            text={'Postal Code'}
                            style={TextStyles.medium10LightBorder}
                        />
                        <CustomTextInput
                            placeholder={'Postal Code'}
                            containerStyle={SpaceStyles.width43}
                        />
                    </View>
                </View>
                <CustomButton
                    containerStyle={SpaceStyles.top3}
                    text={'CONTINUE TO SHIPPING'}
                />
                <CustomButton
                    text={'RETURN TO CART'}
                    textStyle={TextStyles.medium12LightGray}
                    containerStyle={[{ backgroundColor: WHITE }, SpaceStyles.bottom1]}
                />

            </View>
        </ScrollView>
    )
}

export default Contact