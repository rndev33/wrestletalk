import React from "react";
import { View, TouchableOpacity, ScrollView } from 'react-native'
import CustomButton from "../../../components/CustomButton";
import CustomText from "../../../components/CustomText";
import CustomTextInput from "../../../components/CustomTextInput";
import { WHITE } from "../../../constants/Colors";
import SpaceStyles from "../../../style/SpaceStyles";
import TextStyles from "../../../style/TextStyles";

function Shipping({ }) {

    return (
        <ScrollView
            showsVerticalScrollIndicator={false}
            keyboardShouldPersistTaps='handled'
        >
            <View style={SpaceStyles.spaceHorizontal}>
                <CustomText
                    text={'Billing Details'}
                    style={[TextStyles.bold16LightBlack, SpaceStyles.top3]}
                />
                <CustomText
                    text={'Confirm your details'}
                    style={[TextStyles.medium10LightBorder, SpaceStyles.top1]}
                />
                <CustomText
                    text={'Contact'}
                    style={[TextStyles.medium10LightBorder, SpaceStyles.top2]}
                />
                <CustomTextInput
                    placeholder={'Contact'}
                />
                <CustomText
                    text={'Ship to'}
                    style={[TextStyles.medium10LightBorder, SpaceStyles.top2]}
                />
                <CustomTextInput
                    placeholder={'Address'}
                />
                <CustomText
                    text={'Shipping Method'}
                    style={[TextStyles.bold16LightBlack, SpaceStyles.top3]}
                />
                <CustomButton
                    containerStyle={SpaceStyles.top3}
                    text={'CONTINUE TO PAYMENT'}
                />
                <CustomButton
                    text={'RETURN TO INFORMATION'}
                    textStyle={TextStyles.medium12LightGray}
                    containerStyle={[{ backgroundColor: WHITE }, SpaceStyles.bottom1]}
                />
            </View>
        </ScrollView>
    )
}

export default Shipping