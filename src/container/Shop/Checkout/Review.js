import React from "react";
import { View, ScrollView, Image } from 'react-native'
import CustomButton from "../../../components/CustomButton";
import CustomText from "../../../components/CustomText";
import CustomTextInput from "../../../components/CustomTextInput";
import { WHITE } from "../../../constants/Colors";
import { selectRadio, unSelectRadio } from "../../../constants/Images";
import CommonStyles from "../../../style/CommonStyles";
import SpaceStyles from "../../../style/SpaceStyles";
import TextStyles from "../../../style/TextStyles";

function Review({ }) {

    return (
        <ScrollView
            showsVerticalScrollIndicator={false}
            keyboardShouldPersistTaps='handled'
        >
            <View style={SpaceStyles.spaceHorizontal}>
                <CustomText
                    text={'Review Order'}
                    style={[TextStyles.bold16LightBlack, SpaceStyles.top3]}
                />
                <View style={[CommonStyles.reviewBorderView, SpaceStyles.spaceVertical]}>
                    <View style={[SpaceStyles.rowFlex, SpaceStyles.spaceVertical]}>
                        <View style={SpaceStyles.width20}>
                            <CustomText
                                text={'Contact'}
                                style={TextStyles.medium12LightBorder}
                            />
                        </View>
                        <CustomText
                            text={'craig.bellamy@gmail.com'}
                            style={TextStyles.medium12Review}
                        />
                    </View>
                    <View style={[CommonStyles.lineView, { borderColor: '#DCDCDC' }]} />
                    <View style={[SpaceStyles.row, SpaceStyles.spaceVertical]}>
                        <View style={SpaceStyles.width20}>
                            <CustomText
                                text={'Ship to'}
                                style={TextStyles.medium12LightBorder}
                            />
                        </View>
                        <View>
                            <CustomText
                                text={'Craig Bellamy'}
                                style={TextStyles.medium12Review}
                            />
                            <CustomText
                                text={`100 Holmesdale St Orchid Ln, 2nd floor, 23C Cardiff CF11 7AG United Kingdom`}
                                style={[TextStyles.medium12LightBorder, SpaceStyles.top1, SpaceStyles.width62]}
                            />
                        </View>
                    </View>
                    <View style={[CommonStyles.lineView, { borderColor: '#DCDCDC' }]} />
                    <View style={[SpaceStyles.rowFlex, SpaceStyles.spaceVertical]}>
                        <View style={SpaceStyles.width20}>
                            <CustomText
                                text={'Method'}
                                style={TextStyles.medium12LightBorder}
                            />
                        </View>
                        <View style={SpaceStyles.rowFlex}>
                            <CustomText
                                text={'WT Mag Shipping '}
                                style={TextStyles.medium12Review}
                            />
                            <CustomText
                                text={'£4.99'}
                                style={TextStyles.bold12Black}
                            />
                        </View>
                    </View>
                    <View style={[CommonStyles.lineView, { borderColor: '#DCDCDC' }]} />
                    <View style={[SpaceStyles.rowFlex, SpaceStyles.spaceVertical]}>
                        <View style={SpaceStyles.width20}>
                            <CustomText
                                text={'Payment'}
                                style={TextStyles.medium12LightBorder}
                            />
                        </View>
                        <View style={SpaceStyles.rowFlex}>
                            <CustomText
                                text={'ending with 9299'}
                                style={TextStyles.medium12Review}
                            />
                        </View>
                    </View>
                </View>

                <View style={[CommonStyles.reviewBorderView, SpaceStyles.bottom1]}>
                    <View style={SpaceStyles.spaceVertical}>
                        <View style={SpaceStyles.alignSpaceBlock}>
                            <CustomText
                                text={'Subtotal'}
                                style={TextStyles.medium12LightBorder}
                            />
                            <CustomText
                                text={'£50.00'}
                                style={TextStyles.medium12Review}
                            />
                        </View>
                        <View style={[SpaceStyles.alignSpaceBlock, SpaceStyles.top1]}>
                            <CustomText
                                text={'Shipping'}
                                style={TextStyles.medium12LightBorder}
                            />
                            <CustomText
                                text={'£4.99'}
                                style={TextStyles.medium12Review}
                            />
                        </View>
                    </View>
                    <View style={[CommonStyles.lineView, { borderColor: '#DCDCDC' }]} />

                    <View style={[SpaceStyles.alignSpaceBlock, SpaceStyles.spaceVertical]}>
                        <CustomText
                            text={'Total'}
                            style={TextStyles.medium14LightBorder}
                        />
                        <View style={SpaceStyles.rowFlex}>
                            <CustomText
                                text={'GBP'}
                                style={TextStyles.medium8LightBorder}
                            />
                            <CustomText
                                text={'£54.99'}
                                style={[TextStyles.medium14Review, SpaceStyles.left2]}
                            />
                        </View>
                    </View>
                </View>


                <CustomButton
                    containerStyle={SpaceStyles.top1}
                    text={'PAY NOW'}
                />
                <CustomButton
                    text={'RETURN TO PAYMENT'}
                    textStyle={TextStyles.medium12LightGray}
                    containerStyle={[{ backgroundColor: WHITE }, SpaceStyles.bottom1]}
                />
            </View>
        </ScrollView>
    )
}

export default Review