import React from "react";
import { View, ScrollView, Image } from 'react-native'
import CustomButton from "../../../components/CustomButton";
import CustomText from "../../../components/CustomText";
import CustomTextInput from "../../../components/CustomTextInput";
import { WHITE } from "../../../constants/Colors";
import { selectRadio, unSelectRadio } from "../../../constants/Images";
import CommonStyles from "../../../style/CommonStyles";
import SpaceStyles from "../../../style/SpaceStyles";
import TextStyles from "../../../style/TextStyles";

function Payment({ }) {

    return (
        <ScrollView
            showsVerticalScrollIndicator={false}
            keyboardShouldPersistTaps='handled'
        >
            <View style={SpaceStyles.spaceHorizontal}>
                <CustomText
                    text={'Payment'}
                    style={[TextStyles.bold16LightBlack, SpaceStyles.top3]}
                />
                <CustomText
                    text={'All transactions are secure and encrypted'}
                    style={[TextStyles.medium10LightBorder, SpaceStyles.top1]}
                />
                <View style={CommonStyles.wholePaymentBlock}>
                    <View style={CommonStyles.paymentMethodView}>
                        <Image
                            resizeMode={'contain'}
                            source={selectRadio}
                        />
                        <CustomText
                            text={'Credit Card'}
                            style={[TextStyles.medium10Paymnent, SpaceStyles.left2]}
                        />
                    </View>
                    <View style={SpaceStyles.horizontal3}>
                        <CustomText
                            text={'Card Number'}
                            style={[TextStyles.medium10LightBorder, SpaceStyles.top2]}
                        />
                        <CustomTextInput
                            placeholder={'Card Number'}
                        />
                        <CustomText
                            text={'Name on Card'}
                            style={[TextStyles.medium10LightBorder, SpaceStyles.top2]}
                        />
                        <CustomTextInput
                            placeholder={'Name'}
                        />
                        <View style={[SpaceStyles.alignSpaceBlock, SpaceStyles.spaceVertical]}>
                            <View>
                                <CustomText
                                    text={'Expiration Date'}
                                    style={TextStyles.medium10LightBorder}
                                />
                                <CustomTextInput
                                    placeholder={'Date'}
                                    containerStyle={SpaceStyles.width40}
                                />
                            </View>
                            <View>
                                <CustomText
                                    text={'CVV'}
                                    style={TextStyles.medium10LightBorder}
                                />
                                <CustomTextInput
                                    placeholder={'CVV'}
                                    containerStyle={SpaceStyles.width40}
                                />
                            </View>
                        </View>
                    </View>
                    <View style={[CommonStyles.paymentMethodView, CommonStyles.borderSelectedView]}>
                        <Image
                            resizeMode={'contain'}
                            source={unSelectRadio}
                        />
                        <CustomText
                            text={'PayPal'}
                            style={[TextStyles.medium10Paymnent, SpaceStyles.left2]}
                        />
                    </View>
                </View>
                <CustomButton
                    containerStyle={SpaceStyles.top1}
                    text={'REVIEW ORDER'}
                />
                <CustomButton
                    text={'RETURN TO SHIPPING'}
                    textStyle={TextStyles.medium12LightGray}
                    containerStyle={[{ backgroundColor: WHITE }, SpaceStyles.bottom1]}
                />
            </View>
        </ScrollView>
    )
}

export default Payment