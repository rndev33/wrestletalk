import React from "react";
import { SafeAreaView } from "react-native";
import { View } from "react-native";
import { WebView } from "react-native-webview";
import HeaderComponent from "../../../components/HeaderComponent";
import { PRIMARY_COLOR } from "../../../constants/Colors";
import CommonStyles from "../../../style/CommonStyles";

const WebViewCheckoutScreen = (props) => {
    const { route, navigation } = props
    const { link } = route.params

    return (
        <View style={CommonStyles.mainContainer}>
            <SafeAreaView style={{ backgroundColor: PRIMARY_COLOR }} />
            <HeaderComponent />
            <WebView
                source={{ uri: `http://wrestle-talk-uk.myshopify.com/cart/` + link }}
                showsVerticalScrollIndicator={false}
                startInLoadingState
                androidHardwareAccelerationDisabled={true}
            />
        </View>
    );
};

export default WebViewCheckoutScreen;
