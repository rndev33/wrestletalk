import React, {useEffect, useRef, useState} from 'react';
import {View, TouchableOpacity, Platform} from 'react-native';
import CustomText from '../../components/CustomText';
import HeaderComponent from '../../components/HeaderComponent';
import TopTabComponent from '../../components/TopTabComponent';
import {PRIMARY_COLOR, ULTRA_GRAY} from '../../constants/Colors';
import {HOODIES, MAGAZINE, MUGS, TSHIRTS} from '../../constants/GlobalFunction';
import NavigationService from '../../navigation/NavigationService';
import {addCartApiCall} from '../../redux/services/shopServices';
import CommonStyles from '../../style/CommonStyles';
import TextStyles from '../../style/TextStyles';
import Hoodies from './Hoodies';
import Magazine from './Magazine';
import Mugs from './Mugs';
import Tshirt from './Tshirt';
import {useSelector, useDispatch} from 'react-redux';
import LoaderModal from '../../components/LoaderModal';
import {getAllCartData} from '../../redux/action/CartAction';
import {getAllTshirtData} from '../../redux/action/TshirtAction';
import {getAllHoodieData} from '../../redux/action/HoodieAction';
import {getAllMugsData} from '../../redux/action/MugsAction';
import {getAllMagazineData} from '../../redux/action/MagazineAction';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import {SafeAreaView} from 'react-native';

const adUnitId =
  Platform.OS === 'ios'
    ? 'ca-app-pub-7858948897424048/7024961746'
    : 'ca-app-pub-7858948897424048/2828650148';

function ShopScreen(props) {
  const {navigation} = props;
  const dispatch = useDispatch();
  const cartData = useSelector(state => state.cart.cartData);
  const tshirtData = useSelector(state => state.tshirt.tshirtData);
  const hoodieData = useSelector(state => state.hoodie.hoodieData);
  const mugsData = useSelector(state => state.mugs.mugsData);
  const magazineData = useSelector(state => state.magazine.magazineData);

  const [activeIndex, setActiveIndex] = useState(0);
  const [loading, setLoading] = useState(false);
  const showAds = useSelector(state => state.ads.AdsData);

  useEffect(() => {
    dispatch(getAllTshirtData());
    dispatch(getAllHoodieData());
    dispatch(getAllMugsData());
    dispatch(getAllMagazineData());
    dispatch(getAllCartData());
  }, []);

  const addToCart = id => {
    const data = {
      items: [
        {
          id: id,
          quantity: 1,
        },
      ],
    };
    setLoading(true);
    addCartApiCall(data)
      .then(res => {
        dispatch(getAllCartData());
        setLoading(false);
      })
      .catch(err => {
        setLoading(false);
      });
  };

  return (
    <View style={[CommonStyles.mainContainer, {backgroundColor: ULTRA_GRAY}]}>
      <SafeAreaView style={{backgroundColor: PRIMARY_COLOR}} />
      <HeaderComponent />

      {loading && <LoaderModal loading={loading} />}
      <ScrollableTabView
        initialPage={0}
        renderTabBar={() => <TopTabComponent />}
        onChangeTab={({i, from}) => setActiveIndex(i)}>
        <Tshirt
          tabLabel={TSHIRTS}
          tshirtData={tshirtData}
          addToCart={id => addToCart(id)}
        />
        <Hoodies
          tabLabel={HOODIES}
          hoodieData={hoodieData}
          addToCart={id => addToCart(id)}
        />
        <Mugs
          tabLabel={MUGS}
          mugsData={mugsData}
          addToCart={id => addToCart(id)}
        />
        <Magazine
          tabLabel={MAGAZINE}
          magazineData={magazineData}
          addToCart={id => addToCart(id)}
        />
      </ScrollableTabView>
      <View style={CommonStyles.filterView}>
        <TouchableOpacity
          onPress={() =>
            NavigationService.navigate('FilterScreen', {
              activeIndex: activeIndex,
            })
          }
          style={CommonStyles.filterButton}>
          <CustomText text={'FILTER'} style={[TextStyles.medium10FullGray]} />
        </TouchableOpacity>
        <View style={CommonStyles.bottomVerticalLine} />
        <TouchableOpacity
          style={CommonStyles.filterButton}
          disabled={cartData?.items?.length == 0}
          onPress={() => NavigationService.navigate('CartScreen')}>
          <CustomText text={'CART'} style={[TextStyles.medium10FullGray]} />
          {cartData?.items?.length > 0 && (
            <View style={CommonStyles.redCountView}>
              <CustomText
                text={cartData?.items?.length}
                style={TextStyles.medium8White}
              />
            </View>
          )}
        </TouchableOpacity>
      </View>
    </View>
  );
}

export default ShopScreen;
