import React from "react";
import { Image, View, FlatList, TouchableOpacity } from 'react-native'
import CustomText from "../../../components/CustomText";
import SpaceStyles from "../../../style/SpaceStyles";
import TextStyles from "../../../style/TextStyles";

function ProductShippingDelivery({ }) {

    return (
        <View style={[SpaceStyles.spaceHorizontal, SpaceStyles.top2]}>
            <CustomText
                style={TextStyles.medium14Review}
                text={`Please allow up to 2 weeks for your order to arrive. Deliveries outside of the UK could take longer, especially during difficult postage periods such as national lockdowns, Christmas etc.`}
            />
        </View>
    )
}

export default ProductShippingDelivery