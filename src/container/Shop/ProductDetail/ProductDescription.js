import React from "react";
import { ScrollView, Platform, useWindowDimensions } from 'react-native'
import SpaceStyles from "../../../style/SpaceStyles";
import HTML from "react-native-render-html";
import { MAIN_BLACK } from "../../../constants/Colors";

function ProductDescription({ productDetail }) {
    const isIos = Platform.OS === 'ios'
    const {width}=useWindowDimensions()


    const tagsStyles = {
        body: {
            fontFamily: isIos ? 'Montserrat-Medium' : 'GothamMedium',
            fontSize: 14,
            color: MAIN_BLACK,
            letterSpacing: 0.5,
            lineHeight: isIos ? 19 : 17,
            fontWeight :'900'
        },
        a: {
            color: '#EF0054',
        }
    };


    return (
        <ScrollView
            keyboardShouldPersistTaps={'handled'}
            showsVerticalScrollIndicator={false}
            style={[SpaceStyles.spaceHorizontal, SpaceStyles.top2]}>
            {productDetail?.body_html?.includes('Material') ?
                <HTML
                contentWidth={width}
                    source={{ html: '<p><b>Material:</b>' + productDetail?.body_html?.split('Material:')[1] }}
                    baseStyle={tagsStyles.body}
                    tagsStyles={tagsStyles}
                />
                :
                <HTML
                contentWidth={width}
                    source={{ html: productDetail?.body_html }}
                    baseStyle={tagsStyles.body}
                    tagsStyles={tagsStyles}
                />
            }
        </ScrollView>
    )
}

export default ProductDescription