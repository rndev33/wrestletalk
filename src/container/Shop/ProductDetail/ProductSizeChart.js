import React from "react";
import { Image, View, FlatList, TouchableOpacity } from 'react-native'
import CommonStyles from "../../../style/CommonStyles";
import SpaceStyles from "../../../style/SpaceStyles";

function ProductSizeChart({ }) {

    return (
        <View style={SpaceStyles.spaceHorizontal}>
            <Image
                style={CommonStyles.sizeChartStyle}
                resizeMode={'contain'}
                source={{ uri: `https://cdn.shopify.com/s/files/1/0258/2636/1421/files/Sizing_Guide_November_2020.png?v=1606308831` }} />
        </View>
    )
}

export default ProductSizeChart