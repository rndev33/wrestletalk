import React from "react";
import { Image, View, FlatList, TouchableOpacity } from 'react-native'
import CustomText from "../../../components/CustomText";
import SpaceStyles from "../../../style/SpaceStyles";
import TextStyles from "../../../style/TextStyles";

function ProductReview({ }) {

    return (
        <View style={[SpaceStyles.spaceHorizontal, SpaceStyles.top2]}>
            <CustomText
                style={TextStyles.medium14Review}
                text={`No reviews yet`}
            />
        </View>
    )
}

export default ProductReview