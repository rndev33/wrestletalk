import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import RouteHome from './MainRoute/RouteHome';
import SplashScreen from '../container/Auth/SplashScreen';

const Stack = createStackNavigator();
function StackNaviagtion(props) {

  return (
    <Stack.Navigator
      initialRouteName={'SplashScreen'}
      screenOptions={{
        headerShown: false,
        gestureEnabled: false,
        animationEnabled: false
      }}
    >
      <Stack.Screen name="SplashScreen" component={SplashScreen} />
      <Stack.Screen
        options={{
          animationEnabled: false
        }}
        name="RouteHomeStack" component={RouteHome} />
    </Stack.Navigator>
  );
}

export default StackNaviagtion;
