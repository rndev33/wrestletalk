import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import ShopScreen from '../../container/Shop/ShopScreen';
import FilterScreen from '../../container/Shop/FilterScreen';
import CartScreen from '../../container/Shop/CartScreen';
import CheckoutScreen from '../../container/Shop/Checkout/CheckoutScreen';
import OfferScreen from '../../container/Offer/OfferScreen';
import OfferDetailScreen from '../../container/Offer/OfferDetailScreen';

const Stack = createStackNavigator();

function OfferStack(props) {
    return (
        <Stack.Navigator>
            <Stack.Screen
                options={{
                    headerShown: false
                }}
                name="OfferScreen" component={OfferScreen} />
            <Stack.Screen
                options={{
                    headerShown: false
                }}
                name="OfferDetailScreen" component={OfferDetailScreen} />
        </Stack.Navigator>
    );
}

export default OfferStack;
