import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import ShopScreen from '../../container/Shop/ShopScreen';
import FilterScreen from '../../container/Shop/FilterScreen';
import CartScreen from '../../container/Shop/CartScreen';
import CheckoutScreen from '../../container/Shop/Checkout/CheckoutScreen';
import WebViewCheckoutScreen from '../../container/Shop/Checkout/WebViewCheckoutScreen';

const Stack = createStackNavigator();

function ShopStackHome(props) {
    return (
        <Stack.Navigator>
            <Stack.Screen
                options={{
                    headerShown: false
                }}
                name="ShopScreen" component={ShopScreen} />
            <Stack.Screen
                options={{
                    headerShown: false
                }}
                name="FilterScreen" component={FilterScreen} />
            <Stack.Screen
                options={{
                    headerShown: false
                }}
                name="CartScreen" component={CartScreen} />
            <Stack.Screen
                options={{
                    headerShown: false
                }}
                name="CheckoutScreen" component={CheckoutScreen} />
            <Stack.Screen
                options={{
                    headerShown: false
                }}
                name="WebViewCheckoutScreen" component={WebViewCheckoutScreen} />
        </Stack.Navigator>
    );
}

export default ShopStackHome;
