import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { PRIMARY_COLOR, WHITE } from '../../constants/Colors';
import LeagueScreen from '../../container/League/LeagueScreen';
import WebViewScreen from '../../container/League/WebViewScreen';

const Stack = createStackNavigator();
function LeagueStackHome(props) {
    return (
        <Stack.Navigator>
            <Stack.Screen
                options={{
                    headerShown: false
                }}
                name="LeagueScreen" component={LeagueScreen} />
            <Stack.Screen
                options={{
                    headerShown: false
                }}
                name="WebViewScreen" component={WebViewScreen} />
        </Stack.Navigator>
    );
}

export default LeagueStackHome;
