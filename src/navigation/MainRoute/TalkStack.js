import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { PRIMARY_COLOR, WHITE } from '../../constants/Colors';
import TalkScreen from '../../container/Talk/TalkScreen';
import SearchScreen from '../../container/Talk/SearchScreen';
import LoginScreen from '../../container/Auth/LoginScreen';
import RegisterScreen from '../../container/Auth/RegisterScreen';
import ForgotPasswordScreen from '../../container/Auth/ForgotPasswordScreen';
import SettingScreen from '../../container/Auth/SettingScreen';
import FavoriteScreen from '../../container/Auth/FavoriteScreen';
import SetPasswordScreen from '../../container/Auth/SetPasswordScreen';

const Stack = createStackNavigator();
function TalkStackHome(props) {
    return (
        <Stack.Navigator>
            <Stack.Screen
                options={{
                    headerShown: false,
                    animationEnabled: false
                }}
                name="TalkScreen" >
                {() => {
                    return (
                        <TalkScreen
                            {...props}
                        />
                    )
                }}
            </Stack.Screen>
            <Stack.Screen
                options={{
                    headerShown: false,
                }}
                name="SearchScreen" component={SearchScreen} />
            <Stack.Screen
                options={{
                    headerShown: false,
                }}
                name="LoginScreen" component={LoginScreen} />
            <Stack.Screen
                options={{
                    headerShown: false
                }}
                name="RegisterScreen" component={RegisterScreen} />
            <Stack.Screen
                options={{
                    headerShown: false
                }}
                name="ForgotPasswordScreen" component={ForgotPasswordScreen} />
            <Stack.Screen
                options={{
                    headerShown: false
                }}
                name="SetPasswordScreen" component={SetPasswordScreen} />
            <Stack.Screen
                options={{
                    headerShown: false
                }}
                name="SettingScreen" component={SettingScreen} />
            <Stack.Screen
                options={{
                    headerShown: false
                }}
                name="FavoriteScreen" component={FavoriteScreen} />
        </Stack.Navigator>
    );
}

export default TalkStackHome;
