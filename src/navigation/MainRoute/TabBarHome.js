import React from 'react'
import { StyleSheet, View, TouchableOpacity, Image, SafeAreaView } from 'react-native'
import CustomText from '../../components/CustomText';
import { GRAY, PRIMARY_COLOR, WHITE } from '../../constants/Colors';
import { leagueTab, leagueTabSelected, offerTab, offerTabSelected, shopTab, shopTabSelected, wrestleTalkTab, wrestleTalkTabSelected } from '../../constants/Images';
import constants from '../../constants';
import NavigationService from '../NavigationService';

const tabBarConfig = [{
    icon: wrestleTalkTab,
    selectedIcon: wrestleTalkTabSelected,
    name: "TalkStack",
    text: 'WRESTLETALK'
}, {
    icon: shopTab,
    selectedIcon: shopTabSelected,
    name: "ShopStack",
    text: 'WRESTLESHOP'
}, {
    icon: leagueTab,
    selectedIcon: leagueTabSelected,
    name: "LeagueStack",
    text: 'WRESTLELEAGUE'
}, {
    icon: offerTab,
    selectedIcon: offerTabSelected,
    name: "OfferStack",
    text: 'OFFERS'
}]

function TabBarHome(props) {
    const { descriptors, navigation, state } = props
    const focusedOptions = descriptors[state.routes[state.index].key].options;
    const disable = NavigationService?.getActiveRoute()?.name == "LoginScreen" || NavigationService?.getActiveRoute()?.name == "RegisterScreen" || NavigationService?.getActiveRoute()?.name == "ForgotPasswordScreen" || NavigationService?.getActiveRoute()?.name == "SettingScreen"

    if (focusedOptions.tabBarVisible === false) {
        return null;
    }

    return (
        <View>
            <View style={styles.container}>
                {
                    tabBarConfig.map((route, index) => {
                        const isFocused = state.index === index;

                        const onPress = () => {
                            const event = navigation.emit({
                                type: 'tabPress',
                                target: route.key,
                                canPreventDefault: true,
                            });

                            if (!isFocused && !event.defaultPrevented) {
                                navigation.navigate(route.name);
                            }
                        };
                        return (
                            <TouchableOpacity
                                key={String(index)}
                                activeOpacity={0.9}
                                onPress={onPress}
                                style={isFocused ? styles.selectedTab : styles.unSelectedTab}
                            >
                                <Image
                                    source={disable ? route.icon : isFocused ? route.selectedIcon : route.icon}
                                    resizeMode='contain'
                                />
                                <CustomText
                                    text={route.text}
                                    style={disable ? styles.unSelectedText : isFocused ? styles.selectedText : styles.unSelectedText}
                                />
                            </TouchableOpacity>
                        )
                    })
                }
            </View>
            <SafeAreaView style={{ backgroundColor: PRIMARY_COLOR }} />
        </View>
    )
}

export default TabBarHome

const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        backgroundColor: PRIMARY_COLOR,
        paddingHorizontal: (constants.BaseStyle.DEVICE_WIDTH / 100) * 5,
        paddingVertical: 15,
    },
    selectedTab: {
        paddingHorizontal: (constants.BaseStyle.DEVICE_WIDTH / 100) * 5,
        flexDirection: 'column',
        alignItems: 'center'
    },
    unSelectedTab: {
        paddingHorizontal: (constants.BaseStyle.DEVICE_WIDTH / 100) * 5,
        flexDirection: 'column',
        alignItems: 'center'
    },
    selectedText: {
        color: WHITE,
        ...constants.Fonts.bold6,
        marginTop: 10
    },
    unSelectedText: {
        color: GRAY,
        ...constants.Fonts.bold6,
        marginTop: 10
    }
})
