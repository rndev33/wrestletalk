import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import TabBarHome from './TabBarHome';
import TalkStackHome from './TalkStack';
import LeagueStackHome from './LeagueStack';
import ArticleScreen from '../../container/Talk/ArticleScreen';
import ShopStackHome from './ShopStack';
import VideoDetailScreen from '../../container/Talk/VideoDetailScreen';
import ProductDetailScreen from '../../container/Shop/ProductDetailScreen';
import OfferStack from './OfferStack';
import CommentScreen from '../../container/Talk/CommentScreen';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

function RouteHome(props) {
    return (
        <Stack.Navigator>
            <Stack.Screen
                options={{
                    headerShown: false,
                    gestureEnabled: false,
                    animationEnabled: false
                }}
                name="BottomTab">
                {() => {
                    return (
                        <BootmTab  {...props} />
                    )
                }}
            </Stack.Screen>
            <Stack.Screen
                options={{
                    headerShown: false,
                    // animationEnabled: false
                }}
                name="ArticleScreen" component={ArticleScreen} />
            <Stack.Screen
                options={{
                    headerShown: false,
                }}
                name="VideoDetailScreen" component={VideoDetailScreen} />
            <Stack.Screen
                options={{
                    headerShown: false,
                }}
                name="ProductDetailScreen" component={ProductDetailScreen} />
            <Stack.Screen
                options={{
                    headerShown: false,
                }}
                name="CommentScreen" component={CommentScreen} />
        </Stack.Navigator>
    );
}

function BootmTab(props) {
    return (
        <Tab.Navigator tabBar={props => <TabBarHome {...props} />}>
            <Tab.Screen
                options={{
                    headerShown: false,
                    animationEnabled: false
                }}
                name="TalkStack"  >
                {() => {
                    return (
                        <TalkStackHome
                            {...props}
                        />
                    )
                }}
            </Tab.Screen>
            <Tab.Screen
                options={{
                    headerShown: false
                }}
                name="ShopStack" component={ShopStackHome} />
            <Tab.Screen
                options={{
                    headerShown: false
                }}
                name="LeagueStack" component={LeagueStackHome} />
            <Tab.Screen
                options={{
                    headerShown: false
                }}
                name="OfferStack" component={OfferStack} />
        </Tab.Navigator>
    );
}
export default RouteHome;
