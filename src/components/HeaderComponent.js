import React, { useEffect, useState } from "react";
import { View, Image, TouchableOpacity, TextInput, Alert, Platform } from "react-native";
import { arrowLeft, cancelIcon, mainLogo, redBackIcon, searchIcon, verticalMenu } from "../constants/Images";
import NavigationService from "../navigation/NavigationService";
import CommonStyles from "../style/CommonStyles";
import SpaceStyles from "../style/SpaceStyles";
import { Menu, MenuItem, MenuDivider } from 'react-native-material-menu';
import { BLACK, GRAY_TEXT, WHITE } from "../constants/Colors";
import CustomText from "./CustomText";
import TextStyles from "../style/TextStyles";
import { Variables } from "../constants/Variables";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { getData, removeData } from "../constants/GlobalFunction";
import { deleteAccountApiCall } from "../redux/services/AuthServices";

const HeaderComponent = ({ arrow = false, onArrow = () => { } }) => {
    const isIos = Platform.OS === 'ios'
    const [openMenu, setOpenMenu] = useState(false)
    const [serachBar, setSearchBar] = useState(false)
    const [searchName, setSearchName] = useState('')
    const [token, setToken] = useState('')

    useEffect(() => {
        getToken()
    }, [])

    const getToken = async () => {
        getData(Variables.accessToken).then((res) => {
            setToken(JSON.parse(res))
        }).catch(() => {
        })
    }

    const checkSearch = () => {
        NavigationService.navigate('SearchScreen', {
            searchText: searchName
        })
        setSearchBar(false)
    }

    const logoutCall = () => {
        Alert.alert(
            'Logout',
            'Are you sure? You want to logout?',
            [
                {
                    text: 'Cancel',
                    onPress: () => {
                        return null;
                    },
                },
                {
                    text: 'Confirm',
                    onPress: async () => {
                        AsyncStorage.clear();
                        NavigationService.reset('RouteHomeStack')
                        await removeData(Variables.accessToken)
                        await removeData(Variables.token)
                    },
                },
            ],
            { cancelable: false },
        );
    }

    return (
        <View style={CommonStyles.headerView}>
            {serachBar ?
                <View style={CommonStyles.searchInputView}>
                    <TextInput
                        style={[isIos ? TextStyles.medium12MainBlack : TextStyles.medium10Black, { flex: 1 }]}
                        placeholder="Search"
                        placeholderTextColor={GRAY_TEXT}
                        onChangeText={(name) => setSearchName(name)}
                        onEndEditing={() => checkSearch()}
                        textAlignVertical={'center'}
                    />
                </View>
                :
                <View style={SpaceStyles.rowFlex}>
                    {arrow &&
                        <TouchableOpacity
                            onPress={() => onArrow()}
                        >
                            <Image
                                resizeMode={'contain'}
                                source={redBackIcon}
                                style={{ marginRight: 15 }}
                            />
                        </TouchableOpacity>

                    }
                    <TouchableOpacity
                        onPress={() => NavigationService.reset('RouteHomeStack')}
                    >
                        <Image
                            resizeMode={'contain'}
                            source={mainLogo}
                        />
                    </TouchableOpacity>
                </View>
            }
            <View style={SpaceStyles.rowFlex}>
                {!serachBar ?
                    <TouchableOpacity
                        onPress={() => setSearchBar(true)}
                        style={CommonStyles.iconStyle}
                    >
                        <Image
                            resizeMode={'contain'}
                            source={searchIcon}
                        />
                    </TouchableOpacity>
                    :
                    <TouchableOpacity
                        onPress={() => setSearchBar(false)}
                        style={CommonStyles.iconStyle}>
                        <Image
                            source={cancelIcon}
                            resizeMode={'contain'}
                        />
                    </TouchableOpacity>
                }
                <TouchableOpacity
                    onPress={() => setOpenMenu(true)}
                    style={CommonStyles.iconStyle}>
                    <Image
                        resizeMode={'contain'}
                        source={verticalMenu}
                    />
                </TouchableOpacity>
                <View style={CommonStyles.menuPositionStyle}>
                    <Menu
                        anchor={() => {
                            <Image
                                resizeMode={'contain'}
                                source={downArrow}
                                style={{ tintColor: BLACK }}
                            />
                        }}
                        visible={openMenu}
                        onRequestClose={() => setOpenMenu(false)}
                    >
                        <MenuItem
                            onPress={() => { setOpenMenu(false), NavigationService.navigate('SettingScreen') }}>
                            <CustomText
                                text={`Settings`}
                                style={TextStyles.bold10Black}
                            />
                        </MenuItem>
                        <MenuDivider />
                        {token ?
                            <MenuItem
                                onPress={() => { setOpenMenu(false), logoutCall() }}>
                                <CustomText
                                    text={`Logout`}
                                    style={TextStyles.bold10Black}
                                />
                            </MenuItem>
                            :
                            <>
                                <MenuItem
                                    onPress={() => { setOpenMenu(false), NavigationService.navigate('RegisterScreen') }}>
                                    <CustomText
                                        text={`Register`}
                                        style={TextStyles.bold10Black}
                                    />
                                </MenuItem>
                                <MenuDivider />
                                <MenuItem
                                    onPress={() => { setOpenMenu(false), NavigationService.navigate('LoginScreen') }}>
                                    <CustomText
                                        text={`Login`}
                                        style={TextStyles.bold10Black}
                                    />
                                </MenuItem>
                            </>
                        }
                        <MenuDivider />
                    </Menu>
                </View>
            </View>

        </View>
    );
};

export default HeaderComponent;
