import React from 'react'
import { View, TextInput } from 'react-native'
import { LIGHT_BORDER } from '../constants/Colors'
import CommonStyles from '../style/CommonStyles'
import TextStyles from '../style/TextStyles'

const CustomTextInput = ({
    containerStyle,
    style,
    placeholder,
    onChangeText,
    keyboardType,
    maxLength,
    autoCapitalize,
    value,
    defaultValue,
    placeholderTextColor,
    editable,
    numberOfLines,
    textAlignVertical,
    secureTextEntry
}) => {
    return (
        <View style={[CommonStyles.textInputView, containerStyle]}>
            <TextInput
                style={[TextStyles.medium10LightBorder, style, { flex: 1 }]}
                placeholder={placeholder}
                onChangeText={onChangeText}
                placeholderTextColor={placeholderTextColor ? placeholderTextColor : LIGHT_BORDER}
                keyboardType={keyboardType}
                maxLength={maxLength}
                importantForAutofill={'no'}
                autoCapitalize={autoCapitalize ? autoCapitalize : 'none'}
                autoComplete={'off'}
                autoCorrect={false}
                value={value}
                defaultValue={defaultValue}
                editable={editable}
                numberOfLines={numberOfLines ? numberOfLines : 1}
                textAlignVertical={textAlignVertical ? textAlignVertical : 'center'}
                secureTextEntry={secureTextEntry ? secureTextEntry : false}
            />
        </View>
    )
}
export default CustomTextInput
