import React, { useEffect, useRef, useState } from 'react';
import { AppState, Platform } from 'react-native';
import { AppOpenAd, TestIds, AdEventType, useAppOpenAd } from 'react-native-google-mobile-ads';
import { useSelector } from 'react-redux';



export const OpenAppAdService = () => {
    const appState = useRef(AppState.currentState);
    const adUnitId = Platform.OS === 'ios' ? 'ca-app-pub-7858948897424048/5837956863' : 'ca-app-pub-7858948897424048/6576323467'
    const showAds = useSelector(state => state.ads.AdsData);
    const [addShown, setAddShown] = useState(false)


    const { isLoaded, show, load, error, isClosed } = useAppOpenAd(adUnitId, {
        requestNonPersonalizedAdsOnly: true,
    })

    React.useEffect(() => {
        if (isClosed) {
          load();
        }
      }, [isClosed, load]);


    useEffect(() => {
        const subscription = AppState.addEventListener('change', nextAppState => {
            if (
                appState.current.match(/inactive|background/) &&
                nextAppState === 'active'
            ) {

                if (showAds) {
                    if (addShown) {
                        setAddShown(false)
                    } else {
                        load()
                        setTimeout(() => {
                            show()
                        }, 2000)
                        setAddShown(true)
                    }
                }

            }

            appState.current = nextAppState;
        });

        return () => subscription.remove();
    }, [addShown]);

    return <></>
}