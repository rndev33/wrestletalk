import {useEffect, useRef} from 'react';

const useInterval = (callback, delay, clear) => {
  const savedCallback = useRef();

  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  useEffect(() => {
    let id = setInterval(() => {
      savedCallback.current();
    }, delay);
    if (clear) clearInterval(id);
    return () => clearInterval(id);
  }, [delay, clear]);
};

export default useInterval;
