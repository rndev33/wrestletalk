import React, { useEffect, useRef, useState } from "react";
import { ActivityIndicator } from "react-native";
import { Linking } from "react-native";
import AutoHeightWebView from 'react-native-autoheight-webview'
import constants from "../constants";

const mathJaxOptions = {
  messageStyle: "none",
  showMathMenu: false,
};

const MathjaxLoad = (props) => {
  const webview = useRef(null);
  const WIDTH = (constants.BaseStyle.DEVICE_WIDTH / 100)
  const HEIGHT = (constants.BaseStyle.DEVICE_HEIGHT / 100)

  useEffect(() => {
    return () => {
      webview.current = null;
    };
  }, []);

  const wrapMathjax = (content) => {
    const options = JSON.stringify(Object.assign({}, mathJaxOptions));

    return `
			<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
			<script type="text/x-mathjax-config">
				MathJax.Hub.Config(${options});

				MathJax.Hub.Queue(function() {
					var height = document.documentElement.scrollHeight;
					window.ReactNativeWebView.postMessage(String(height));
					document.getElementById("formula").style.visibility = '';
				});
			</script>
       <style>
        body {font-family: 'SF Compact Display', sans-serif; font-size: 20px; font-weight: 300; line-height: 150 %; min-width: 320px; position: relative; background: #ffffff; margin: auto; color: #141414;}
    .related-story h5 {margin - bottom: 0.25em;font - size: 1rem;  color: #c00000;}
    .related-story {padding-bottom: 0.5em !important; border-top: 1px solid #4F5955 !important;  border-bottom: 1px solid #4F5955 !important; width:100%}
    a { color: #CC0808; text-decoration:none}
    .post-preview__media { width: 100px; }
    .post-preview__body {padding-bottom: 20px;padding-top: 20px;  }
    .post-preview__body a { margin-bottom: 0.4rem; font-size: 1em;font-weight: 600;line-height: 1.25;vertical-align: middle; color: #444;}
    p img { max-width: 100% !important; width: 100% !important; height: auto !important;}
    .wp-caption-text{font-style:italic;width: ${WIDTH * 90} !important;font-size: 14px}
      </style>
      <script async defer src="https://platform.instagram.com/en_US/embeds.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-MML-AM_CHTML"></script>
				<div id="formula" style="font-size:17;  visibility: hidden; user-select: none; -khtml-user-select: none; -moz-user-select: none; -ms-user-select: none;-webkit-user-select: none;">
				${content}
			</div>
		`;
  };

  const html = wrapMathjax(props.html);
  return (
    <>
      <AutoHeightWebView
        androidLayerType={'hardware'}
        ref={webview}
        style={{ width: WIDTH * 90, marginTop: 10, marginBottom: -HEIGHT * 5 }}
        files={[{
          href: 'cssfileaddress',
          type: 'text/css',
          rel: 'stylesheet'
        }]}
      //   customStyle={`
      //   * {
      //     font-family: 'Times New Roman';
      //   }
      //   p {
      //     font-size: 16px;
      //   }
      // `}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        source={{ html: html }}
        scalesPageToFit={true}
        javaScriptEnabled={true}
        domStorageEnabled={true}
        scrollEnabled={false}
        startInLoadingState={true}
        renderLoading={() => <ActivityIndicator color={"black"} size="small" />}
        androidHardwareAccelerationDisabled={true}
        cacheMode="LOAD_DEFAULT"
        viewportContent={'width=device-width, user-scalable=no'}
        onNavigationStateChange={(event) => {
          if (webview.current && event.url && event.url.includes("http")) {
            webview.current.stopLoading();
            Linking.canOpenURL(event.url)
              .then((supported) => {
                Linking.openURL(event.url);
              })
              .catch((err) => {
              });
          }
        }}
      />
    </>

  );
};

export default MathjaxLoad;
