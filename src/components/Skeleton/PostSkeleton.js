import React from "react";
import { View } from "react-native";
import SkeletonPlaceholder from "react-native-skeleton-placeholder";
import { ULTRA_GRAY, ULTRA_LIGHT_GRAY } from "../../constants/Colors";
import CommonStyles from "../../style/CommonStyles";
import SpaceStyles from "../../style/SpaceStyles";

const PostSkeleton = ({ offer = false }) => {
    const data = [{}, {}, {}, {}]

    const renderPostList = (i, index) => {
        return (
            <View
                key={index}
                style={[CommonStyles.videosBannerStyle, SpaceStyles.spaceHorizontal, SpaceStyles.vertical1, { borderRadius: offer ? 20 : 8 }]}
            />
        )
    }
    return (
        <SkeletonPlaceholder
            backgroundColor={ULTRA_LIGHT_GRAY}
            highlightColor={ULTRA_GRAY}
            speed={1000}
        >
            {data.map((i, index) => {
                return renderPostList(i, index)
            })}

        </SkeletonPlaceholder>
    );
};

export default PostSkeleton;