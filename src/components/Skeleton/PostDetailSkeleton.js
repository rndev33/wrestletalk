import React from "react";
import { View } from "react-native";
import SkeletonPlaceholder from "react-native-skeleton-placeholder";
import { ULTRA_GRAY, ULTRA_LIGHT_GRAY } from "../../constants/Colors";
import CommonStyles from "../../style/CommonStyles";
import SpaceStyles from "../../style/SpaceStyles";

const PostDetailSkeleton = () => {

    const data = [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}]
    return (
        <SkeletonPlaceholder
            backgroundColor={ULTRA_LIGHT_GRAY}
            highlightColor={ULTRA_GRAY}
            speed={1000}
        >
            <View style={[SpaceStyles.spaceHorizontal, SpaceStyles.top2]}>
                <View style={CommonStyles.videosBannerStyle} />
                {data?.map((i, index) => {
                    return (
                        <View style={CommonStyles.textSkeleton} />
                    )
                })}
                <View style={[CommonStyles.videosBannerStyle, SpaceStyles.top1]} />
            </View>

        </SkeletonPlaceholder>
    );
};

export default PostDetailSkeleton;