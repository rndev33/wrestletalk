import React from "react";
import { View, FlatList } from "react-native";
import SkeletonPlaceholder from "react-native-skeleton-placeholder";
import { ULTRA_GRAY, ULTRA_LIGHT_GRAY } from "../../constants/Colors";
import CommonStyles from "../../style/CommonStyles";
import SpaceStyles from "../../style/SpaceStyles";

const SearchSkeleton = () => {
    const data = [{}, {}, {}, {}, {}, {}, {}]

    const renderPostList = (i, index) => {
        return (
            <View
                key={index}
                style={[CommonStyles.searchItemSkeletonView, SpaceStyles.vertical1]}>

            </View>
        )
    }
    return (
        <SkeletonPlaceholder
            backgroundColor={ULTRA_LIGHT_GRAY}
            highlightColor={ULTRA_GRAY}
            speed={1000}
        >
            <View style={[SpaceStyles.paddingTop2, SpaceStyles.sortRoeWrap, SpaceStyles.left5]}>
                {data.map((i, index) => {
                    return renderPostList(i, index)
                })}
            </View>

        </SkeletonPlaceholder>
    );
};

export default SearchSkeleton;