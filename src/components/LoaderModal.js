import React from 'react';
import { StyleSheet, View, Modal, ActivityIndicator } from 'react-native';
import { PRIMARY_COLOR, WHITE } from '../constants/Colors';

const LoaderModal = ({ loading }) => {
    return (
        <Modal
            transparent={true}
            animationType={'none'}
            visible={loading}
            onRequestClose={() => {
            }}>
            <View style={styles.modalBackground}>
                <View style={styles.activityIndicatorWrapper}>
                    <ActivityIndicator
                        animating={true}
                        color={PRIMARY_COLOR}
                        size="large"
                    />
                </View>
            </View>
        </Modal>
    );
};
export default LoaderModal;


const styles = StyleSheet.create({
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#00000040',
        justifyContent: 'center',
    },
    activityIndicatorWrapper: {
        alignItems: 'center',
        backgroundColor: WHITE,
        justifyContent: 'center',
        padding: 5,
        borderRadius: 90
    }
});
