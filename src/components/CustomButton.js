import React from "react";
import { StyleSheet, TouchableOpacity } from "react-native";
import constants from "../constants";
import { BLACK, WHITE } from "../constants/Colors";
import CustomText from "./CustomText";

const CustomButton = ({ containerStyle, textStyle, text, onPress }) => {
    return (
        <TouchableOpacity
            style={[styles.buttonContainer, containerStyle]}
            onPress={onPress}
            activeOpacity={0.6}
        >
            <CustomText
                text={text}
                style={[styles.buttonText, textStyle]}
            />
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    buttonContainer: {
        width: (constants.BaseStyle.DEVICE_WIDTH / 100) * 90,
        backgroundColor: BLACK,
        height: 45,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonText: {
        color: WHITE,
        ...constants.Fonts.bold12
    },
});

export default CustomButton;
