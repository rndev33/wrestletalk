import React from "react";
import { View, ActivityIndicator, StyleSheet } from "react-native";
import { PRIMARY_COLOR } from "../constants/Colors";

const BottomLoader = (props) => {
    return (
        <View style={styles.bottomLoaderContainer}>
            <ActivityIndicator color={PRIMARY_COLOR} size="large" />
        </View>
    );
};

const styles = StyleSheet.create({
    bottomLoaderContainer: {
        width: '100%',
        alignItems: "center",
        justifyContent: "center",
        paddingVertical: 20,
    },
});

export default BottomLoader;