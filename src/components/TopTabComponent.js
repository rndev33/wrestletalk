import React from "react";
import { View, TouchableOpacity } from "react-native";
import { DARK_BORDER_COLOR, DARK_GRAY, GRAY_TEXT } from "../constants/Colors";
import CommonStyles from "../style/CommonStyles";
import TextStyles from "../style/TextStyles";
import CustomText from "./CustomText";

const TopTabComponent = props => {
    return (
        <View style={[CommonStyles.topTabView, props?.isLeagueScreen && {flexWrap:'wrap'}]}>
            {props.tabs.map((tab, i) => {
                const borderColor = props.activeTab === i ? DARK_BORDER_COLOR : DARK_GRAY
                const textColor = props.activeTab === i ? DARK_BORDER_COLOR : GRAY_TEXT
                return (
                    <TouchableOpacity
                        key={i}
                        onPress={() => {
                            props.goToPage(i);
                        }}
                        style={[CommonStyles.innerTopBlock, { borderBottomColor: borderColor }, props?.isLeagueScreen && {maxWidth:90,}]}>
                        <CustomText
                            text={tab}
                            style={[TextStyles.bold10GrayText, { color: textColor },props?.isLeagueScreen && {textAlign:'center',lineHeight:13}]}
                        />
                    </TouchableOpacity>
                );
            })}
        </View>
    );
};

export default TopTabComponent;