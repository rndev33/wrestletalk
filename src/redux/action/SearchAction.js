import { GET_SEARCH_ACTION } from "../config/actionTypes";
import { getSearchApiCall } from "../services/talkServices";
import { RefreshLoading, SearchLoading, SearchScrollLoading } from "./LoadingAction";


export const getAllSearchData = (
    query,
    isRefresh = false,
    successSearch = () => { },
    failedSearch = () => { },
    isScrollEvent = false,
) => {
    return (dispatch, getState) => {
        if (isRefresh) {
            dispatch(RefreshLoading(true))
        }
        if (isScrollEvent) {
            dispatch(SearchScrollLoading(true))
        } else {
            console.log('hello');
            dispatch(SearchLoading(true))
        }
        getSearchApiCall(query)
            .then((res) => {
                if (isScrollEvent) {
                    let tmpArray = getState().search.searchData;
                    dispatch(setSearchData(tmpArray.concat(res.data)));
                } else {
                    dispatch(setSearchData(res.data))
                }
                if (res.data?.length > 0) {
                    successSearch(res.headers['x-wp-totalpages'], res.headers['x-wp-total']);
                } else {
                    failedSearch();
                }
            })
            .catch((err) => {
                dispatch(SearchScrollLoading(false))
                dispatch(RefreshLoading(false))
                dispatch(SearchLoading(false))
                dispatch(setSearchData([]))
            }).finally(()=>{
                dispatch(SearchScrollLoading(false))
                dispatch(RefreshLoading(false))
                dispatch(SearchLoading(false))
            });
    };
};

export const setSearchData = (payload) => {
    return { type: GET_SEARCH_ACTION, payload: payload };
};

