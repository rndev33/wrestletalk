import { getDifferenceInHours, HOODIE_IDS } from "../../constants/GlobalFunction";
import { GET_HOODIE_ACTION, GET_HOODIE_FILTER_ACTION } from "../config/actionTypes";

export const getAllHoodieData = (lowestPrice = "", highestPrice = "", lastAdded = false, search = "") => {
    const productData = require("../../assets/data/productData.json")
    return (dispatch) => {
        let IdArray = HOODIE_IDS
        let finalArray = []
        productData?.products?.map((i) => {
            IdArray?.map((itmId) => {
                if (i.id == itmId) {
                    if (lowestPrice != "" && highestPrice != "") {
                        if (parseFloat(i?.variants[0]?.price) <= parseFloat(highestPrice) && parseFloat(i?.variants[0]?.price) >= lowestPrice) {
                            finalArray.push(i)
                        }
                    }
                    else {
                        finalArray.push(i)
                    }
                }
            })
        })
        if (lastAdded) {
            finalArray?.sort((a, b) => (a.created_at > b.created_at) ? 1 : -1).reverse()
        }
        if (search != '') {
            let filteredData = finalArray?.filter(function (item) {
                return item?.title.toLowerCase().match(search?.toLowerCase());
            });
            dispatch(setHoodieData(filteredData))
        }
        else {
            dispatch(setHoodieData(finalArray))
        }
        const obj = {
            search: search,
            lastAdded: lastAdded,
            highestPrice: highestPrice,
            lowestPrice: lowestPrice
        }
        dispatch(setHoodieFilterData(obj))
    };
};

export const setHoodieData = (payload) => {
    return { type: GET_HOODIE_ACTION, payload: payload };
};

export const setHoodieFilterData = (payload) => {
    return { type: GET_HOODIE_FILTER_ACTION, payload: payload };
};