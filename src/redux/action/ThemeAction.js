import { GET_THEME_ACTION } from "../config/actionTypes";

export const setThemeData = (payload) => {
    return { type: GET_THEME_ACTION, payload: payload };
};