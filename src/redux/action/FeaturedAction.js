import { GET_FEATURED_ACTION, GET_FEATURED_COUNT_ACTION } from "../config/actionTypes";
import { getFeaturedApiCall } from "../services/talkServices";
import { FeaturedLoading, FeaturedScrollLoading, RefreshLoading } from "./LoadingAction";

export const getAllFeaturedData = (
    query,
    isRefresh = false,
    successFeatured = () => { },
    failedFeatured = () => { },
    isScrollEvent = false
) => {
    return (dispatch, getState) => {
        if (isRefresh) {
            dispatch(RefreshLoading(true))
        }
        if (isScrollEvent) {
            dispatch(FeaturedScrollLoading(true))
        } else {
            dispatch(FeaturedLoading(true))
        }
        getFeaturedApiCall(query)
            .then((res) => {
                dispatch(FeaturedScrollLoading(false))
                dispatch(RefreshLoading(false))
                dispatch(FeaturedLoading(false))
                let totalData = [res.data.mobile_app_main_post, ...res.data.sub_posts]
                if (isScrollEvent) {
                    let tmpArray = getState().featured.featuredData;
                    dispatch(setFeaturedData(tmpArray.concat(totalData)));
                } else {
                    dispatch(setFeaturedData(totalData))
                }
                if (totalData?.length > 0) {
                    successFeatured();
                    dispatch(setFeaturedCountData(res.headers['x-wp-totalpages']))
                } else {
                    failedFeatured();
                }
            })
            .catch(() => {
                dispatch(FeaturedScrollLoading(false))
                dispatch(RefreshLoading(false))
                dispatch(FeaturedLoading(false))
                dispatch(setFeaturedData([]))
            });
    };
};

export const setFeaturedData = (payload) => {
    return { type: GET_FEATURED_ACTION, payload: payload };
};

export const setFeaturedCountData = (payload) => {
    return { type: GET_FEATURED_COUNT_ACTION, payload: payload };
};