import { GET_MAGAZINE_ACTION, GET_MAGAZINE_FILTER_ACTION } from "../config/actionTypes";
import { getMagazineApiCall } from "../services/shopServices";

export const getAllMagazineData = (lowestPrice = "", highestPrice = "", lastAdded = false, search = "") => {
    return (dispatch) => {
        getMagazineApiCall()
            .then((res) => {
                let array = []
                res?.data?.products?.map((i) => {
                    if (i?.product_type == "Magazine" && i?.variants[0]?.price != 1) {
                        if (lowestPrice != "" && highestPrice != "") {
                            if (parseFloat(i?.variants[0]?.price) <= parseFloat(highestPrice) && parseFloat(i?.variants[0]?.price) >= lowestPrice) {
                                array.push(i)
                            }
                        }
                        else {
                            array.push(i)
                        }
                    }
                })
                if (lastAdded) {
                    array?.sort((a, b) => (a.created_at > b.created_at) ? 1 : -1).reverse()
                }
                if (search != '') {
                    let filteredData = array?.filter(function (item) {
                        return item?.title.toLowerCase().match(search?.toLowerCase());
                    });
                    dispatch(setMagazineData(filteredData))
                }
                else {
                    dispatch(setMagazineData(array))
                }
                const obj = {
                    search: search,
                    lastAdded: lastAdded,
                    highestPrice: highestPrice,
                    lowestPrice: lowestPrice
                }
                dispatch(setMagazineFilterData(obj))
            })
            .catch((err) => {
                dispatch(setMagazineData([]))
            })
    };
};

export const setMagazineData = (payload) => {
    return { type: GET_MAGAZINE_ACTION, payload: payload };
};

export const setMagazineFilterData = (payload) => {
    return { type: GET_MAGAZINE_FILTER_ACTION, payload: payload };
};