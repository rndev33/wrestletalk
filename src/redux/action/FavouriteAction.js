import { GET_FAVOURITE_ACTION } from "../config/actionTypes";
import { getFavouriteCall } from "../services/AuthServices";

export const getAllFavouriteData = () => {
    return (dispatch) => {
        getFavouriteCall()
            .then((res) => {
                res?.data?.popular?.map((i) => {
                    i.check = false
                })
                dispatch(setFavouriteData(res?.data?.popular))
            })
            .catch((err) => {
                dispatch(setFavouriteData([]))
            });
    };
};

export const setFavouriteData = (payload) => {
    return { type: GET_FAVOURITE_ACTION, payload: payload };
};

