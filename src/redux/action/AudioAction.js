import { GET_AUDIO_ACTION, GET_AUDIO_COUNT_ACTION } from "../config/actionTypes";
import { getAudioApiCall } from "../services/talkServices";
import { AudioLoading, AudioScrollLoading, RefreshLoading } from "./LoadingAction";

export const getAllAudioData = (
    query,
    isRefresh = false,
    successAudio = () => { },
    failedAudio = () => { },
    isScrollEvent = false
) => {
    return (dispatch, getState) => {
        if (isRefresh) {
            dispatch(RefreshLoading(true))
        }
        if (isScrollEvent) {
            dispatch(AudioScrollLoading(true))
        } else {
            dispatch(AudioLoading(true))
        }
        getAudioApiCall(query)
            .then((res) => {
                dispatch(AudioScrollLoading(false))
                dispatch(RefreshLoading(false))
                dispatch(AudioLoading(false))
                if (isScrollEvent) {
                    let tmpArray = getState().audio.audioData;
                    dispatch(setAudioData(tmpArray.concat(res.data)));
                } else {
                    dispatch(setAudioData(res.data))
                }
                if (res.data.length > 0) {
                    successAudio();
                    dispatch(setAudioCountData(res.headers['x-wp-totalpages']))
                } else {
                    failedAudio();
                }
            })
            .catch((err) => {
                dispatch(AudioScrollLoading(false))
                dispatch(RefreshLoading(false))
                dispatch(AudioLoading(false))
                dispatch(setAudioData([]))
            });
    };
};

export const setAudioData = (payload) => {
    return { type: GET_AUDIO_ACTION, payload: payload };
};

export const setAudioCountData = (payload) => {
    return { type: GET_AUDIO_COUNT_ACTION, payload: payload };
};

