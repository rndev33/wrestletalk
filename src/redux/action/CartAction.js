import { GET_CART_ACTION } from "../config/actionTypes";
import { getCartApiCall } from "../services/shopServices";

export const getAllCartData = () => {
    return (dispatch) => {
        getCartApiCall()
            .then((res) => {
                dispatch(setCartData(res.data))
            })
            .catch((err) => {
                dispatch(setCartData(''))
            });
    };
};

export const setCartData = (payload) => {
    return { type: GET_CART_ACTION, payload: payload };
};

