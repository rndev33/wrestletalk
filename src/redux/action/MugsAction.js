import { getDifferenceInHours, MUG_IDS } from "../../constants/GlobalFunction";
import { GET_MUGS_ACTION, GET_MUGS_FILTER_ACTION } from "../config/actionTypes";

export const getAllMugsData = (lowestPrice = "", highestPrice = "", lastAdded = false, search = "") => {
    const productData = require("../../assets/data/productData.json")
    return (dispatch) => {
        let IdArray = MUG_IDS
        let finalArray = []
        productData?.products?.map((i) => {
            IdArray?.map((itmId) => {
                if (i.id == itmId) {
                    if (lowestPrice != "" && highestPrice != "") {
                        if (parseFloat(i?.variants[0]?.price) <= parseFloat(highestPrice) && parseFloat(i?.variants[0]?.price) >= lowestPrice) {
                            finalArray.push(i)
                        }
                    }
                    else {
                        finalArray.push(i)
                    }
                }
            })
        })
        if (lastAdded) {
            finalArray?.sort((a, b) => (a.created_at > b.created_at) ? 1 : -1).reverse()
        }
        if (search != '') {
            let filteredData = finalArray?.filter(function (item) {
                return item?.title.toLowerCase().match(search?.toLowerCase());
            });
            dispatch(setMugsData(filteredData))
        }
        else {
            dispatch(setMugsData(finalArray))
        }
        const obj = {
            search: search,
            lastAdded: lastAdded,
            highestPrice: highestPrice,
            lowestPrice: lowestPrice
        }
        dispatch(setMugsFilterData(obj))
    };
};

export const setMugsData = (payload) => {
    return { type: GET_MUGS_ACTION, payload: payload };
};

export const setMugsFilterData = (payload) => {
    return { type: GET_MUGS_FILTER_ACTION, payload: payload };
};