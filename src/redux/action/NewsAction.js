import { GET_NEWS_ACTION, GET_NEWS_COUNT_ACTION } from "../config/actionTypes";
import { getNewsApiCall } from "../services/talkServices";
import { NewsLoading, NewsScrollLoading, RefreshLoading } from "./LoadingAction";

export const getAllNewsData = (
    query,
    isRefresh = false,
    successNews = () => { },
    failedNews = () => { },
    isScrollEvent = false
) => {
    return (dispatch, getState) => {
        if (isRefresh) {
            dispatch(RefreshLoading(true))
        }
        if (isScrollEvent) {
            dispatch(NewsScrollLoading(true))
        } else {
            dispatch(NewsLoading(true))
        }
        getNewsApiCall(query)
            .then((res) => {
                dispatch(NewsScrollLoading(false))
                dispatch(RefreshLoading(false))
                dispatch(NewsLoading(false))

                if (isScrollEvent) {
                    let tmpArray = getState().news.newsData;
                    dispatch(setNewsData(tmpArray.concat(res.data)));
                } else {
                    dispatch(setNewsData(res.data))
                }
                if (res.data.length > 0) {
                    successNews();
                    dispatch(setNewsCountData(res.headers['x-wp-totalpages']))
                } else {
                    failedNews();
                }
            })
            .catch((err) => {
                dispatch(NewsScrollLoading(false))
                dispatch(RefreshLoading(false))
                dispatch(NewsLoading(false))
                dispatch(setNewsData([]))
            });
    };
};

export const setNewsData = (payload) => {
    return { type: GET_NEWS_ACTION, payload: payload };
};

export const setNewsCountData = (payload) => {
    return { type: GET_NEWS_COUNT_ACTION, payload: payload };
};
