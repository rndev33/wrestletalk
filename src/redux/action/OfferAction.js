import { GET_OFFER_ACTION } from "../config/actionTypes";
import { getOfferApiCall } from "../services/offerServices";
import { OfferLoading, OfferScrollLoading, RefreshLoading } from "./LoadingAction";

export const getAllOfferData = (
    query,
    isRefresh = false,
    successOffer = () => { },
    failedOffer = () => { },
    isScrollEvent = false
) => {
    return (dispatch, getState) => {
        if (isRefresh) {
            dispatch(RefreshLoading(true))
        }
        if (isScrollEvent) {
            dispatch(OfferScrollLoading(true))
        } else {
            dispatch(OfferLoading(true))
        }
        getOfferApiCall(query)
            .then((res) => {
                dispatch(RefreshLoading(false))
                dispatch(OfferScrollLoading(false))
                dispatch(OfferLoading(false))
                if (isScrollEvent) {
                    let tmpArray = getState().offer.offerData;
                    dispatch(setOfferData(tmpArray.concat(res.data)));
                } else {
                    dispatch(setOfferData(res.data))
                }
                if (res.data.length > 0) {
                    successOffer(res.headers['x-wp-totalpages']);
                } else {
                    failedOffer();
                }
            })
            .catch((err) => {
                dispatch(RefreshLoading(false))
                dispatch(OfferScrollLoading(false))
                dispatch(OfferLoading(false))
                dispatch(setOfferData([]))
            });
    };
};

export const setOfferData = (payload) => {
    return { type: GET_OFFER_ACTION, payload: payload };
};

