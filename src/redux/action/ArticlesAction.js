import { GET_ARTICLES_ACTION, GET_ARTICLES_COUNT_ACTION } from "../config/actionTypes";
import { getArticlesApiCall } from "../services/talkServices";
import { ArticlesLoading, ArticlesScrollLoading, RefreshLoading } from "./LoadingAction";

export const getAllArticlesData = (
    query,
    isRefresh = false,
    successArticles = () => { },
    failedArticles = () => { },
    isScrollEvent = false
) => {
    return (dispatch, getState) => {
        if (isRefresh) {
            dispatch(RefreshLoading(true))
        }
        if (isScrollEvent) {
            dispatch(ArticlesScrollLoading(true))
        } else {
            dispatch(ArticlesLoading(true))
        }
        getArticlesApiCall(query)
            .then((res) => {
                dispatch(ArticlesScrollLoading(false))
                dispatch(RefreshLoading(false))
                dispatch(ArticlesLoading(false))

                if (isScrollEvent) {
                    let tmpArray = getState().articles.articlesData;
                    dispatch(setArticlesData(tmpArray.concat(res.data)));
                } else {
                    dispatch(setArticlesData(res.data))
                }
                if (res.data.length > 0) {
                    successArticles();
                    dispatch(setArticlesCountData(res.headers['x-wp-totalpages']))
                } else {
                    failedArticles();
                }
            })
            .catch((err) => {
                dispatch(ArticlesScrollLoading(false))
                dispatch(RefreshLoading(false))
                dispatch(ArticlesLoading(false))
                dispatch(setArticlesData([]))
            });
    };
};

export const setArticlesData = (payload) => {
    return { type: GET_ARTICLES_ACTION, payload: payload };
};

export const setArticlesCountData = (payload) => {
    return { type: GET_ARTICLES_COUNT_ACTION, payload: payload };
};
