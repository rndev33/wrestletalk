import { GET_VIDEO_ACTION, GET_VIDEO_COUNT_ACTION } from "../config/actionTypes";
import { getVideoApiCall } from "../services/talkServices";
import { RefreshLoading, VideoLoading, VideoScrollLoading } from "./LoadingAction";

export const getAllVideoData = (
    query,
    isRefresh = false,
    successVideo = () => { },
    failedVideo = () => { },
    isScrollEvent = false,
) => {
    return (dispatch, getState) => {
        if (isRefresh) {
            dispatch(RefreshLoading(true))
        }
        if (isScrollEvent) {
            dispatch(VideoScrollLoading(true))
        } else {
            dispatch(VideoLoading(true))
        }
        getVideoApiCall(query)
            .then((res) => {
                dispatch(VideoScrollLoading(false))
                dispatch(RefreshLoading(false))
                dispatch(VideoLoading(false))
                if (isScrollEvent) {
                    let tmpArray = getState().video.videoData;
                    dispatch(setVideoData(tmpArray.concat(res.data)));
                } else {
                    dispatch(setVideoData(res.data))
                }
                if (res.data.length > 0) {
                    successVideo();
                    dispatch(setVideoCountData(res.headers['x-wp-totalpages']))
                } else {
                    failedVideo();
                }
            })
            .catch((err) => {
                dispatch(VideoScrollLoading(false))
                dispatch(RefreshLoading(false))
                dispatch(VideoLoading(false))
                dispatch(setVideoData([]))
            });
    };
};

export const setVideoData = (payload) => {
    return { type: GET_VIDEO_ACTION, payload: payload };
};

export const setVideoCountData = (payload) => {
    return { type: GET_VIDEO_COUNT_ACTION, payload: payload };
};
