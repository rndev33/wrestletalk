import { getData } from "../../constants/GlobalFunction";
import { Variables } from "../../constants/Variables";
import { GET_ADS_ACTION } from "../config/actionTypes";

export const getAdsData = () => {
    return (dispatch) => {
        getData(Variables.showAd)
            .then(() => {
                dispatch(setAdsData(false))
            })
            .catch(() => {
                dispatch(setAdsData(true))
            })
    };
};

export const setAdsData = (payload) => {
    return { type: GET_ADS_ACTION, payload: payload };
};

