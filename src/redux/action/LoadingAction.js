import { ARTICLES_LOADING_ACTION, ARTICLES_SCROLL_LOADING_ACTION, AUDIO_LOADING_ACTION, AUDIO_SCROLL_LOADING_ACTION, FEATURED_LOADING_ACTION, FEATURED_SCROLL_LOADING_ACTION, NEWS_LOADING_ACTION, NEWS_SCROLL_LOADING_ACTION, OFFER_LOADING_ACTION, OFFER_SCROLL_LOADING_ACTION, REFRESH_LOADING_ACTION, SEARCH_LOADING_ACTION, SEARCH_SCROLL_LOADING_ACTION, VIDEO_LOADING_ACTION, VIDEO_SCROLL_LOADING_ACTION } from "../config/actionTypes";

export const FeaturedLoading = (payload) => {
    return (dispatch) => {
        dispatch({ type: FEATURED_LOADING_ACTION, payload: payload });
    };
};

export const FeaturedScrollLoading = (payload) => {
    return (dispatch) => {
        dispatch({ type: FEATURED_SCROLL_LOADING_ACTION, payload: payload });
    };
};

export const NewsLoading = (payload) => {
    return (dispatch) => {
        dispatch({ type: NEWS_LOADING_ACTION, payload: payload });
    };
};

export const NewsScrollLoading = (payload) => {
    return (dispatch) => {
        dispatch({ type: NEWS_SCROLL_LOADING_ACTION, payload: payload });
    };
};

export const ArticlesLoading = (payload) => {
    return (dispatch) => {
        dispatch({ type: ARTICLES_LOADING_ACTION, payload: payload });
    };
};

export const ArticlesScrollLoading = (payload) => {
    return (dispatch) => {
        dispatch({ type: ARTICLES_SCROLL_LOADING_ACTION, payload: payload });
    };
};

export const VideoLoading = (payload) => {
    return (dispatch) => {
        dispatch({ type: VIDEO_LOADING_ACTION, payload: payload });
    };
};

export const VideoScrollLoading = (payload) => {
    return (dispatch) => {
        dispatch({ type: VIDEO_SCROLL_LOADING_ACTION, payload: payload });
    };
};

export const AudioLoading = (payload) => {
    return (dispatch) => {
        dispatch({ type: AUDIO_LOADING_ACTION, payload: payload });
    };
};

export const AudioScrollLoading = (payload) => {
    return (dispatch) => {
        dispatch({ type: AUDIO_SCROLL_LOADING_ACTION, payload: payload });
    };
};

export const RefreshLoading = (payload) => {
    return (dispatch) => {
        dispatch({ type: REFRESH_LOADING_ACTION, payload: payload });
    };
};

export const SearchLoading = (payload) => {
    return (dispatch) => {
        dispatch({ type: SEARCH_LOADING_ACTION, payload: payload });
    };
};

export const SearchScrollLoading = (payload) => {
    return (dispatch) => {
        dispatch({ type: SEARCH_SCROLL_LOADING_ACTION, payload: payload });
    };
};

export const OfferLoading = (payload) => {
    return (dispatch) => {
        dispatch({ type: OFFER_LOADING_ACTION, payload: payload });
    };
};

export const OfferScrollLoading = (payload) => {
    return (dispatch) => {
        dispatch({ type: OFFER_SCROLL_LOADING_ACTION, payload: payload });
    };
};