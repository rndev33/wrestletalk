import { GET_CART_ACTION } from "../config/actionTypes";

const initialState = {
    cartData: [],
};

const CartReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_CART_ACTION: {
            return {
                ...state,
                cartData: action.payload,
            };
        }
        default: {
            return state;
        }
    }
};

export default CartReducer;
