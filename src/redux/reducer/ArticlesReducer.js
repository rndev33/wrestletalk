import { GET_ARTICLES_ACTION, GET_ARTICLES_COUNT_ACTION } from "../config/actionTypes";

const initialState = {
    articlesData: [],
    articlesCountData: 0
};

const ArticlesReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_ARTICLES_ACTION: {
            return {
                ...state,
                articlesData: action.payload,
            };
        }
        case GET_ARTICLES_COUNT_ACTION: {
            return {
                ...state,
                articlesCountData: action.payload,
            };
        }
        default: {
            return state;
        }
    }
};

export default ArticlesReducer;
