import { GET_MUGS_ACTION, GET_MUGS_FILTER_ACTION } from "../config/actionTypes";

const initialState = {
    mugsData: [],
    mugsFilterData: ''
};

const MugsReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_MUGS_ACTION: {
            return {
                ...state,
                mugsData: action.payload,
            };
        }
        case GET_MUGS_FILTER_ACTION: {
            return {
                ...state,
                mugsFilterData: action.payload,
            };
        }
        default: {
            return state;
        }
    }
};

export default MugsReducer;
