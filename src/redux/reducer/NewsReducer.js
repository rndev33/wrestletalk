import { GET_NEWS_ACTION, GET_NEWS_COUNT_ACTION } from "../config/actionTypes";

const initialState = {
    newsData: [],
    newsCountData: 0
};

const NewsReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_NEWS_ACTION: {
            return {
                ...state,
                newsData: action.payload,
            };
        }
        case GET_NEWS_COUNT_ACTION: {
            return {
                ...state,
                newsCountData: action.payload,
            };
        }
        default: {
            return state;
        }
    }
};

export default NewsReducer;
