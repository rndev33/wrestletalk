import { GET_VIDEO_ACTION, GET_VIDEO_COUNT_ACTION } from "../config/actionTypes";

const initialState = {
    videoData: [],
    videoCountData: 0
};

const VideoReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_VIDEO_ACTION: {
            return {
                ...state,
                videoData: action.payload,
            };
        }
        case GET_VIDEO_COUNT_ACTION: {
            return {
                ...state,
                videoCountData: action.payload,
            };
        }
        default: {
            return state;
        }
    }
};

export default VideoReducer;
