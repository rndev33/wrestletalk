import { GET_SEARCH_ACTION } from "../config/actionTypes";

const initialState = {
    searchData: [],
};

const SearchReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_SEARCH_ACTION: {
            return {
                ...state,
                searchData: action.payload,
            };
        }
        default: {
            return state;
        }
    }
};

export default SearchReducer;
