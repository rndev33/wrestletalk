import { combineReducers } from "redux";
import AdsReducer from "./AdsReducer";
import ArticlesReducer from "./ArticlesReducer";
import AudioReducer from "./AudioReducer";
import CartReducer from "./CartReducer";
import FavouriteReducer from "./FavouriteReducer";
import FeaturedReducer from "./FeaturedReducer";
import HoodieReducer from "./HoodieReducer";
import loadingReducer from "./LoadingReducer";
import MagazineReducer from "./MagazineReducer";
import MugsReducer from "./MugsReducer";
import NewsReducer from "./NewsReducer";
import OfferReducer from "./OfferReducer";
import SearchReducer from "./SearchReducer";
import ThemeReducer from "./ThemeReducer";
import TshirtReducer from "./TshirtReducer";
import VideoReducer from "./VideoReducer";

const rootReducer = combineReducers({
    featured: FeaturedReducer,
    loading: loadingReducer,
    news: NewsReducer,
    articles: ArticlesReducer,
    video: VideoReducer,
    audio: AudioReducer,
    search: SearchReducer,
    cart: CartReducer,
    tshirt: TshirtReducer,
    hoodie: HoodieReducer,
    mugs: MugsReducer,
    magazine: MagazineReducer,
    offer: OfferReducer,
    Favourite: FavouriteReducer,
    theme: ThemeReducer,
    ads: AdsReducer
});

const appReducer = (state, action) => {
    if (action.type === "USER_LOGGING_OUT") {
        state = undefined;
    }
    return rootReducer(state, action);
};

export default appReducer;
