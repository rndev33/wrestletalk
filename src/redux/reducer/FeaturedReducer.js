import { GET_FEATURED_ACTION, GET_FEATURED_COUNT_ACTION } from "../config/actionTypes";

const initialState = {
    featuredData: [],
    featuredCountData: 0
};

const FeaturedReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_FEATURED_ACTION: {
            return {
                ...state,
                featuredData: action.payload,
            };
        }
        case GET_FEATURED_COUNT_ACTION: {
            return {
                ...state,
                featuredCountData: action.payload,
            };
        }
        default: {
            return state;
        }
    }
};

export default FeaturedReducer;
