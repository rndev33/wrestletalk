import { ADD_SHOWED, ARTICLE_READ, GET_ADS_ACTION } from "../config/actionTypes";

const initialState = {
    AdsData: true,
    articleRead: 0
};

const AdsReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_ADS_ACTION: {
            return {
                ...state,
                AdsData: action.payload,
            };
        }
        case ARTICLE_READ: {
            return {
                ...state,
                articleRead: state.articleRead + 1
            }
        }
        case ADD_SHOWED: {
            return {
                ...state,
                articleRead: 0
            }
        }
        default: {
            return state;
        }
    }
};

export default AdsReducer;
