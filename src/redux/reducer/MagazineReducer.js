import { GET_MAGAZINE_ACTION, GET_MAGAZINE_FILTER_ACTION } from "../config/actionTypes";

const initialState = {
    magazineData: [],
    magazineFilterData: '',

};

const MagazineReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_MAGAZINE_ACTION: {
            return {
                ...state,
                magazineData: action.payload,
            };
        }
        case GET_MAGAZINE_FILTER_ACTION: {
            return {
                ...state,
                magazineFilterData: action.payload,
            };
        }
        default: {
            return state;
        }
    }
};

export default MagazineReducer;
