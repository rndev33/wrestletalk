import { GET_FAVOURITE_ACTION } from "../config/actionTypes";

const initialState = {
    FavouriteData: [],
};

const FavouriteReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_FAVOURITE_ACTION: {
            return {
                ...state,
                FavouriteData: action.payload,
            };
        }
        default: {
            return state;
        }
    }
};

export default FavouriteReducer;
