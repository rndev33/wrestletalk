import { GET_AUDIO_ACTION, GET_AUDIO_COUNT_ACTION } from "../config/actionTypes";

const initialState = {
    audioData: [],
    audioCountData: 0
};

const AudioReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_AUDIO_ACTION: {
            return {
                ...state,
                audioData: action.payload,
            };
        }
        case GET_AUDIO_COUNT_ACTION: {
            return {
                ...state,
                audioCountData: action.payload,
            };
        }
        default: {
            return state;
        }
    }
};

export default AudioReducer;
