import { GET_OFFER_ACTION } from "../config/actionTypes";

const initialState = {
    offerData: [],
};

const OfferReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_OFFER_ACTION: {
            return {
                ...state,
                offerData: action.payload,
            };
        }
        default: {
            return state;
        }
    }
};

export default OfferReducer;
