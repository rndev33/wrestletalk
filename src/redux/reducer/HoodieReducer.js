import { GET_HOODIE_ACTION, GET_HOODIE_FILTER_ACTION } from "../config/actionTypes";

const initialState = {
    hoodieData: [],
    hoodieFilterData: ''
};

const HoodieReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_HOODIE_ACTION: {
            return {
                ...state,
                hoodieData: action.payload,
            };
        }
        case GET_HOODIE_FILTER_ACTION: {
            return {
                ...state,
                hoodieFilterData: action.payload,
            };
        }
        default: {
            return state;
        }
    }
};

export default HoodieReducer;
