import { GET_TSHIRT_ACTION, GET_TSHIRT_FILTER_ACTION } from "../config/actionTypes";

const initialState = {
    tshirtData: [],
    tshirtFilterData: '',
};

const TshirtReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_TSHIRT_ACTION: {
            return {
                ...state,
                tshirtData: action.payload,
            };
        }
        case GET_TSHIRT_FILTER_ACTION: {
            return {
                ...state,
                tshirtFilterData: action.payload,
            };
        }
        default: {
            return state;
        }
    }
};

export default TshirtReducer;
