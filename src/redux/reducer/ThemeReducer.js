import { MODERN } from "../../constants/GlobalFunction";
import { GET_THEME_ACTION } from "../config/actionTypes";

const initialState = {
    themeData: MODERN,
};

const ThemeReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_THEME_ACTION: {
            return {
                ...state,
                themeData: action.payload,
            };
        }

        default: {
            return state;
        }
    }
};

export default ThemeReducer;
