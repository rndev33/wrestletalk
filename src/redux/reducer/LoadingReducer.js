import { ARTICLES_LOADING_ACTION, ARTICLES_SCROLL_LOADING_ACTION, AUDIO_LOADING_ACTION, AUDIO_SCROLL_LOADING_ACTION, FEATURED_LOADING_ACTION, FEATURED_SCROLL_LOADING_ACTION, NEWS_LOADING_ACTION, NEWS_SCROLL_LOADING_ACTION, OFFER_LOADING_ACTION, OFFER_SCROLL_LOADING_ACTION, REFRESH_LOADING_ACTION, SEARCH_LOADING_ACTION, SEARCH_SCROLL_LOADING_ACTION, VIDEO_LOADING_ACTION, VIDEO_SCROLL_LOADING_ACTION } from "../config/actionTypes";

const initialState = {
    featureLoading: false,
    newsLoading: false,
    articlesLoading: false,
    videoLoading: false,
    audioLoading: false,
    searchLoading: false,
    offerLoading: false,

    refreshLoading: false,

    featuredScrollLoading: false,
    newsScrollLoading: false,
    articlesScrollLoading: false,
    videoScrollLoading: false,
    audioScrollLoading: false,
    searchScrollLoading: false,
    offerScrollLoading: false
};

const loadingReducer = (state = initialState, action) => {
    switch (action.type) {
        case FEATURED_LOADING_ACTION: {
            return {
                ...state,
                featureLoading: action.payload,
            };
        }
        case FEATURED_SCROLL_LOADING_ACTION: {
            return {
                ...state,
                featuredScrollLoading: action.payload,
            };
        }
        case NEWS_LOADING_ACTION: {
            return {
                ...state,
                newsLoading: action.payload,
            };
        }
        case NEWS_SCROLL_LOADING_ACTION: {
            return {
                ...state,
                newsScrollLoading: action.payload,
            };
        }
        case ARTICLES_LOADING_ACTION: {
            return {
                ...state,
                articlesLoading: action.payload,
            };
        }
        case ARTICLES_SCROLL_LOADING_ACTION: {
            return {
                ...state,
                articlesScrollLoading: action.payload,
            };
        }
        case VIDEO_LOADING_ACTION: {
            return {
                ...state,
                videoLoading: action.payload,
            };
        }
        case VIDEO_SCROLL_LOADING_ACTION: {
            return {
                ...state,
                videoScrollLoading: action.payload,
            };
        }
        case AUDIO_LOADING_ACTION: {
            return {
                ...state,
                audioLoading: action.payload,
            };
        }
        case AUDIO_SCROLL_LOADING_ACTION: {
            return {
                ...state,
                audioScrollLoading: action.payload,
            };
        }
        case REFRESH_LOADING_ACTION: {
            return {
                ...state,
                refreshLoading: action.payload,
            };
        }
        case SEARCH_LOADING_ACTION: {
            return {
                ...state,
                searchLoading: action.payload,
            };
        }
        case SEARCH_SCROLL_LOADING_ACTION: {
            return {
                ...state,
                searchScrollLoading: action.payload,
            };
        }
        case OFFER_LOADING_ACTION: {
            return {
                ...state,
                offerLoading: action.payload,
            };
        }
        case OFFER_SCROLL_LOADING_ACTION: {
            return {
                ...state,
                offerScrollLoading: action.payload,
            };
        }
        default: {
            return state;
        }
    }
};

export default loadingReducer;
