import {
  GET_ALL_CATEGORY,
  GET_ALL_COMMENTS,
  GET_ALL_FEATURED_POST_API,
  GET_ALL_POSTS_API,
  GET_TAGS,
  POST_COMMENT_API,
} from '../config/apiEndPoints';
import {baseApiCall} from '../config/BaseApiCall';

var today = Math.round(new Date().getTime() / 1000);

export const getFeaturedApiCall = query => {
  return baseApiCall({
    url: GET_ALL_FEATURED_POST_API + query + `&_embed&uniqTime=${today}`,
    method: 'get',
  });
};

export const getNewsApiCall = query => {
  return baseApiCall({
    url: GET_ALL_POSTS_API + query + `&_embed&uniqTime=${today}`,
    method: 'get',
  });
};

export const getArticlesApiCall = query => {
  return baseApiCall({
    url: GET_ALL_POSTS_API + query + `&_embed&uniqTime=${today}`,
    method: 'get',
  });
};

export const getVideoApiCall = query => {
  return baseApiCall({
    url: GET_ALL_POSTS_API + query + `&_embed&uniqTime=${today}`,
    method: 'get',
  });
};

export const getAudioApiCall = query => {
  return baseApiCall({
    url: GET_ALL_POSTS_API + query,
    method: 'get',
  });
};

export const getSearchApiCall = query => {
  return baseApiCall({
    url: GET_ALL_POSTS_API + query + `&_embed&uniqTime=${today}`,
    method: 'get',
  });
};
export const getAllCommentApiCall = query => {
  return baseApiCall({
    url: GET_ALL_COMMENTS + query,
    method: 'get',
  });
};

export const postCommentApiCall = data => {
  return baseApiCall({
    url: POST_COMMENT_API,
    method: 'post',
    data,
  });
};

export const getPostDetailApiCall = id => {
  return baseApiCall({
    url: GET_ALL_POSTS_API + '/' + id + '?_embed',
    method: 'get',
  });
};

export const getAllCategoryApiCall = () => {
  return baseApiCall({
    url: GET_ALL_CATEGORY,
    method: 'get',
  });
};

export const getTagsApi = () => {
  return baseApiCall({
    url: GET_TAGS,
    method: 'get',
  });
};
