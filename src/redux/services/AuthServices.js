import { DELETE_ACCOUNT_API, FAVOURITE_API, GET_NONCE_API, GET_NOTIFICATION_CATEGORY, GET_TOKEN_API, GET_USER_DETAILS, GET_USER_TOKEN, LOGIN_API, REGISTER_API, REGISTER_TOKEN_API, RESET_PASSWORD_API, SET_PASSWORD_API, VALIDATE_CODE_API } from "../config/apiEndPoints";
import { baseApiCall } from "../config/BaseApiCall";

export const registerApiCall = (data) => {
    return baseApiCall({
        url: REGISTER_API,
        method: "post",
        data
    });
}
export const getUserToken = data => {
    return baseApiCall({
      url: GET_USER_TOKEN,
      method: 'post',
      data,
    });
  };
    
export const getNonceCall = () => {
    return baseApiCall({
        url: GET_NONCE_API,
        method: "get"
    });
}
export const getLoginCall = (data) => {
    return baseApiCall({
        url: LOGIN_API + data,
        method: "get",
    });
}
export const resetPasswordCall = (data) => {
    return baseApiCall({
        url: RESET_PASSWORD_API,
        method: "post",
        data
    });
}
export const validateCodeCall = (data) => {
    return baseApiCall({
        url: VALIDATE_CODE_API,
        method: "post",
        data
    });
}
export const setPasswordCall = (data) => {
    return baseApiCall({
        url: SET_PASSWORD_API,
        method: "post",
        data
    });
}

export const addRegisterTokenCall = (data) => {
    return baseApiCall({
        url: REGISTER_TOKEN_API,
        method: "post",
        data
    });
}

export const getFavouriteCall = () => {
    return baseApiCall({
        url: FAVOURITE_API,
        method: "get",
    });
}

export const getTokenCall = (data) => {
    return baseApiCall({
        url: GET_TOKEN_API,
        method: "post",
        data
    });
}
export const getAllNotificationCategory = (data) => {
    return baseApiCall({
        url: GET_NOTIFICATION_CATEGORY + data,
        method: "get"
    });
}
export const deleteAccountApiCall = (data) => {
    return baseApiCall({
        url: DELETE_ACCOUNT_API + data,
        method: "get"
    });
}
export const getUserDetails = (data) => {
    return baseApiCall({
        url: GET_USER_DETAILS + data,
        method: "get"
    });
}