import messaging from '@react-native-firebase/messaging';
import {Platform} from 'react-native';
class FCMService {
  register = (onRegister, onNotification, onOpenNotification) => {
    this.checkPermission(onRegister).then(r => {});
    this.createNotificationListeners(
      onRegister,
      onNotification,
      onOpenNotification,
    );
  };
  registerAppWithFCM = async () => {
    if (Platform.OS === 'ios') {
      await messaging().registerDeviceForRemoteMessages();
      await messaging().setAutoInitEnabled(true);
    }
  };
  checkPermission = async onRegister => {
    await messaging()
      .hasPermission()
      .then(enabled => {
        if (enabled) {
          // User has permission
          this.getToken(onRegister);
        } else {
          // User does not have permission
          this.requestPermission(onRegister);
        }
      })
      .catch(error => {});
  };
  getToken = onRegister => {
    messaging()
      .getToken()
      .then(fcmToken => {
        console.log('fcmToken', fcmToken);
        if (fcmToken) {
          onRegister(fcmToken);
        } else {
        }
      })
      .catch(error => {});
  };
  requestPermission = onRegister => {
    messaging()
      .requestPermission()
      .then(() => {
        this.getToken(onRegister);
      })
      .catch(error => {});
  };
  deleteToken = () => {
    messaging()
      .deleteToken()
      .catch(error => {});
  };
  createNotificationListeners = (
    onRegister,
    onNotification,
    onOpenNotification,
  ) => {
    // when app is running in background
    messaging().onNotificationOpenedApp(remoteMessage => {
      if (remoteMessage) {
        const notification = remoteMessage.data;
        onOpenNotification(remoteMessage);
      }
    });
    // when app is opened from quit state
    messaging()
      .getInitialNotification()
      .then(remoteMessage => {
        if (remoteMessage) {
          const notification = remoteMessage.notification;
          onOpenNotification(remoteMessage);
        }
      });
    // when app is in foreground
    this.messageListener = messaging().onMessage(async remoteMessage => {
      if (remoteMessage) {
        let notification = null;
        if (Platform.OS === 'ios') {
          notification = remoteMessage.data; //remoteMessage.data.notification;
        } else {
          notification = remoteMessage.data;
        }
        onNotification(notification);
      }
    });
    // triggered when we have new token
    messaging().onTokenRefresh(fcmToken => {
      onRegister(fcmToken);
    });
  };
  unRegister = () => {
    this.messageListener();
  };
}
export const fcmService = new FCMService();
