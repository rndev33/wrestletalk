import {
  GET_DASHBOARD_DATA_API,
  GET_LEADERBOARD_DATA,
  GET_LEAGUE_IMAGE_API,
  GET_MYSCORE_DATA,
  GET_RESULTS_DATA,
} from '../config/apiEndPoints';
import {baseApiCall} from '../config/BaseApiCall';

export const getLeagueImageApiCall = () => {
  return baseApiCall({
    url: GET_LEAGUE_IMAGE_API,
    method: 'get',
  });
};

export const getDashboardData = () => {
  return baseApiCall({
    url: GET_DASHBOARD_DATA_API,
    method: 'get',
  });
};


export const getLeaderBoardData = () => {
  return baseApiCall({
    url: GET_LEADERBOARD_DATA,
    method: 'get',
  });
};


export const getResultsData = () => {
  return baseApiCall({
    url: GET_RESULTS_DATA,
    method: 'get',
  });
};

export const getMyScoreData = () => {
  return baseApiCall({
    url: GET_MYSCORE_DATA,
    method: 'get',
  });
};

export const getResultsDataById = (id) => {
  return baseApiCall({
    url: `${GET_RESULTS_DATA}?season_id=${id}`,
    method: 'get',
  });
};
