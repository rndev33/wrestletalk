import { ADD_CART_API, GET_ALL_CART_API, GET_ALL_MAGAZINE_API, UPDATE_CART_API } from "../config/apiEndPoints";
import { baseApiCall } from "../config/BaseApiCall";

export const getMagazineApiCall = () => {
    return baseApiCall({
        url: GET_ALL_MAGAZINE_API,
        method: "get"
    });
}

export const getCartApiCall = () => {
    return baseApiCall({
        url: GET_ALL_CART_API,
        method: "get"
    });
}

export const updateCartApiCall = (data) => {
    return baseApiCall({
        url: UPDATE_CART_API,
        method: "post",
        data
    });
}

export const addCartApiCall = (data) => {
    return baseApiCall({
        url: ADD_CART_API,
        method: "post",
        data
    });
}
