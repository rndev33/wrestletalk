import moment from 'moment';

const getTimeDiff = (time, reverse) => {
  const now = moment(),
    secondTime = moment(time);
  return moment.duration(reverse ? now.diff(secondTime) : secondTime.diff(now));
};

const addLeadingZero = val => {
  return val < 10 ? `0${val}` : val;
};

export {getTimeDiff, addLeadingZero};
