import PushNotification from "react-native-push-notification";
import PushNotificationIOS from "@react-native-community/push-notification-ios";
import { Platform } from "react-native";

class LocalNotificationService {
  configure = (onRegister, onNotification, onOpenNotification) => {
    PushNotification.configure({
      onRegister: function (token) {
      },
      onNotification (notification) {
        if (notification.userInteraction) {
          onOpenNotification(
            Platform.OS === "ios" ? notification : notification
          );
        }
        notification.finish(PushNotificationIOS.FetchResult.NoData);
      },
      onAction: function (notification) {
      },
      onRegistrationError: function (err) {
      },
      senderId: "734378397995",
      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },
      playSound: true,
      popInitialNotification: true,
      requestPermissions: true,
    });
  };

  unRegister = () => {
    PushNotification.unregister();
  };

  showNotification = (id, title, message, data = {}, options = {}) => {
    PushNotification.localNotification({
      ...this.buildAndroidNotification(id, title, message, data, options),
      ...this.buildIOSNotification(id, title, message, data, options),
      title: title || "",
      message: message || "",
      playSound: true,
      userInteraction: false,
      channelId: "new_notification_arrived_channel",
    });
  };
  buildAndroidNotification = (id, title, message, data = {}, options = {}) => {
    return {
      id: id,
      autoCancel: true,
      // largeIcon: options.largeIcon || "ic_launcher",
      smallIcon: "ic_launcher" || "ic_launcher_round",
      bigText: message || "",
      // subText: title || '',
      vibrate: true,
      vibration: 300,
      priority: "high",
      importance: "high",
      data: data,
    };
  };
  buildIOSNotification = (id, title, message, data = {}, options = {}) => {
    return {
      alertAction: options.alertAction || "view",
      alertTitle: title || 'WrestleTalk',
      alertBody: message || '',
      category: options.category || "",
      userInfo: {
        id: id,
        item: data,
      },
    };
  };
  cancelAllLocalNotification = () => {
    if (Platform.OS === "ios") {
      PushNotificationIOS.removeAllDeliveredNotifications();
    } else {
      PushNotification.cancelAllLocalNotifications();
    }
  };
  removeDeliveredNotificationByID = (notificationId) => {
    PushNotification.cancelLocalNotifications({ id: `${notificationId}` });
  };
}
export const localNotificationService = new LocalNotificationService();
