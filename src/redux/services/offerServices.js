import { GET_ALL_POSTS_API } from "../config/apiEndPoints";
import { baseApiCall } from "../config/BaseApiCall";

export const getOfferApiCall = (query) => {
    return baseApiCall({
        url: GET_ALL_POSTS_API + query,
        method: "get"
    });
}
