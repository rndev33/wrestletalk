import axios from 'axios';
import {Variables} from '../../constants/Variables';
import {getData} from '../../constants/GlobalFunction';

//staging url
export const mainUrl = 'https://league.wrestletalk.com/api/';

const instance = axios.create({
  baseURL: 'https://league.wrestletalk.com/api/',
});
export const initialConfig = () => {
  setupAxios();
};
const setupAxios = async () => {
  instance.interceptors.request.use(
    async config => {
      let token = await getData(Variables?.token)
        .then(res => {
          console.log('---   token ---', res);
          if (res) {
            return res;
          } else {
            return null;
          }
        })
        .catch(err => {
          console.log('---   token --- err', err);

          return null;
        });
      if (token) {
        config.headers.Authorization = `Bearer ${JSON.parse(token)}`;
      }
      return config;
    },
    err => {
      Promise.reject(err);
    },
  );
};

export default instance;
