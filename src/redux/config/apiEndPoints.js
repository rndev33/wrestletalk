//league tab
export const GET_LEAGUE_IMAGE_API =
  'https://wrestletalk.com/wp-json/wp/v2/posts/328389';
export const GET_DASHBOARD_DATA_API = 'https://league.wrestletalk.com/api/data';
export const GET_LEADERBOARD_DATA = 'https://league.wrestletalk.com/api/leaderboard';
export const GET_RESULTS_DATA = 'https://league.wrestletalk.com/api/results';
export const GET_MYSCORE_DATA = 'https://league.wrestletalk.com/api/myscore';

//talk tab
export const GET_ALL_POSTS_API = 'https://wrestletalk.com/wp-json/wp/v2/posts';
export const GET_ALL_COMMENTS =
  'https://wrestletalk.com/wp-json/wp/v2/comments';
export const GET_ALL_FEATURED_POST_API =
  'https://wrestletalk.com/wp-json/wp/v2/lf_hmoepage_mobile_banner';
export const POST_COMMENT_API =
  'https://wrestletalk.com/wp-json/wp/v2/comments';
export const GET_ALL_CATEGORY =
  'https://wrestletalk.com/wp-json/wp/v2/categories';
export const GET_TAGS =
  'https://wrestletalk.com/wp-json/wp/v2/api_improved_post_data';

// Product Tab
export const GET_ALL_MAGAZINE_API =
  'https://wrestleshop.com/products.json?limit=250';
export const GET_ALL_CART_API = 'https://wrestle-talk-uk.myshopify.com/cart.js';
export const UPDATE_CART_API =
  'https://wrestle-talk-uk.myshopify.com/cart/update.js';
export const ADD_CART_API = 'https://wrestle-talk-uk.myshopify.com/cart/add.js';

//auth
export const REGISTER_API =
  'https://wrestletalk.com/wp-json/wp/v2/user_registration_wt_lo';
export const LOGIN_API =
  'https://wrestletalk.com/api/auth/generate_auth_cookie/?';
export const RESET_PASSWORD_API =
  'https://wrestletalk.com/wp-json/bdpwr/v1/reset-password';
export const VALIDATE_CODE_API =
  'https://wrestletalk.com/wp-json/bdpwr/v1/validate-code';
export const SET_PASSWORD_API =
  'https://wrestletalk.com/wp-json/bdpwr/v1/set-password';
export const GET_NONCE_API =
  'https://wrestletalk.com/api/get_nonce/?controller=user&method=register';
export const REGISTER_TOKEN_API = 'https://wrestletalk.com/pnfw/register/';
export const GET_TOKEN_API =
  'https://wrestletalk.com//wp-json/jwt-auth/v1/token';
export const GET_USER_DETAILS =
  'https://wrestletalk.com/wp-json/wp/v2/getuserdatalmn?';
  export const GET_USER_TOKEN =
  'https://league.wrestletalk.com/api/auth/register';
// Setting
export const FAVOURITE_API =
  'https://wrestletalk.com/wp-json/wp/v2/profile_my_home';
export const GET_NOTIFICATION_POST_API = 'http://wrestletalk.com/pnfw/posts/';
export const GET_NOTIFICATION_CATEGORY =
  'https://wrestletalk.com/wp-json/wp/v2/lf_get_categories_notifications?token=';
export const DELETE_ACCOUNT_API =
  'https://wrestletalk.com/wp-json/wp/v2/lf_del_user_xe_rex?em=';
