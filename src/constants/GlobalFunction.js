import Toast from "react-native-simple-toast";
import instance from "../redux/config/apiConfig";
import { Variables } from "./Variables";
import AsyncStorage from '@react-native-async-storage/async-storage';

//talk screem
export const FEATURED = 'FEATURED'
export const LATEST_NEWS = 'NEWS'
export const ARTICLES = 'ARTICLES'
export const VIDEOS = 'VIDEOS'
export const AUDIO = 'PODCASTS'

//league screen
export const CURRENT_ROUND = 'CURRENT ROUND'
export const RESULTS = 'RESULTS'
export const STANDINGS = 'STANDINGS'
export const PAST_SEASONS = 'PAST SEASONS'
export const DASHBOARD = 'DASHBOARD'
export const LEADERBOARD = 'LEADERBOARD'
export const MY_SCORE = 'MY SCORE'

// Shop screen
export const TSHIRTS = 'T-SHIRTS'
export const HOODIES = 'HOODIES'
export const MUGS = 'MUGS'
export const MAGAZINE = 'MAGAZINE'

// Shop Detail screen
export const DESCRIPTION = 'DESCRIPTION'
export const REVIEWS = 'REVIEWS (0)'
export const SIZE_CHART = 'SIZE CHART'
export const SHIPPING_DELIVERY = 'SHIPPING & DELIVERY'

// Checkout screen
export const CONTACT = 'CONTACT'
export const SHIPPING = 'SHIPPING'
export const PAYMENT = 'PAYMENT'
export const REVIEW = 'REVIEW'

//product screen
export const MUG_IDS = ['4620254609485', '4715200905293', '7039251775673', '4662316859469', '6930081480889', '6950271189177', '7611895939298', '7516565864674', '7513540264162']
export const HOODIE_IDS = ['4620496535629', '4617217769549', '4618211524685', '4671793397837', '4715173806157', '4765206609997', '4808189214797', '4846420623437', '4846629060685', "4627913965645", '7513514115298', '7521036632290', '7608066212066', '7622101532898', '7637738848482']
export const TSHIRT_IDS = ['4666320289869', '4662255943757', '4658372051021', '4627949617229', '4627856818253', '4618201858125', '4666985316429', '4671775604813', '4675716022349', "4748458688589", '4765055975501', '4776977268813', '4808181907533', '4846417936461', '4846624538701', '4855724113997', '6097787453625', '6097798529209', '6097990615225', '6098001461433', '6141353722041', '6141415784633', '6583047291065', '6583020552377', "6639113011385", '6652491399353', '6715672199353', '6877654286521', '6930052022457', '6930059624633', '7036141469881', '7036151726265', '7513453527266', '7513553993954', '7515313897698', '7607300358370', '7611882766562', '7614368448738', '7621797347554', "7622458015970", '7631822880994', '7637781414114', '7707214774498', '7714655109346', '7717107040482', '7718609125602']

// Search Screen
export const MODERN = 'MODERN'
export const CLASSIC = 'CLASSIC'

export const getDifferenceInHours = (date1, date2) => {
    const diffInMs = Math.abs(date2 - date1);
    return parseInt(diffInMs / (1000 * 60 * 60 * 24));
}

export const ToastAlert = (data) => {
    return Toast.showWithGravity(data, Toast.SHORT, Toast.BOTTOM);
};

export const isEmail = (text) => {
    const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (text === "") {
        return false;
    } else if (reg.test(text)) {
        return true;
    } else {
        return false;
    }
};

export const isEmpty = (text) => {
    if (text === "" || (text?.trim() === "")) {
        return false;
    } else {
        return true;
    }
}

export const setAccessToken = async (payload) => {
    await saveData(Variables.accessToken, JSON.stringify(payload));
};

export const saveData = async (key, data) => {
    await AsyncStorage.setItem(key, data);
};

export const removeData = async (key) => {
    await AsyncStorage.removeItem(key);
};

export const getData = async (key) => {
    return new Promise((resolve, reject) => {
        AsyncStorage.getItem(key)
            .then((res) => {
                if (res) {
                    resolve(res);
                } else {
                    reject(res);
                }
            })
            .catch((err) => {
                reject(err);
            });
    });
};