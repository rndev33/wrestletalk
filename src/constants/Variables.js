export const Variables = {
  accessToken: '@accessToken',
  postId: '@postId',
  theme: '@theme',
  email: '@email',
  showAd: '@showAd',
  user: '@user',
  token:"@token"
};
