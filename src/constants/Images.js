module.exports = {
    //tab bar
    wrestleTalkTab: require('./../assets/wrestleTalkTab.png'),
    wrestleTalkTabSelected: require('./../assets/wrestleTalkTabSelected.png'),
    shopTab: require('./../assets/shopTab.png'),
    shopTabSelected: require('./../assets/shopTabSelected.png'),
    leagueTab: require('./../assets/leagueTab.png'),
    leagueTabSelected: require('./../assets/leagueTabSelected.png'),
    offerTab: require('./../assets/offerTab.png'),
    offerTabSelected: require('./../assets/offerTabSelected.png'),

    //header component
    mainLogo: require('./../assets/mainLogo.png'),
    searchIcon: require('./../assets/searchIcon.png'),
    verticalMenu: require('./../assets/verticalMenu.png'),
    arrowLeft: require('./../assets/arrow-left.png'),

    //league tab
    dummyBanner: require('./../assets/dummyBanner.png'),

    //Talk tab
    cancelIcon: require('./../assets/cancelIcon.png'),
    downArrow: require('./../assets/downArrow.png'),
    shadow: require('./../assets/shadow.png'),
    smallShadow: require('./../assets/smallShadow.png'),
    noSearchIcon: require('./../assets/noSearchIcon.png'),

    //Article screen
    redBackIcon: require('./../assets/redBackIcon.png'),
    shareIcon: require('./../assets/shareIcon.png'),
    likeIcon: require('./../assets/likeIcon.png'),
    disLikeIcon: require('./../assets/disLikeIcon.png'),
    downCommentArrow: require('./../assets/downCommentArrow.png'),

    //current round
    unSelectRound: require('./../assets/unSelectRound.png'),
    selectedRound: require('./../assets/selectedRound.png'),

    //Shop tab
    searchGrayIcon: require('./../assets/searchGrayIcon.png'),
    rightRoundArrow: require('./../assets/rightRoundArrow.png'),

    //cart screen
    plusRedIcon: require('./../assets/plusRedIcon.png'),
    minusRedIcon: require('./../assets/minusRedIcon.png'),

    //checkout screen
    arrowRight: require('./../assets/arrowRight.png'),

    //payment screen
    unSelectRadio: require('./../assets/unSelectRadio.png'),
    selectRadio: require('./../assets/selectRadio.png'),

    //Setting Screen
    grayStarIcon: require('./../assets/grayStarIcon.png'),
    splashBackground: require('./../assets/splashBackground.png'),
    cancleIcon: require('./../assets/cancleIcon.png'),
};