const { Platform } = require("react-native");

const isIos = Platform.OS === 'ios'

const Fonts = {
    bold25: {
        fontSize: 25,
        fontFamily: isIos ? 'Montserrat-Black' : 'Gotham Black Regular', //25 Bold-Regular
    },
    bold18: {
        fontSize: 18,
        fontFamily: isIos ? 'Montserrat-Black' : 'GothamBold'  //18 bold
    },
    bold16: {
        fontSize: 16,
        fontFamily: isIos ? 'Montserrat-Black' : 'GothamBold'  //16 bold
    },
    medium16: {
        fontSize: 16,
        fontFamily: isIos ? 'Montserrat-Medium' : 'GothamMedium'  //16 Medium
    },
    bold15: {
        fontSize: 15,
        fontFamily: isIos ? 'Montserrat-Black' : 'GothamBold'  //15 bold
    },
    bold14: {
        fontSize: 14,
        fontFamily: isIos ? 'Montserrat-Black' : 'GothamBold'  //14 bold
    },
    medium14: {
        fontSize: 14,
        fontFamily: isIos ? 'Montserrat-Medium' : 'GothamMedium'  //14 Medium
    },
    bold13: {
        fontSize: 13,
        fontFamily: isIos ? 'Montserrat-Black' : 'GothamBold'  //13 bold
    },
    bold13Black: {
        fontSize: 13,
        fontFamily: isIos ? 'Montserrat-Black' : 'Gotham Black Regular', //13 Bold-Regular
    },
    bold12: {
        fontSize: 12,
        fontFamily: isIos ? 'Montserrat-Black' : 'GothamBold'  //12 bold
    },
    medium12: {
        fontSize: 12,
        fontFamily: isIos ? 'Montserrat-Medium' : 'GothamMedium'  //12 medium
    },
    medium11: {
        fontSize: 11,
        fontFamily: isIos ? 'Montserrat-Medium' : 'GothamMedium'  //12 medium
    },
    bold10: {
        fontSize: 10,
        fontFamily: isIos ? 'Montserrat-Black' : 'GothamBold'  //10 bold
    },
    medium10: {
        fontSize: 10,
        fontFamily: isIos ? 'Montserrat-Medium' : 'GothamMedium'  //10 medium
    },
    medium9: {
        fontSize: 9,
        fontFamily: isIos ? 'Montserrat-Medium' : 'GothamMedium'  //9 medium
    },
    bold8: {
        fontSize: 8,
        fontFamily: isIos ? 'Montserrat-Black' : 'GothamBold'  //8 bold
    },
    medium8: {
        fontSize: 8,
        fontFamily: isIos ? 'Montserrat-Medium' : 'GothamMedium'  //8 Medium
    },
    medium7: {
        fontSize: 7,
        fontFamily: isIos ? 'Montserrat-Medium' : 'GothamMedium'  //7 Medium
    },
    bold6: {
        fontSize: 6,
        fontFamily: isIos ? 'Montserrat-Black' : 'GothamBold'  //6 bold
    },
};
module.exports = Fonts;
