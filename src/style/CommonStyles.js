import {StyleSheet} from 'react-native';
import constants from '../constants';
import {
  BACK_TABLE,
  BLACK,
  BORDER_COLOR,
  DARK_BLACK,
  DARK_BORDER_COLOR,
  DARK_GRAY,
  DARK_RED,
  DARK_WHITE,
  GRAY,
  GRAY_TEXT,
  INPUT_COLOR,
  LIGHT_BORDER,
  PRIMARY_COLOR,
  WHITE,
} from '../constants/Colors';

const HEIGHT = constants.BaseStyle.DEVICE_HEIGHT / 100;
const WIDTH = constants.BaseStyle.DEVICE_WIDTH / 100;

const CommonStyles = StyleSheet.create({
  //header component
  headerView: {
    backgroundColor: PRIMARY_COLOR,
    paddingHorizontal: WIDTH * 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: HEIGHT * 2,
  },
  iconStyle: {
    width: WIDTH * 5,
    height: WIDTH * 5,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 15,
  },
  menuPositionStyle: {
    top: HEIGHT * 2,
  },

  //top tab
  topTabView: {
    backgroundColor: DARK_GRAY,
    paddingHorizontal: WIDTH * 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  topScrollableTabView: {
    backgroundColor: DARK_GRAY,
    paddingHorizontal: WIDTH * 5,
  },
  innerTopBlock: {
    paddingVertical: 15,
    borderBottomWidth: WIDTH * 1,
    borderBottomColor: DARK_BORDER_COLOR,
  },

  //league screen
  mainContainer: {
    flex: 1,
    // backgroundColor: WHITE,
    backgroundColor: '#F8F8F8',
  },

  //current round
  currentRoundBannerStyle: {
    width: WIDTH * 100,
    height: HEIGHT * 25,
  },
  timerView: {
    borderRadius: 4,
    paddingVertical: 8,
    paddingHorizontal: WIDTH * 2,
    marginLeft: WIDTH * 2,
  },
  playerView: {
    backgroundColor: BACK_TABLE,
    marginTop: 7,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: WIDTH * 4,
    paddingVertical: 15,
  },

  //videos
  videosBannerStyle: {
    borderRadius: 8,
    width: WIDTH * 90,
    height: HEIGHT * 25,
  },
  tagBoxView: {
    borderWidth: 0.5,
    borderColor: DARK_BLACK,
    borderRadius: 4,
    padding: 5,
    marginLeft: WIDTH * 2,
    marginBottom: HEIGHT * 0.8,
  },
  lineView: {
    borderWidth: 0.5,
    borderColor: LIGHT_BORDER,
  },

  //Search screen
  searchInputView: {
    borderRadius: 5,
    backgroundColor: '#F2F2F2',
    width: WIDTH * 70,
    paddingHorizontal: WIDTH * 3,
    height: 38,
    justifyContent: 'center',
  },
  filterSearchView: {
    backgroundColor: PRIMARY_COLOR,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    paddingBottom: HEIGHT * 2,
    paddingTop: HEIGHT * 1,
  },
  searchItemView: {
    width: WIDTH * 43,
    height: WIDTH * 43,
    marginRight: WIDTH * 4,
    marginBottom: HEIGHT * 2,
  },
  searchItemSkeletonView: {
    width: WIDTH * 43,
    height: WIDTH * 43,
    marginRight: WIDTH * 4,
    marginBottom: HEIGHT * 2,
    borderRadius: 8,
  },
  searchItem: {
    width: WIDTH * 43,
    height: WIDTH * 43,
    borderRadius: 8,
  },
  positionDarkviewSearch: {
    width: WIDTH * 43,
    height: HEIGHT * 20,
    position: 'absolute',
    bottom: 0,
    borderBottomRightRadius: 8,
    borderBottomLeftRadius: 8,
  },
  serachResultView: {
    backgroundColor: DARK_GRAY,
    paddingVertical: HEIGHT * 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  positionSearchContent: {
    position: 'absolute',
    bottom: HEIGHT * 2,
    alignSelf: 'center',
    paddingHorizontal: WIDTH * 2,
  },
  noSearchView: {
    height: HEIGHT * 60,
    alignItems: 'center',
    justifyContent: 'center',
  },

  // latest news
  trandingTopicView: {
    backgroundColor: PRIMARY_COLOR,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: HEIGHT * 2,
    paddingHorizontal: WIDTH * 5,
  },
  trandingTopicBox: {
    borderWidth: 0.5,
    borderColor: DARK_GRAY,
    borderRadius: 4,
    padding: 5,
    marginLeft: WIDTH * 2,
  },
  textPositionOnImage: {
    position: 'absolute',
    bottom: HEIGHT * 2,
    left: WIDTH * 6,
    right: WIDTH * 4,
  },
  verticalLine: {
    height: 10,
    backgroundColor: DARK_WHITE,
    width: 1,
  },
  positionDarkview: {
    width: WIDTH * 90,
    height: HEIGHT * 20,
    position: 'absolute',
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 8,
    bottom: 0,
  },
  newsSecondDesign: {
    marginLeft: WIDTH * 4,
  },
  newsSmallImage: {
    width: WIDTH * 30,
    height: HEIGHT * 9,
    borderRadius: 3,
  },
  newsLineView: {
    borderWidth: 2,
    borderColor: GRAY + 10,
    backgroundColor: GRAY + 10,
    marginVertical: HEIGHT * 0.5,
  },

  //Article screen
  textSkeleton: {
    height: 10,
    width: WIDTH * 90,
    borderRadius: 5,
    marginTop: HEIGHT * 1,
  },
  textPositionOnArticleImage: {
    position: 'absolute',
    bottom: HEIGHT * 2,
    left: WIDTH * 4,
    right: WIDTH * 4,
  },
  articleBackIcon: {
    position: 'absolute',
    left: 10,
    top: 10,
  },
  articleTopicBox: {
    borderWidth: 0.5,
    borderColor: DARK_GRAY,
    borderRadius: 4,
    padding: 5,
    marginRight: WIDTH * 2,
  },
  addBannner: {
    height: HEIGHT * 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: GRAY_TEXT + 60,
    marginVertical: HEIGHT * 2,
  },

  //standings
  standingBlock: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 12,
  },

  //Shop tab
  shopProductView: {
    width: WIDTH * 44,
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 3,
    marginTop: HEIGHT * 2,
    marginRight: WIDTH * 2,
    backgroundColor: WHITE,
    borderRadius: 5,
    paddingVertical: HEIGHT * 2,
  },
  prodctImageStyle: {
    height: HEIGHT * 17,
    width: WIDTH * 23,
  },
  addToCartButton: {
    backgroundColor: BLACK,
    marginTop: HEIGHT * 2,
    paddingVertical: HEIGHT * 1,
    width: WIDTH * 32,
    alignItems: 'center',
    justifyContent: 'center',
  },
  filterView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    backgroundColor: WHITE,
    paddingVertical: HEIGHT * 1,
    paddingHorizontal: WIDTH * 5,
    borderTopColor: BORDER_COLOR,
    borderTopWidth: 1,
  },
  filterButton: {
    alignItems: 'center',
    height: 35,
    justifyContent: 'center',
    flexDirection: 'row',
    width: WIDTH * 45,
  },
  redCountView: {
    backgroundColor: '#EF3441',
    width: 23,
    height: 23,
    borderRadius: 90,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: WIDTH * 2,
  },
  bottomVerticalLine: {
    height: 35,
    width: 1,
    backgroundColor: BORDER_COLOR,
  },
  inputView: {
    borderBottomWidth: 1,
    width: WIDTH * 90,
    alignSelf: 'center',
    borderBottomColor: BORDER_COLOR,
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  sortView: {
    borderWidth: 1,
    borderColor: BLACK,
    borderRadius: 6,
    paddingHorizontal: WIDTH * 3,
    paddingVertical: HEIGHT * 0.7,
    marginRight: WIDTH * 2,
    marginBottom: HEIGHT * 1.5,
  },
  priceInput: {
    width: WIDTH * 44,
    alignSelf: 'center',
    height: 40,
    paddingHorizontal: WIDTH * 3,
    borderRadius: 8,
    borderWidth: 1,
    borderColor: INPUT_COLOR,
    flexDirection: 'row',
    alignItems: 'center',
  },
  productDetailContainer: {
    // height: HEIGHT * 100,
    backgroundColor: WHITE,
    paddingVertical: HEIGHT * 2,
    paddingHorizontal: WIDTH * 5,
  },
  detailBackArrow: {
    position: 'absolute',
    top: HEIGHT * 2,
    left: WIDTH * 4,
  },
  nextArrow: {
    position: 'absolute',
    top: HEIGHT * 12,
    right: WIDTH * 13,
  },
  sizeBoxView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: WIDTH * 43,
    borderWidth: 2,
    borderColor: INPUT_COLOR,
    paddingHorizontal: WIDTH * 2,
    paddingVertical: HEIGHT * 1.5,
  },
  colorMenuPositionStyle: {
    position: 'absolute',
    zIndex: 999999,
    top: HEIGHT * 5,
  },
  sizeMenuPositionStyle: {
    position: 'absolute',
    zIndex: 999999,
    top: HEIGHT * 5,
    right: 0,
  },
  sizeChartStyle: {
    width: WIDTH * 90,
    height: HEIGHT * 20,
  },
  dateMenuPositionStyle: {
    position: 'absolute',
    zIndex: 999999,
    top: HEIGHT * 5,
    left: WIDTH * 10,
  },

  //cart screen
  cartImageStyle: {
    width: WIDTH * 20,
    height: WIDTH * 20,
  },
  labelView: {
    backgroundColor: DARK_RED,
    paddingHorizontal: WIDTH * 2,
    paddingVertical: 5,
    alignSelf: 'flex-start',
    marginVertical: HEIGHT * 1.5,
  },
  quantityView: {
    borderRadius: 5,
    borderWidth: 1,
    borderColor: INPUT_COLOR,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: WIDTH * 24,
    height: 25,
  },
  quantitySubView: {
    width: WIDTH * 8,
    alignItems: 'center',
    justifyContent: 'center',
    height: 25,
  },

  //contact screen
  textInputView: {
    borderWidth: 1,
    borderColor: '#E3E3E3',
    height: 40,
    paddingHorizontal: WIDTH * 3,
    marginTop: HEIGHT * 1,
  },

  //payment screen
  paymentMethodView: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#E3E3E3' + 60,
    paddingHorizontal: WIDTH * 3,
    paddingVertical: 10,
  },
  wholePaymentBlock: {
    borderWidth: 2,
    borderColor: '#E3E3E3',
    marginVertical: HEIGHT * 2,
  },
  borderSelectedView: {
    borderTopWidth: 2,
    borderTopColor: '#E3E3E3',
  },

  //review screem
  reviewBorderView: {
    borderWidth: 1,
    borderColor: '#E3E3E3',
    paddingHorizontal: WIDTH * 3,
  },

  //setting screen
  addFreeButton: {
    backgroundColor: BLACK,
    width: WIDTH * 43,
    paddingVertical: HEIGHT * 1.5,
  },
  premiunButton: {
    backgroundColor: BLACK,
    paddingHorizontal: WIDTH * 4,
    paddingVertical: HEIGHT * 1.5,
  },
  selectedSubjectedView: {
    borderColor: '#E3E3E3',
    borderWidth: 1,
    borderRadius: 5,
    paddingHorizontal: WIDTH * 3,
    paddingVertical: HEIGHT * 1,
    marginTop: HEIGHT * 1,
  },

  //splash screen
  splashImageStyle: {
    height: HEIGHT * 100,
    width: WIDTH * 100,
    position: 'absolute',
  },
  splashLogoStyle: {
    width: WIDTH * 60,
    height: HEIGHT * 8,
  },

  //Offer Screen
  offerCardView: {
    borderRadius: 20,
    paddingBottom: HEIGHT * 3,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 5,
    backgroundColor: WHITE,
    marginHorizontal: WIDTH * 1,
    marginBottom: HEIGHT * 3,
  },
  offerTopImage: {
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    width: WIDTH * 88,
    height: HEIGHT * 10,
  },
  offerLogoStyle: {
    width: WIDTH * 20,
    height: HEIGHT * 5,
  },

  //Offer Detail
  offerMidImage: {
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    width: WIDTH * 80,
    height: HEIGHT * 10,
  },
  offerDetailCardView: {
    borderRadius: 20,
    paddingBottom: HEIGHT * 2,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 5,
    backgroundColor: WHITE,
    width: WIDTH * 80,
    marginHorizontal: WIDTH * 10,
    marginTop: -HEIGHT * 10,
    marginBottom: HEIGHT * 3,
  },
  moreDetailButton: {
    backgroundColor: BLACK,
    paddingVertical: HEIGHT * 1,
    width: WIDTH * 32,
    alignItems: 'center',
    justifyContent: 'center',
  },
  mainOfferImageStyle: {
    width: WIDTH * 100,
    height: HEIGHT * 60,
  },

  // Empty View
  emptyView: {
    alignItems: 'center',
    justifyContent: 'center',
    height: HEIGHT * 65,
  },
});

export default CommonStyles;
