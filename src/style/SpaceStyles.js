import { StyleSheet } from "react-native";
import constants from "../constants";

const HEIGHT = (constants.BaseStyle.DEVICE_HEIGHT / 100)
const WIDTH = (constants.BaseStyle.DEVICE_WIDTH / 100)

const SpaceStyles = StyleSheet.create({
    rowFlex: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    row: {
        flexDirection: 'row',
    },
    rowJustify: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    alignSpaceBlock: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    rowWrap: {
        flexWrap: 'wrap',
        width: WIDTH * 60,
        alignItems: 'flex-end'
    },
    sortRoeWrap: {
        flexDirection: "row",
        flexWrap: "wrap",
        alignItems: 'center'
    },
    left5: {
        marginLeft: WIDTH * 5
    },
    left3: {
        marginLeft: WIDTH * 3
    },
    left2: {
        marginLeft: WIDTH * 2
    },
    left1: {
        marginLeft: WIDTH * 1
    },
    width70: {
        width: WIDTH * 70,
    },
    width80: {
        width: WIDTH * 80,
    },
    width40: {
        width: WIDTH * 40,
    },
    width35: {
        width: WIDTH * 35
    },
    spaceVertical: {
        marginVertical: HEIGHT * 2
    },
    vertical3: {
        marginVertical: HEIGHT * 3
    },
    top5: {
        marginTop: HEIGHT * 5
    },
    top3: {
        marginTop: HEIGHT * 3
    },
    top2: {
        marginTop: HEIGHT * 2
    },
    top1: {
        marginTop: HEIGHT * 1
    },
    spaceHorizontal: {
        marginHorizontal: WIDTH * 5
    },
    horizontal10: {
        marginHorizontal: WIDTH * 10
    },
    vertical1: {
        marginVertical: HEIGHT * 1
    },
    vertical2: {
        marginVertical: HEIGHT * 2
    },
    paddingVertical2: {
        paddingVertical: HEIGHT * 2
    },
    width62: {
        width: WIDTH * 63
    },
    paddingTop2: {
        paddingTop: HEIGHT * 2
    },
    paddingBottom2: {
        paddingBottom: HEIGHT * 2
    },
    paddingBottom5: {
        paddingBottom: HEIGHT * 5
    },
    paddingBottom22: {
        paddingBottom: HEIGHT * 22
    },
    width15: {
        width: WIDTH * 15,
    },
    width20: {
        width: WIDTH * 20,
    },
    width25: {
        width: WIDTH * 25,
    },
    width28: {
        width: WIDTH * 28,
    },
    width23: {
        width: WIDTH * 23,
    },
    width45: {
        width: WIDTH * 45,
    },
    width43: {
        width: WIDTH * 43,
    },
    width55: {
        width: WIDTH * 55,
    },
    paddingLeft3: {
        paddingLeft: WIDTH * 3
    },
    bottom1: {
        marginBottom: HEIGHT * 1
    },
    bottom2: {
        marginBottom: HEIGHT * 2
    },
    horizontal3: {
        paddingHorizontal: WIDTH * 3
    },
    width40: {
        width: WIDTH * 40,
    },
    height28: {
        height: 28,
        alignItems: 'center',
        justifyContent: 'center'
    },
    minusMargin: {
        marginHorizontal: -WIDTH * 5
    }
});

export default SpaceStyles;
