import { StyleSheet } from "react-native";
import constants from "../constants";
import { BLACK, BROWN, CLOSE_TEXT, DARK_BLACK, DARK_BORDER_COLOR, DARK_GRAY, DARK_RED, DARK_WHITE, FULL_GRAY, GRAY_TEXT, LIGHT_BLACK, LIGHT_BORDER, LIGHT_GRAY, MAIN_BLACK, PAYMENT_TEXT, PRIMARY_COLOR, REVIEW_TEXT, STANDING_TEXT, TEXT_BLACK, TITLE_BLACK, TOMATO, WHITE } from "../constants/Colors";

const TextStyles = StyleSheet.create({
    bold25TitleBlack: {
        color: TITLE_BLACK,
        ...constants.Fonts.bold25   //25 bold
    },
    bold25TitleWhite: {
        color: WHITE,
        ...constants.Fonts.bold25   //25 bold
    },
    bold18LightBlack: {
        color: LIGHT_BLACK,
        ...constants.Fonts.bold18   //18 bold
    },
    bold16LightBlack: {
        color: LIGHT_BLACK,
        ...constants.Fonts.bold16   //16 bold
    },
    medium16LightBorder: {
        color: LIGHT_BORDER,
        ...constants.Fonts.medium16   //16 medium
    },
    medium16: {
        color: BLACK,
        ...constants.Fonts.medium16   //16 medium
    },
    bold15TitleBlack: {
        color: TITLE_BLACK,
        ...constants.Fonts.bold15   //15 bold
    },
    medium14LightBorder: {
        color: LIGHT_BORDER,
        ...constants.Fonts.medium14   //14 medium
    },
    medium14Review: {
        color: REVIEW_TEXT,
        ...constants.Fonts.medium14   //14 medium
    },
    medium14ReviewWhite: {
        color: WHITE,
        ...constants.Fonts.medium14   //14 medium
    },
    bold14LightBlack: {
        color: LIGHT_BLACK,
        ...constants.Fonts.bold14   //14 bold
    },
    bold14MainBlack: {
        color: MAIN_BLACK,
        ...constants.Fonts.bold14   //14 bold
    },
    bold14LightBorder: {
        color: LIGHT_BORDER,
        ...constants.Fonts.bold14   //14 bold
    },
    bold13MainBlack: {
        color: MAIN_BLACK,
        ...constants.Fonts.bold13   //13 bold
    },
    bold13DarkBorder: {
        color: DARK_BORDER_COLOR,
        ...constants.Fonts.bold13   //13 bold
    },
    bold13TitleBlack: {
        color: TITLE_BLACK,
        ...constants.Fonts.bold13Black   //13 bold black
    },
    bold13Red: {
        color: DARK_RED,
        ...constants.Fonts.bold13Black   //13 bold black
    },
    bold12DarkBlack: {
        color: DARK_BLACK,
        ...constants.Fonts.bold12   //12 bold
    },
    bold12LightBlack: {
        color: LIGHT_BLACK,
        ...constants.Fonts.bold12   //12 bold
    },
    bold12LightGray: {
        color: LIGHT_GRAY,
        ...constants.Fonts.bold12   //12 bold
    },
    bold12Black: {
        color: BLACK,
        ...constants.Fonts.bold12   //12 bold
    },
    bold12White: {
        color: WHITE,
        ...constants.Fonts.bold12   //12 bold
    },
    medium12BackText: {
        color: TEXT_BLACK,
        ...constants.Fonts.medium12   //12 medium
    },
    medium12LightBlack: {
        color: LIGHT_BLACK,
        ...constants.Fonts.medium12   //12 medium
    },
    medium11LightBlack: {
        color: LIGHT_BLACK,
        ...constants.Fonts.medium11   //12 medium
    },
    medium12StadingText: {
        color: STANDING_TEXT,
        ...constants.Fonts.medium12   //12 medium
    },
    medium12LightGray: {
        color: LIGHT_GRAY,
        ...constants.Fonts.medium12   //12 medium
    },
    medium12MainBlack: {
        color: MAIN_BLACK,
        ...constants.Fonts.medium12   //12 medium
    },
    medium12Review: {
        color: REVIEW_TEXT,
        ...constants.Fonts.medium12   //12 medium
    },
    medium12LightBorder: {
        color: LIGHT_BORDER,
        ...constants.Fonts.medium12   //12 medium
    },
    medium12White: {
        color: WHITE,
        ...constants.Fonts.medium12   //12 medium
    },
    medium12Orange: {
        color: TOMATO,
        ...constants.Fonts.medium12   //12 medium
    },
    medium10GrayText: {
        color: GRAY_TEXT,
        ...constants.Fonts.medium10   //10 medium
    },
    medium10Black: {
        color: BLACK,
        ...constants.Fonts.medium10   //10 medium
    },
    medium10LightBorder: {
        color: LIGHT_BORDER,
        ...constants.Fonts.medium10   //10 medium
    },
    medium10CloseText: {
        color: CLOSE_TEXT,
        ...constants.Fonts.medium10   //10 medium
    },
    medium10White: {
        color: WHITE,
        ...constants.Fonts.medium10   //10 medium
    },
    medium10FullGray: {
        color: FULL_GRAY,
        ...constants.Fonts.medium10   //10 medium
    },
    medium10DarkRed: {
        color: DARK_RED,
        ...constants.Fonts.medium10   //10 medium
    },
    medium10Paymnent: {
        color: PAYMENT_TEXT,
        ...constants.Fonts.medium10   //10 medium
    },
    bold10White: {
        color: WHITE,
        ...constants.Fonts.bold10   //10 bold
    },
    bold10GrayText: {
        color: GRAY_TEXT,
        ...constants.Fonts.bold10   //10 bold
    },
    bold10DarkGray: {
        color: DARK_GRAY,
        ...constants.Fonts.bold10   //10 bold
    },
    bold10PrimaryColor: {
        color: PRIMARY_COLOR,
        ...constants.Fonts.bold10   //10 bold
    },
    bold10Black: {
        color: BLACK,
        ...constants.Fonts.bold10   //10 bold
    },
    bold9DarkWhite: {
        color: DARK_WHITE,
        ...constants.Fonts.medium9   //9 medium
    },
    medium9LightBorder: {
        color: LIGHT_BORDER,
        ...constants.Fonts.medium9   //9 medium
    },
    bold8Brown: {
        color: BROWN,
        ...constants.Fonts.bold8   //8 bold
    },
    bold8White: {
        color: WHITE,
        ...constants.Fonts.bold8   //8 bold
    },
    bold8DarkBlack: {
        color: DARK_BLACK,
        ...constants.Fonts.bold8   //8 bold
    },
    bold8DarkWhite: {
        color: DARK_WHITE,
        ...constants.Fonts.medium8   //8 medium
    },
    medium8White: {
        color: WHITE,
        ...constants.Fonts.medium8   //8 medium
    },
    medium8LightBorder: {
        color: LIGHT_BORDER,
        ...constants.Fonts.medium8   //8 medium
    },
    bold6DarkBlack: {
        color: DARK_BLACK,
        ...constants.Fonts.bold6   //6 bold
    },
    bold6DarkWhite: {
        color: WHITE,
        ...constants.Fonts.bold6   //6 bold
    },

});

export default TextStyles;
