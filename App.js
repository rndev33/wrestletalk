import React, {useEffect} from 'react';
import {LogBox, StatusBar, Platform} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import NavigationService from './src/navigation/NavigationService';
import StackNaviagtion from './src/navigation/StackNavigation';
import {PRIMARY_COLOR} from './src/constants/Colors';
import {Provider} from 'react-redux';
import {configureStore} from './src/redux/store';
import {fcmService} from './src/redux/services/fcmService';
import {IAPService} from './src/iap';
import {OpenAppAdService} from './src/components/openAppAd';
import OneSignal from 'react-native-onesignal';
import mobileAds from 'react-native-google-mobile-ads';
import {check, request, PERMISSIONS, RESULTS} from 'react-native-permissions';
import {Host} from 'react-native-portalize';
import {GestureHandlerRootView} from 'react-native-gesture-handler';
import {initialConfig} from './src/redux/config/apiConfig';

const App = () => {
  const isIos = Platform.OS === 'ios';
  useEffect(() => {
    initialConfig();
  }, []);
  const CheckPermissions = async () => {
    const result = await check(PERMISSIONS.IOS.APP_TRACKING_TRANSPARENCY);
    if (result === RESULTS.DENIED) {
      // The permission has not been requested, so request it.
      await request(PERMISSIONS.IOS.APP_TRACKING_TRANSPARENCY);
    }
    await mobileAds().initialize();
  };

  OneSignal.setAppId(
    isIos
      ? '458e12e6-c830-4b6c-b1fc-79c705e683ce'
      : '56859181-0f94-43b4-9200-629da14e792d',
  );
  LogBox.ignoreAllLogs();
  const store = configureStore();
  function onRegister() {}
  const onNotification = () => {};
  const onOpenNotification = () => {};

  useEffect(() => {
    CheckPermissions();
    fcmService.registerAppWithFCM();
    fcmService.register(onRegister, onNotification, onOpenNotification);
  }, []);

  return (
    <Provider store={store}>
      <StatusBar barStyle="light-content" backgroundColor={PRIMARY_COLOR} />
      <NavigationContainer
        ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}>
        <GestureHandlerRootView style={{flex: 1}}>
          <Host>
            <StackNaviagtion />
            {/* <OpenAppAdService /> */}
            <IAPService />
          </Host>
        </GestureHandlerRootView>
      </NavigationContainer>
    </Provider>
  );
};

export default App;
